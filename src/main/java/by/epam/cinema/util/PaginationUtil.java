package by.epam.cinema.util;

import by.epam.cinema.command.constant.DefaultConst;

import java.util.ArrayList;
import java.util.List;

/**
 * Class {@code PaginationUtil} is an util class that deals with defining pages numbers in jsp
 */
public class PaginationUtil {

    /**
     * <p>Calculates total number of pages</p>
     * @param listSize defines the quantity of objects that are to be divided into pages
     * @return total number of pages
     */
    public static int calculateTotalNumberOfPages(int listSize){
        return (int) Math.ceil(listSize * 1.0 / DefaultConst.DEFAULT_RECORDS_PER_PAGE_NUMBER);
    }

    /**
     * <p>Calculates starting and ending record number</p>
     * @param pageNumber specifies page
     * @return list consisting of starting and ending record number
     */
    public static List<Integer> calculateRecordInterval(int pageNumber){
        List<Integer> interval = new ArrayList<>();
        int startRecordNumber = (pageNumber - 1) * DefaultConst.DEFAULT_RECORDS_PER_PAGE_NUMBER + 1;
        int endRecordNumber = startRecordNumber - 1 + DefaultConst.DEFAULT_RECORDS_PER_PAGE_NUMBER ;
        interval.add(startRecordNumber);
        interval.add(endRecordNumber);
        return interval;
    }


}
