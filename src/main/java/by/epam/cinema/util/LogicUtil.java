package by.epam.cinema.util;


import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class {@code LogicUtil} is an util class that performs logical validations
 */
public class LogicUtil {
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String FILE_TYPE = "image";

    /**
     * <p>Checks strings for not being equal to null</p>
     * @param parameters
     * @return false if given values are null and true otherwise
     */
    public static boolean isNotNull(String ... parameters){
        if(parameters == null){
            return false;
        }
        for(String parameter: parameters){
            if(parameter.isEmpty()){
                return false;
            }
        }
        return true;
    }


    /**
     * <p>Parses given numbers and checks every one of them for being a positive numbers</p>
     * @param parameters
     * @return false if number isn't positive and true otherwise
     */
    public static boolean isNumberPositive(String ... parameters){
        for(String parameter:parameters){
            int number = Integer.parseInt(parameter);
            if(number < 0){
                return false;
            }
        }
        return true;
    }

    /**
     * <p>Checks if given file is an image</p>
     * @param file
     * @return true if given file is an image and false otherwise
     */
    public static boolean isAnImage(File file){
        String mimeType= new MimetypesFileTypeMap().getContentType(file);
        String type = mimeType.split("/")[0];
        if(type.equals(FILE_TYPE)) {
            return true;
        }
        return false;
    }

    /**
     * <p>Checks if given string is an email</p>
     * @param email
     * @return true if a given string is an email and false otherwise
     */
    public static boolean isAnEmail(String email){
        if(isNotNull(email)) {
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(email);
            return matcher.matches();
        }
        return false;

    }

}
