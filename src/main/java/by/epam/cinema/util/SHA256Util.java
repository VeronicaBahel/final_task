package by.epam.cinema.util;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Class {@code SHA256Util} is an util class that dela with hashing passwords
 */
public class SHA256Util {
    private static final String SALT = "Salt12@$@4&#%^$*";

    /**
     * <p>Encrypts a password with given salt</p>
     * @param password specifies password
     * @return encrypted password
     */
    public static String encrypt(String password){
        return DigestUtils.sha256Hex(password + SALT);
    }
}
