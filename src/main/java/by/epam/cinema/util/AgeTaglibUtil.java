package by.epam.cinema.util;


import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;

/**
 * Class {@code AgeTaglibUtil} extends class {@code TagSupport} to form and process own tag
 * @author Veronica Bahel
 */
public class AgeTaglibUtil extends TagSupport {

    private static Logger logger = Logger.getLogger(AgeTaglibUtil.class);

    private Date birthday;

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public AgeTaglibUtil(){}

    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            LocalDate today = LocalDate.now();
            Period age = Period.between(birthday.toLocalDate(), today);
            out.print(age.getYears());
        } catch (IOException e) {
            logger.error("IOException occurred while trying to print age to a jsp");
        }
        return SKIP_BODY;
    }

    public int doEndTag() throws JspException{
        return EVAL_PAGE;
    }

}
