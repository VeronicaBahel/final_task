package by.epam.cinema.service;
import by.epam.cinema.dao.IRatingDAO;
import by.epam.cinema.dao.impl.OracleRatingDAO;
import by.epam.cinema.dao.DAOException;
import by.epam.cinema.entity.Comment;
import by.epam.cinema.entity.Movie;
import by.epam.cinema.entity.Rating;
import by.epam.cinema.util.PaginationUtil;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Map;


/**
 * Class {@code RatingService} is the class, that deals with transmitting data from commands to dao layer
 * @author Veronica Bahel
 */
public class RatingService {

    private static final IRatingDAO dao = new OracleRatingDAO();

    /**
     * <p>Parses all needed parameters,sends them to dao</p>
     * @param stringUserId
     * @param stringMovieId
     * @param stringRatingValue
     * @param ratingDate
     * @throws ServiceException
     */
    public static void addRating(String stringUserId, String stringMovieId, String stringRatingValue, Date ratingDate) throws ServiceException {
        try {
            long movieId = Long.parseLong(stringMovieId);
            int ratingValue = Integer.parseInt(stringRatingValue);
            long userId = Long.parseLong(stringUserId);
            Rating rating = new Rating(userId, movieId, ratingValue, ratingDate);
            dao.addRating(rating);
        } catch (DAOException e) {
            throw new ServiceException("Cant add rating ", e);
        }
    }

    /**
     * <p>Parses user id, then sends it to dao</p>
     * @param stringUserId specifies user
     * @return map of users and their ratings
     * @throws ServiceException
     */
    public static Map<Movie, Rating> showUserRates(String stringUserId) throws ServiceException{
        try{
            long userId = Long.parseLong(stringUserId);
            Map<Movie, Rating> rates = dao.showUserRatings(userId, Integer.MIN_VALUE, Integer.MAX_VALUE);
            for(Map.Entry<Movie, Rating> entry : rates.entrySet()){
                entry.getKey().setGenres(GenreService
                        .showGenresForTheMovie(String.valueOf(entry.getKey().getMovieId())));
            }
            return rates;
        } catch (DAOException e) {
            throw new ServiceException("Can't show rates for this user ", e);
        }
    }

    /**
     * <p>Retrieves number of pages for user ratings</p>
     * @param stringUserId specifies user
     * @return number of ratings pages
     * @throws ServiceException
     */
    public static int countUserRatesPages(String stringUserId) throws ServiceException{
        try{
            long userId = Long.parseLong(stringUserId);
            Map<Movie, Rating> userComments = dao.showUserRatings(userId, Integer.MIN_VALUE, Integer.MAX_VALUE);
            return PaginationUtil.calculateTotalNumberOfPages(userComments.size());
        } catch (DAOException e) {
            throw new ServiceException("Can't show movies list size", e);
        }
    }

    /**
     * <p>Parses user and movie id, then sends them to dao</p>
     * @param stringUserId specifies user
     * @param stringMovieId specifies movie
     * @throws ServiceException
     */
    public static void deleteRating(String stringUserId, String stringMovieId) throws ServiceException{
        try {
            long movieId = Long.parseLong(stringMovieId);
            long userId = Long.parseLong(stringUserId);
            dao.deleteRating(userId, movieId);
        } catch (DAOException e) {
            throw new ServiceException("Can't delete rating from this user", e);
        }
    }

    /**
     * <p>Parses movies id, then sends it to dao </p>
     * @param stringMovieId specifies movie
     * @return average rating for this movie
     * @throws ServiceException
     */
    public static double showAverageRating(String stringMovieId) throws ServiceException {
        try {
            long movieId = Long.parseLong(stringMovieId);
            return dao.showAverageRating(movieId);
        } catch (DAOException e) {
            throw new ServiceException("Can't show average rating ", e);
        }
    }

    /**
     * <p>Parses needed parameters, sends them to dao</p>
     * @param stringMovieId specifies movie
     * @param stringUserId specifies user
     * @return rating that user gave to that movie
     * @throws ServiceException
     */
    public static int retrieveRatingValue(String stringMovieId, String stringUserId) throws ServiceException {
        try{
            long movieId = Long.parseLong(stringMovieId);
            long userId = Long.parseLong(stringUserId);
            return dao.retrieveRatingValue(userId, movieId);
        } catch (DAOException e) {
            throw new ServiceException("Can't retrieve user's + " + stringUserId + " rating for movie: " + stringMovieId, e);
        }
    }

    /**
     * <p>Retrieves number of user ratings</p>
     * @param userId specifies user
     * @return number of user rates
     * @throws ServiceException
     */
    public static int countUserRates(long userId) throws ServiceException {
        try {
            return dao.showUserRatings(userId, Integer.MIN_VALUE, Integer.MAX_VALUE).size();
        } catch (DAOException e) {
            throw new ServiceException("Can't count user rates ", e);
        }
    }


}
