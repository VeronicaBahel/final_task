package by.epam.cinema.service;


import by.epam.cinema.dao.IPersonDAO;
import by.epam.cinema.dao.impl.OraclePersonDAO;
import by.epam.cinema.dao.DAOException;
import by.epam.cinema.entity.Person;
import by.epam.cinema.util.PaginationUtil;

import javax.servlet.http.Part;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;

/**
 * Class {@code PersonService} is the class, that deals with transmitting data from commands to dao layer
 * @author Veronica Bahel
 */
public class PersonService {

    private static final IPersonDAO dao = new OraclePersonDAO();

    /**
     * <p>Defines page number, then calls dao to retrieve movies list and sets it's average rating and genres</p>
     * @param stringPageNumber
     * @return
     * @throws ServiceException
     */
    public static ArrayList<Person> showPeople(String stringPageNumber) throws ServiceException {
        try {
            int pageNumber = (Integer.parseInt(stringPageNumber));

            return dao.showPeople(PaginationUtil.calculateRecordInterval(pageNumber).get(0),
                    PaginationUtil.calculateRecordInterval(pageNumber).get(1));

        } catch (DAOException e) {
            throw new ServiceException("Can't show list of people", e);
        }
    }

    /**
     * <p>Retrieves all people by calling dao</p>
     * @return list of people
     * @throws ServiceException
     */
    public static ArrayList<Person> showWholeListOfPeople() throws ServiceException {
        try {
            return dao.showPeople(Integer.MIN_VALUE, Integer.MAX_VALUE);
        } catch (DAOException e) {
            throw new ServiceException("Can't show list of people", e);
        }
    }

    /**
     * <p>Retrieves number of pages for people</p>
     * @return number of pages
     * @throws ServiceException
     */
    public static int countPeople() throws ServiceException{
        try{
            ArrayList<Person> people = dao.showPeople(Integer.MIN_VALUE, Integer.MAX_VALUE);
            return PaginationUtil.calculateTotalNumberOfPages(people.size());
        } catch (DAOException e) {
            throw new ServiceException("Can't show movies list size", e);
        }
    }

    /**
     * <p>Parses movie id, sends it to dao</p>
     * @param stringMovieId specifies movie
     * @return list of people starred in that particular movie
     * @throws ServiceException
     */
    public static ArrayList<Person> showPeopleForTheMovie(String stringMovieId) throws ServiceException{
        try{
            long movieId = Long.parseLong(stringMovieId);
            return dao.showPeopleForTheMovie(movieId);
        } catch (DAOException e) {
            throw new ServiceException("Can't show list of people for the movie with id " + stringMovieId, e);
        }
    }

    /**
     * <p>Parses person id, sends it to dao</p>
     * @param stringId specifies person
     * @return found person
     * @throws ServiceException
     */
    public static Person findPersonById(String stringId) throws ServiceException {
        try {
            long personId = Long.parseLong(stringId);
            return dao.findPersonById(personId);
        } catch (DAOException e) {
            throw new ServiceException("Can't find person with id " + stringId, e);
        }
    }

    /**
     * <p>Loads files into directory, parses all needed parameters, sends them to dao</p>
     * @param requestPage is necessary for correct file loading
     * @param firstName
     * @param lastName
     * @param birthday
     * @param pictureURL
     * @param birthPlace
     * @param picturePart
     * @return generated person id
     * @throws ServiceException
     */
    public static long addPerson(String requestPage, String firstName, String lastName, String birthday,
                         String pictureURL, String birthPlace, Part picturePart) throws ServiceException{
        try{
            if(!MediaFilesUtil.getFileName(picturePart).isEmpty()) {
                pictureURL = MediaFilesUtil.addFile(picturePart, requestPage);
            }
            Person person = new Person(firstName, lastName, Date.valueOf(birthday), birthPlace, pictureURL);
            return dao.addPerson(person);
        }catch (DAOException e) {
            throw new ServiceException("Can't add person to the database ", e);
        } catch (IOException e) {
            throw new ServiceException("Can't load files into directory ", e);
        }

    }


    /**
     * <p>Loads files into directory, parses all needed parameters, sends them to dao</p>
     * @param requestPage is necessary for correct file loading
     * @param stringPersonId specifies person
     * @param firstName
     * @param lastName
     * @param birthday
     * @param birthPlace
     * @param imagePart
     * @param image
     * @throws ServiceException
     */
    public static void updatePersonInfo(String requestPage, String stringPersonId, String firstName, String lastName, String birthday,
                                String birthPlace, Part imagePart, String image) throws ServiceException{
        try{
            if (!MediaFilesUtil.getFileName(imagePart).isEmpty()) {
                image = MediaFilesUtil.addFile(imagePart, requestPage);
            }
            long personId = Long.parseLong(stringPersonId);
            Person person = new Person(personId, firstName, lastName, Date.valueOf(birthday), birthPlace, image);
            dao.updatePersonInfo(person);
        } catch (DAOException e) {
            throw new ServiceException("Can't update person info", e);
        } catch (IOException e) {
            throw new ServiceException("Can't load files into directory ", e);
        }
    }


    /**
     * <p>Parses all needed parameters, then sends them to dao</p>
     * @param stringMovieId specifies movie
     * @param personIds specifies each person participated in that movie
     * @param role
     * @throws ServiceException
     */
    public static void addPeopleToTheMovie(String stringMovieId, ArrayList<String> personIds, String role) throws ServiceException {
        try{
            long movieId = Long.parseLong(stringMovieId);
            for (String stringPersonId : personIds) {
                long personId = Long.parseLong(stringPersonId);
                dao.addPeopleToTheMovie(personId, movieId, role);
            }
        } catch (DAOException e) {
            throw new ServiceException("Can't add people with role " + role + " to the movie " + stringMovieId, e);
        }
    }

    /**
     * <p>Parses movie id, then sends it to dao</p>
     * @param stringMovieId specifies movie
     * @throws ServiceException
     */
    public static void deleteMoviePeople(String stringMovieId) throws ServiceException {
        try{
            long movieId = Long.parseLong(stringMovieId);
            dao.deleteMoviePeople(movieId);
        } catch (DAOException e) {
            throw new ServiceException("Can't delete people for the movie: " + stringMovieId, e);
        }
    }

    /**
     * <p>Sends given day and month to dao</p>
     * @param day specifies day
     * @param month specifies month
     * @return list of people born today
     * @throws ServiceException
     */
    public static ArrayList<Person> showBornToday(int day, int month) throws ServiceException {
        try {
            return dao.showBornToday(day, month);
        } catch (DAOException e) {
            throw new ServiceException("Can't show list of people born today ", e);
        }
    }
}
