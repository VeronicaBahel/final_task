package by.epam.cinema.service;

import by.epam.cinema.dao.IUserDAO;
import by.epam.cinema.dao.impl.OracleUserDAO;
import by.epam.cinema.dao.DAOException;
import by.epam.cinema.entity.User;
import by.epam.cinema.util.LogicUtil;
import by.epam.cinema.util.SHA256Util;

import javax.servlet.http.Part;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Class {@code UserService} is the class, that deals with transmitting data from commands to dao layer
 * @author Veronica Bahel
 */
public class UserService {

    private static final IUserDAO dao = new OracleUserDAO();


    /**
     * <p>Parses all needed parameters,sends them to dao</p>
     * @param email
     * @param login
     * @param password is to be encrypted
     * @param defaultPicture is set on registration
     * @param defaultRole is regular user
     * @throws ServiceException
     */
    public static void addUser(String email, String login, String password, String defaultPicture, String defaultRole) throws ServiceException {
        try {
            User user = new User(email, login, SHA256Util.encrypt(password), defaultPicture, defaultRole);
            user.setImageURL(defaultPicture);
            dao.addUser(user);
        } catch (DAOException e) {
            throw new ServiceException("Can't add user to a database ", e);
        }
    }


    /**
     * <p>Sends given login to dao</p>
     * @param enterLogin specifies user
     * @return found user
     * @throws ServiceException
     */
    public static User findUser(String enterLogin) throws ServiceException {
        try {
            return dao.findUser(enterLogin);
        } catch (DAOException e) {
            throw new ServiceException("There's no user with such username ", e);
        }
    }

    /**
     * <p>Checks if user entered their password correctly</p>
     * @param user specifies user
     * @param enterPassword is entered by user
     * @throws ServiceException
     */
    public static void checkUser(User user, String enterPassword) throws ServiceException{
        if(!SHA256Util.encrypt(enterPassword).equals(user.getPassword())){
             throw new ServiceException("Wrong password ");
         }
    }


    /**
     * <p>Parses all needed parameters,sends them to dao</p>
     * @param email
     * @param requestPage is necessary ro correct file uploading
     * @param stringUserId
     * @param firstName
     * @param lastName
     * @param email
     * @param currentPassword
     * @param imagePart
     * @param login
     * @param image
     * @param location
     * @param stringBirthday
     * @throws ServiceException
     */
    public static void updateUserInfo(String requestPage, String stringUserId, String firstName, String lastName, String email,
                               String currentPassword, Part imagePart, String login, String image,
                               String location, String stringBirthday) throws ServiceException{
        try{
            if(!MediaFilesUtil.getFileName(imagePart).isEmpty()){
                image = MediaFilesUtil.addFile(imagePart, requestPage);
            }
            User user = new User();
            long userId = Long.parseLong(stringUserId);
            user.setUserId(userId);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            user.setPassword(currentPassword);
            user.setLogin(login);
            user.setImageURL(image);
            user.setLocation(location);
            if(LogicUtil.isNotNull(stringBirthday)){
                user.setBirthday(Date.valueOf(stringBirthday));
            }
            dao.updateUserInfo(user);
        } catch (DAOException e) {
            throw new ServiceException("Can't update user in service method ", e);
        } catch (IOException e) {
            throw new ServiceException("Can't load files into directory ", e);
        }
    }

    /**
     * <p>Parses all needed parameters, then sends them to dao</p>
     * @param stringUserId specifies user
     * @param defaultImage is set instead of user's picture
     * @throws ServiceException
     */
    public static void deleteProfilePicture (String stringUserId, String defaultImage) throws ServiceException{
        try {
            long userId = Long.parseLong(stringUserId);
            dao.deleteProfilePicture(userId, defaultImage);
        } catch (DAOException e) {
            throw new ServiceException("Can't set default image", e);
        }
    }

    /**
     * <p>Parses user id and sends it to dao</p>
     * @param stringId specifies user
     * @return found user
     * @throws ServiceException
     */
    public static User findUserById(String stringId) throws ServiceException {
        try {
            long userId = Long.parseLong(stringId);
            return dao.findUserById(userId);
        } catch (DAOException e) {
            throw new ServiceException("Can't find user with such login ", e);
        }
    }

    /**
     * <p>Parses all needed parameters and sends them to dao</p>
     * @param stringUserId specifies user
     * @param role specifies their role
     * @throws ServiceException
     */
    public static void assignRoleToUser(String stringUserId, String role) throws ServiceException {
        try{
            long userId = Long.parseLong(stringUserId);
            dao.assignRoleToUser(userId, role);
        } catch (DAOException e) {
            throw new ServiceException("Can't ban user with id ", e);
        }
    }

    /**
     * <p>Calls dao to retrieve a table of all users</p>
     * @return list of maps between users and their statistics
     * @throws ServiceException
     */
    public static ArrayList<HashMap<User, ArrayList<Integer>>> showUserTable() throws ServiceException{
        try{
            return dao.showUserTable();
        } catch (DAOException e) {
            throw new ServiceException("Can't show user table ", e);
        }
    }

    /**
     * <p>Parses all needed parameters and sends them to dao</p>
     * @param stringUserId specifies user
     * @throws ServiceException
     */
    public static void deleteProfile(String stringUserId) throws ServiceException {
        try{
            long userId = Long.parseLong(stringUserId);
            dao.deleteProfile(userId);
        } catch (DAOException e) {
            throw new ServiceException("Can't delete user ", e);
        }
    }

    /**
     * <p>Retrieves statistic for a single user. Parses user i and sends it to dao</p>
     * @param stringUserId specifies user
     * @return list of statistic values
     * @throws ServiceException
     */
    public static ArrayList<Integer> showUserStatistic(String stringUserId) throws ServiceException{
        try{
            long userId = Long.parseLong(stringUserId);
            ArrayList<Integer> statistic = new ArrayList<>();
            statistic.add(CommentService.countUserComments(stringUserId));
            statistic.add(RatingService.countUserRates(userId));
            return statistic;
        } catch (ServiceException e) {
            throw new ServiceException("Can't initialize user statistic ", e);
        }
    }


}
