package by.epam.cinema.service;


import by.epam.cinema.util.LogicUtil;

import javax.servlet.http.Part;
import java.io.*;

/**
 * Class {@code MediaFilesUtil} is a class that deals with handling media files logic
 * @author Veronica Bahel
 */
public class MediaFilesUtil {

    private static final int BYTE_SIZE = 1024;
    private static final String UPLOAD_DIRECTORY = "upload";
    private static final String FILE_URL_DIRECTORY = "/upload/";

    private static final String YOUTUBE_PREFIX = "watch?v=";
    private static final String EMBED_PREFIX = "embed/";


    /**
     * <p>Adds image to a directory</p>
     * @param imgPart specifies file
     * @param requestPage is needed for retrieving real path to the image
     * @return url of the added image
     * @throws IOException
     */
    protected static String addFile(Part imgPart, String requestPage) throws IOException{
        String fileName = getFileName(imgPart);
        String fileURL = FILE_URL_DIRECTORY + fileName;
        File file = new File(requestPage + UPLOAD_DIRECTORY + File.separator + fileName);
        try (OutputStream out = new FileOutputStream(file);
             InputStream fileContent = imgPart.getInputStream();) {
            int read = 0;
            final byte[] bytes = new byte[BYTE_SIZE];

            if(LogicUtil.isAnImage(file)){
                while ((read = fileContent.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
            }

        }
        return fileURL;
    }

    /**
     * <p>Replaces a youtube prefix to a prefix needed for correct representation on th jsp</p>
     * @param link specifies youtube link
     * @return correct link
     */
    protected static String replacePrefix(String link){
        String workingLink = link.replace(YOUTUBE_PREFIX, EMBED_PREFIX);
        return workingLink;
    }


    /**
     * <p>Retrieves the name of the image</p>
     * @param part specifies a file
     * @return image url
     */
    protected static String getFileName(final Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

}
