package by.epam.cinema.service;

import by.epam.cinema.command.constant.DefaultConst;
import by.epam.cinema.dao.IMovieDAO;
import by.epam.cinema.dao.impl.OracleMovieDAO;
import by.epam.cinema.dao.DAOException;
import by.epam.cinema.entity.Movie;
import by.epam.cinema.util.LogicUtil;
import by.epam.cinema.util.PaginationUtil;

import javax.servlet.http.Part;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Class {@code MediaFilesUtil} is the class, that deals with transmitting data from commands to dao layer
 * @author Veronica Bahel
 */
public class MovieService {
    private static final IMovieDAO dao = new OracleMovieDAO();


    /**
     * <p>Defines page number, then calls dao to retrieve movies list and sets it's average rating and genres</p>
     * @param stringPageNumber specifies page number
     * @return list of movies
     * @throws ServiceException
     */
    public static ArrayList<Movie> showMovies(String stringPageNumber) throws ServiceException {
        try {
            int pageNumber = (Integer.parseInt(stringPageNumber));

            ArrayList<Movie> movies = dao.showMovies(PaginationUtil.calculateRecordInterval(pageNumber).get(0),
                    PaginationUtil.calculateRecordInterval(pageNumber).get(1));
            for(Movie movieItem: movies){
                movieItem.setAverageRating(RatingService
                        .showAverageRating(String.valueOf(movieItem.getMovieId())));
                movieItem.setGenres(GenreService
                        .showGenresForTheMovie(String.valueOf(movieItem.getMovieId())));
            }
            return movies;
        } catch (DAOException e) {
            throw new ServiceException("Can't show movies list ", e);
        }
    }

    /**
     * <p>Determines the size of movies list</p>
     * @return size of the movies list
     * @throws ServiceException
     */
    public static int countMovies() throws ServiceException{
        try{
            ArrayList<Movie> movies = dao.showMovies(Integer.MIN_VALUE, Integer.MAX_VALUE);
            return PaginationUtil.calculateTotalNumberOfPages(movies.size());
        } catch (DAOException e) {
            throw new ServiceException("Can't show movies list size", e);
        }
    }

    /**
     * <p>Defines page number, then calls dao to retrieve movies list and sets it's average rating and genres</p>
     * @param stringPageNumber specifies page number
     * @return list of movies
     * @throws ServiceException
     */
    public static ArrayList<Movie> showComingSoon(String stringPageNumber) throws ServiceException {
        try {
            int pageNumber = (Integer.parseInt(stringPageNumber));
            ArrayList<Movie> movies = dao.showComingSoon(PaginationUtil.calculateRecordInterval(pageNumber).get(0),
                    PaginationUtil.calculateRecordInterval(pageNumber).get(1));
            for(Movie movieItem: movies){
                movieItem.setAverageRating(RatingService
                        .showAverageRating(String.valueOf(movieItem.getMovieId())));
                movieItem.setGenres(GenreService
                        .showGenresForTheMovie(String.valueOf(movieItem.getMovieId())));
            }
            return movies;
        } catch (DAOException e) {
            throw new ServiceException("Can't show movies list ", e);
        }
    }

    /**
     * <p>Determines the size of the pages range</p>
     * @return size of the pages range
     * @throws ServiceException
     */
    public static int countComingSoon() throws ServiceException{
        try{
            ArrayList<Movie> movies = dao.showComingSoon(Integer.MIN_VALUE, Integer.MAX_VALUE);
            return PaginationUtil.calculateTotalNumberOfPages(movies.size());
        } catch (DAOException e) {
            throw new ServiceException("Can't show movies list size", e);
        }
    }

    /**
     * <p>Calls dao to retrieve movies list and sets it's average rating and genres</p>
     * @param limit specifies the top limit
     * @return movies list
     * @throws ServiceException
     */
    public static ArrayList<Movie> showTopRated(int limit) throws ServiceException {
        try {
            ArrayList<Movie> moviesList = dao.showTopRated(limit);
            for(Movie movie: moviesList){
                movie.setAverageRating(RatingService.showAverageRating(String.valueOf(movie.getMovieId())));
                movie.setGenres(GenreService.showGenresForTheMovie(String.valueOf(movie.getMovieId())));
            }
            return moviesList;
        } catch (DAOException e) {
            throw new ServiceException("Can't show top movies list ", e);
        }
    }

    /**
     * <p>Parses person id, the sends it to dao and sets average rating and genres</p>
     * @param stringPersonId specifies person
     * @return list of movies
     * @throws ServiceException
     */
    public static ArrayList<Movie> showMoviesWithPerson(String stringPersonId) throws ServiceException {
        try {
            long personId = Long.parseLong(stringPersonId);
            ArrayList<Movie> moviesList = dao.showMoviesForPerson(personId);
            for(Movie movie: moviesList){
                movie.setAverageRating(RatingService.showAverageRating(String.valueOf(movie.getMovieId())));
                movie.setGenres(GenreService.showGenresForTheMovie(String.valueOf(movie.getMovieId())));
            }
            return moviesList;
        } catch (DAOException e) {
            throw new ServiceException("Can't show movies list ", e);
        }
    }

    /**
     * <p>Defines page number, calls dao to retrieve movies list, then sets average rating and genres</p>
     * @param genre specifies genre
     * @param stringPageNumber specifies page number
     * @return movies list
     * @throws ServiceException
     */
    public static ArrayList<Movie> showMoviesWithGenre(String genre, String stringPageNumber) throws ServiceException {
        try {
            int pageNumber = (Integer.parseInt(stringPageNumber));
            ArrayList<Movie> moviesList = dao.showWithGenre(GenreService.findGenreByLabel(genre).getGenreId(),
                    PaginationUtil.calculateRecordInterval(pageNumber).get(0), PaginationUtil.calculateRecordInterval(pageNumber).get(1));
            for(Movie movie: moviesList){
                movie.setAverageRating(RatingService.showAverageRating(String.valueOf(movie.getMovieId())));
                movie.setGenres(GenreService.showGenresForTheMovie(String.valueOf(movie.getMovieId())));
            }
            return moviesList;
        } catch (DAOException e) {
            throw new ServiceException("Can't show movies with genre: " + genre, e);
        }
    }

    /**
     * <p>Determines the size of the pages range</p>
     * @param genre specifies genre
     * @return size of the pages range
     * @throws ServiceException
     */
    public static int countMoviesWithGenre(String genre) throws ServiceException{
        try{
            long genreId = GenreService.findGenreByLabel(genre).getGenreId();
            ArrayList<Movie> movies = dao.showWithGenre(genreId, Integer.MIN_VALUE, Integer.MAX_VALUE);
            return PaginationUtil.calculateTotalNumberOfPages(movies.size());
        } catch (DAOException e) {
            throw new ServiceException("Can't show movies list size", e);
        }
    }

    /**
     * <p>Calls dao, then sets average rating and genres</p>
     * @return movies list
     * @throws ServiceException
     */
    public static ArrayList<Movie> showMostDiscussed() throws ServiceException {
        try {
            ArrayList<Movie> movies =  dao.showMostDiscussed(DefaultConst.DEFAULT_START_PAGE_NUMBER, DefaultConst.DEFAULT_RECORDS_PER_PAGE_NUMBER);
            for(Movie movieItem: movies){
                movieItem.setAverageRating(RatingService
                        .showAverageRating(String.valueOf(movieItem.getMovieId())));
                movieItem.setGenres(GenreService
                        .showGenresForTheMovie(String.valueOf(movieItem.getMovieId())));
            }
            return movies;
        } catch (DAOException e) {
            throw new ServiceException("Can't initialize most discussed movies list ", e);
        }
    }

    /**
     * <p>Parses user id, defines page number, calls dao and sets average rating and genres</p>
     * @param stringUserId specifies user
     * @param stringPageNumber specifies page number
     * @return list of movies in user's watchlist
     * @throws ServiceException
     */
    public static ArrayList<Movie> viewWatchlist(String stringUserId, String stringPageNumber) throws ServiceException {
        try {
            long userId = Long.parseLong(stringUserId);
            int pageNumber = (Integer.parseInt(stringPageNumber));
            ArrayList<Movie> movies = dao.viewWatchList(userId, PaginationUtil.calculateRecordInterval(pageNumber).get(0),
                    PaginationUtil.calculateRecordInterval(pageNumber).get(1));
            for(Movie movie: movies){
                movie.setAverageRating(RatingService.showAverageRating(String.valueOf(movie.getMovieId())));
                movie.setGenres(GenreService
                        .showGenresForTheMovie(String.valueOf(movie.getMovieId())));
                movie.setPeople(PersonService.showPeopleForTheMovie(stringUserId));
            }
            return movies;
        } catch (DAOException e) {
            throw new ServiceException("Can't show watchlist ", e);
        }
    }

    /**
     * <p>Determines the number of pages for watchlist</p>
     * @param stringUserId specifies user
     * @return number of pages for watchlist
     * @throws ServiceException
     */
    public static int countWatchlistRecords(String stringUserId) throws ServiceException{
        try{
            long userId = Long.parseLong(stringUserId);
            ArrayList<Movie> movies = dao.viewWatchList(userId, Integer.MIN_VALUE, Integer.MAX_VALUE);
            return PaginationUtil.calculateTotalNumberOfPages(movies.size());
        } catch (DAOException e) {
            throw new ServiceException("Can't show movies list size", e);
        }
    }

    /**
     * <p>Parses movie id and calls dao, then sets average rating, genres and people</p>
     * @param stringId specifies movie
     * @return found movie
     * @throws ServiceException
     */
    public static Movie findMovieById(String stringId) throws ServiceException{
        try {
            long movieId = Long.parseLong(stringId);
            Movie movie = dao.findMovieById(movieId);
            movie.setAverageRating(RatingService.showAverageRating(stringId));
            movie.setGenres(GenreService.showGenresForTheMovie(stringId));
            movie.setPeople(PersonService.showPeopleForTheMovie(stringId));
            return movie;
        }catch (DAOException e) {
            throw new ServiceException("Can't find no movie with such id", e);
        }
    }

    /**
     * <p>Adds files to a directory, parses needed parameters and sends them to dao</p>
     * @param requestPage specifies page needed for correct file uploading
     * @param title
     * @param originalTitle
     * @param genres
     * @param actors
     * @param directors
     * @param date
     * @param budget
     * @param ageLimit
     * @param briefDescription
     * @param posterURL
     * @param imgPart
     * @param youtubeTrailerURL
     * @return string version of generated id
     * @throws ServiceException
     */
    public static String addMovie(String requestPage, String title, String originalTitle, String[] genres, String[] actors, String[] directors,
                         String date, String budget, String ageLimit, String briefDescription,
                         String posterURL, Part imgPart, String youtubeTrailerURL) throws ServiceException{
        try {
            if(!MediaFilesUtil.getFileName(imgPart).isEmpty()){
                posterURL = MediaFilesUtil.addFile(imgPart, requestPage);
            }
            String trailerURL = MediaFilesUtil.replacePrefix(youtubeTrailerURL);

            Movie movie = new Movie(title, originalTitle, Date.valueOf(date), Integer.parseInt(budget),
                        Integer.parseInt(ageLimit), briefDescription, posterURL, trailerURL);
            long generatedId = dao.addBasicMovieInformation(movie);
            GenreService.addGenresToTheMovie(String.valueOf(generatedId),
                        new ArrayList<>(Arrays.asList(genres)));
            PersonService.addPeopleToTheMovie(String.valueOf(generatedId),
                        new ArrayList<>(Arrays.asList(actors)), PersonRole.ACTOR.toString().toUpperCase());
            PersonService.addPeopleToTheMovie(String.valueOf(generatedId),
                        new ArrayList<>(Arrays.asList(directors)), PersonRole.DIRECTOR.toString().toUpperCase());
            return String.valueOf(generatedId);
        } catch (DAOException e) {
            throw new ServiceException("Can't add movie to a database ", e);
        } catch (IOException e) {
            throw new ServiceException("Can't load files into directory ", e);
        }
    }

    /**
     * <p>Sends given value to dao</p>
     * @param value specifies search criterion
     * @return list of found movies
     * @throws ServiceException
     */
    public static ArrayList<Movie> searchMovie(String value) throws ServiceException {
        try {
            ArrayList<Movie> movies = dao.search(value);
            for(Movie movie: movies){
                movie.setAverageRating(RatingService.showAverageRating(String.valueOf(movie.getMovieId())));
                movie.setGenres(GenreService
                        .showGenresForTheMovie(String.valueOf(movie.getMovieId())));
            }
            return movies;
        } catch (DAOException e) {
            throw new ServiceException("Can't find a movie which contains such word", e);
        }
    }

    /**
     * <p>Adds files to a directory, parses needed parameters and sends them to dao</p>
     * @param requestPage
     * @param stringMovieId
     * @param title
     * @param originalTitle
     * @param genres
     * @param actors
     * @param directors
     * @param date
     * @param budget
     * @param ageLimit
     * @param briefDescription
     * @param image
     * @param imagePart
     * @param youtubeTrailerURL
     * @throws ServiceException
     */
    public static void updateMovieInfo(String requestPage, String stringMovieId, String title, String originalTitle,
                                String[] genres, String[] actors, String[] directors,
                                String date, String budget, String ageLimit, String briefDescription,
                                String image, Part imagePart, String youtubeTrailerURL) throws ServiceException {
        try {
            if (!MediaFilesUtil.getFileName(imagePart).isEmpty()) {
                image = MediaFilesUtil.addFile(imagePart, requestPage);
            }
            long movieId = Long.parseLong(stringMovieId);
            String trailerURL = MediaFilesUtil.replacePrefix(youtubeTrailerURL);
            Movie movie = new Movie(title, originalTitle, Date.valueOf(date), Integer.parseInt(budget),
                    Integer.parseInt(ageLimit), briefDescription, image, trailerURL);
            movie.setMovieId(movieId);

            dao.updateMovieInformation(movie);

            GenreService.deleteMovieGenres(stringMovieId);
            PersonService.deleteMoviePeople(stringMovieId);

            GenreService.addGenresToTheMovie(stringMovieId, new ArrayList<>(Arrays.asList(genres)));
            PersonService.addPeopleToTheMovie(stringMovieId, new ArrayList<>(Arrays.asList(actors)),
                    PersonRole.ACTOR.name());
            PersonService.addPeopleToTheMovie(stringMovieId, new ArrayList<>(Arrays.asList(directors)),
                    PersonRole.DIRECTOR.name());


        } catch (IOException e) {
            throw new ServiceException("Can't load files into directory ", e);
        } catch (DAOException e) {
            throw new ServiceException("Can't update movie in service method ", e);
        }

    }

    /**
     * <p>Parses needed parameters, sends them to dao</p>
     * @param stringUserId
     * @param stringMovieId
     * @throws ServiceException
     */
    public static void addMovieToFavourites(String stringUserId, String stringMovieId) throws ServiceException {
        try{
            long userId = Integer.parseInt(stringUserId);
            long movieId = Integer.parseInt(stringMovieId);
            dao.addMovieToFavourites(userId, movieId);
        } catch (DAOException e) {
            throw new ServiceException("Can't add movie to favourites ", e);
        }
    }

    /**
     * <p>Parses needed parameters, sends them to dao</p>
     * @param stringUserId
     * @param stringMovieId
     * @throws ServiceException
     */
    public static void removeMovieFromFavourites(String stringUserId, String stringMovieId) throws ServiceException {
        try{
            long userId = Integer.parseInt(stringUserId);
            long movieId = Integer.parseInt(stringMovieId);
            dao.removeMovieFromFavourites(userId, movieId);
        } catch (DAOException e) {
            throw new ServiceException("Can't remove movie from favourites ", e);
        }
    }

    /**
     * <p>Defines whether or not this movie is in this user's watchlist. Parses parameters, sends them to dao
     *  to retrieve user's watchlist and then checks given movie</p>
     * @param stringUserId specifies user
     * @param stringMovieId specifies movie
     * @return true if this movie is in this user's watchlist and false otherwise
     * @throws ServiceException
     */
    public static boolean isInWatchlist(String stringUserId, String stringMovieId) throws ServiceException {
        try {
            long userId = Long.parseLong(stringUserId);
            long movieId = Long.parseLong(stringMovieId);
            ArrayList<Movie> watchlist = dao.viewWatchList(userId, Integer.MIN_VALUE, Integer.MAX_VALUE);
            for(Movie movie: watchlist){
                if(movieId == movie.getMovieId()){
                    return true;
                }
            }
            return false;
        } catch (DAOException e) {
            throw new ServiceException("Can't initialize watchlist ", e);
        }
    }
}
