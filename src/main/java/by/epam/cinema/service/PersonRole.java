package by.epam.cinema.service;

/**
 * Class {@code PersonRole} is the class, that contains enumeration celebrity roles in the system
 * @author Veronica Bahel
 */
public enum PersonRole {
    ACTOR, DIRECTOR, PRODUCER
}
