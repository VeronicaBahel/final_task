package by.epam.cinema.service;

import by.epam.cinema.dao.IGenreDAO;
import by.epam.cinema.dao.impl.OracleGenreDAO;
import by.epam.cinema.dao.DAOException;
import by.epam.cinema.entity.Genre;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * Class {@code GenreService} is the class, that deals with transmitting data from commands to dao layer
 * @author Veronica Bahel
 */
public class GenreService {


    private static final IGenreDAO dao = new OracleGenreDAO();


    /**
     * <p>Parses movie id and sends it to dao</p>
     * @param stringMovieId specifies movie
     * @return list of genres
     * @throws ServiceException
     */
    public static ArrayList<Genre> showGenresForTheMovie(String stringMovieId) throws ServiceException {
        try {
            long movieId = Long.parseLong(stringMovieId);
            return dao.showMovieGenres(movieId);
        } catch (DAOException e) {
            throw new ServiceException("Can't show genres for this movie ", e);
        }
    }

    /**
     * <p>Retrieves all genres</p>
     * @return list of genres
     * @throws ServiceException
     */
    public static ArrayList<Genre> showAllGenres() throws ServiceException {
        try {
            return dao.showAllGenres();
        } catch (DAOException e) {
            throw new ServiceException("Can't show list of all genres ", e);
        }
    }

    /**
     * <p>Parses movie id, then parses all genre ids and sends each of them to dao</p>
     * @param stringMovieId specifies movie
     * @param genreIds specifies list of genres
     * @throws ServiceException
     */
    public static void addGenresToTheMovie(String stringMovieId, ArrayList<String> genreIds) throws ServiceException {
        try {
            long movieId = Long.parseLong(stringMovieId);
            for (String stringGenreId : genreIds) {
                long genreId = Long.parseLong(stringGenreId);
                dao.addGenreToTheMovie(movieId, genreId);
            }
        } catch (DAOException e) {
            throw new ServiceException("Can't add genres to the movie ", e);
        }

    }


    /**
     * <p>Parses movie id and sends it to dao</p>
     * @param stringMovieId specifies movie
     * @throws ServiceException
     */
    public static void deleteMovieGenres(String stringMovieId) throws ServiceException {
        try{
            long movieId = Long.parseLong(stringMovieId);
            dao.deleteMovieGenres(movieId);
        } catch (DAOException e) {
            throw new ServiceException("Can't delete genres for the movie with id: " + stringMovieId, e);
        }
    }

    /**
     * <p>Sends genre label to dao</p>
     * @param genreLabel specifies genre
     * @return found genre
     * @throws ServiceException
     */
    public static Genre findGenreByLabel(String genreLabel) throws ServiceException {
        try {
            return dao.findGenreByLabel(genreLabel);
        } catch (DAOException e) {
            throw new ServiceException("Can't find genre with label: " + genreLabel, e);
        }
    }


}
