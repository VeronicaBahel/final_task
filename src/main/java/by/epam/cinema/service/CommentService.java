package by.epam.cinema.service;

import by.epam.cinema.dao.ICommentDAO;
import by.epam.cinema.dao.impl.OracleCommentDAO;
import by.epam.cinema.dao.DAOException;
import by.epam.cinema.entity.Comment;
import by.epam.cinema.entity.Movie;
import by.epam.cinema.entity.User;
import by.epam.cinema.util.PaginationUtil;

import java.sql.Date;
import java.util.Map;

/**
 * Class {@code CommentService} is the class, that deals with transmitting data from commands to dao layer
 * @author Veronica Bahel
 */
public class CommentService {

    private static final ICommentDAO dao = new OracleCommentDAO();

    /**
     * <p>Determines starting and ending record number, parses string parameters and sends them to dao</p>
     * @param stringMovieId specifies movie
     * @param stringPageNumber specifies page number
     * @return a map of users and their comments
     * @throws ServiceException
     */
    public static Map<User, Comment> showMovieCommentsByPages(String stringMovieId, String stringPageNumber)
            throws ServiceException {
        try {
            int pageNumber = (Integer.parseInt(stringPageNumber));
            long movieId = Long.parseLong(stringMovieId);
            return dao.showMovieComments(movieId,
                    PaginationUtil.calculateRecordInterval(pageNumber).get(0),
                    PaginationUtil.calculateRecordInterval(pageNumber).get(1));
        } catch (DAOException e) {
            throw new ServiceException("Can't show comments for this movie ", e);
        }
    }

    /**
     * <p>Retrieves number of all movie comments</p>
     * @param stringMovieId
     * @return number of all comments
     * @throws ServiceException
     */
    public static int countMovieComments(String stringMovieId) throws ServiceException{
        try{
            long movieId = Long.parseLong(stringMovieId);
            Map<User, Comment> movieComments = dao.showMovieComments(movieId, Integer.MIN_VALUE, Integer.MAX_VALUE);
            return PaginationUtil.calculateTotalNumberOfPages(movieComments.size());
        } catch (DAOException e) {
            throw new ServiceException("Can't show movies list size", e);
        }
    }

    /**
     * <p>Parses string parameters and sends them to dao</p>
     * @param content is the content of the comment
     * @param stringUserId specifies a user
     * @param date is a comment date
     * @param stringMovieId specifies movie
     * @throws ServiceException
     */
    public static void addComment(String content, String stringUserId, Date date, String stringMovieId) throws ServiceException{
        try {
            long movieId = Long.parseLong(stringMovieId);
            long userId = Long.parseLong(stringUserId);
            Comment comment = new Comment(userId, content, date, movieId);
            dao.addComment(comment);
        } catch (DAOException e) {
            throw new ServiceException("Can't add comment ", e);
        }
    }

    /**
     * <p>Parses comment id and sends it to dao</p>
     * @param stringId specifies comment
     * @throws ServiceException
     */
    public static void deleteComment(String stringId) throws ServiceException{
        try {
            long commentId = Long.parseLong(stringId);
            dao.deleteComment(commentId);
        } catch (DAOException e) {
            throw new ServiceException("Can't delete comment with such id", e);
        }
    }

    /**
     * <p>Determines starting and ending record number, parses string parameters and sends them to dao</p>
     * @param stringUserId specifies a user
     * @return a map of movies and comments for them
     * @throws ServiceException
     */
    public static Map<Movie, Comment> showUserComments(String stringUserId) throws ServiceException{
        try{
            long userId = Long.parseLong(stringUserId);
            return dao.showUserComments(userId,Integer.MIN_VALUE, Integer.MAX_VALUE);
        } catch (DAOException e) {
            throw new ServiceException("Can't show comments for this user ", e);
        }
    }

    /**
     * <p>Retrieves number of all user comments</p>
     * @param stringUserId specifies user
     * @return number of user comments
     * @throws ServiceException
     */
    public static int countUserComments(String stringUserId) throws ServiceException{
        try{
            long userId = Long.parseLong(stringUserId);
            Map<Movie, Comment> userComments = dao.showUserComments(userId, Integer.MIN_VALUE, Integer.MAX_VALUE);
            return userComments.size();
        } catch (DAOException e) {
            throw new ServiceException("Can't show movies list size", e);
        }
    }




}
