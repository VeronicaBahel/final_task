package by.epam.cinema.filter;


import org.apache.log4j.Logger;

import javax.servlet.*;
import java.io.IOException;

/**
 * Class {@code EncodingFilter} is the class, that implements {@code Filter} interface to
 * process with different encodings of Russian and English languages.
 * @author Veronica Bahel
 */
public class EncodingFilter implements Filter {

    private static Logger logger = Logger.getLogger(EncodingFilter.class);

    @Override
    public void init(FilterConfig fConfig){

    }

    /**
     * <p>Sets necessary encoding for the pages. </p>
     * @param request is a request we are processing
     * @param response is a response we are creating
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        String encoding = request.getCharacterEncoding();
        if ((encoding != null) && (encoding.equalsIgnoreCase("utf-8")))
        {
            response.setContentType("text/html; charset=utf-8");
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() { }


}
