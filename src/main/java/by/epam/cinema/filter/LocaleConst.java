package by.epam.cinema.filter;

/**
 * Class {@code LocaleConst} contains constants to be used as locale attributes
 * @author Veronica Bahel
 */
public class LocaleConst {

    public static final String ATTR_LOCALE = "locale";
    public static final String EN_LOCALE = "en";
    public static final String RU_LOCALE = "ru";
}
