package by.epam.cinema.filter;


import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Class {@code LocalizationFilter} is the class, that implements {@code Filter} interface to
 * deal with localization in the system.
 * @author Veronica Bahel
 */
public class LocalizationFilter implements Filter {

    private static Logger logger = Logger.getLogger(EncodingFilter.class);

    private String locale;

    public LocalizationFilter(){}

    /**
     * <p>Sets initial locale for the system.</p>
     * @param fConfig is the configuration of the filter.
     */
    public void init(FilterConfig fConfig){
        locale = LocaleConst.EN_LOCALE;
    }

    /**
     * <p>Sets necessary locale for the next pages.</p>
     * @param request is necessary to get actual locale and set next one.
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        logger.info("Initializing localization filter");
        HttpServletRequest req = (HttpServletRequest) request;
        if (req.getSession().getAttribute(LocaleConst.ATTR_LOCALE) == null) {
            req.getSession().setAttribute(LocaleConst.ATTR_LOCALE, locale);
        }
        if (req.getSession().getAttribute(LocaleConst.ATTR_LOCALE) == LocaleConst.EN_LOCALE) {
            req.getSession().setAttribute(LocaleConst.ATTR_LOCALE, LocaleConst.EN_LOCALE);
        }
        if (req.getSession().getAttribute(LocaleConst.ATTR_LOCALE) == LocaleConst.RU_LOCALE) {
            req.getSession().setAttribute(LocaleConst.ATTR_LOCALE, LocaleConst.RU_LOCALE);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() { }


}
