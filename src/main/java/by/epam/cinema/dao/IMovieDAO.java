package by.epam.cinema.dao;

import by.epam.cinema.entity.Movie;

import java.util.ArrayList;
import java.util.List;


/**
 * Interface {@code IMovieDAO} is the class, that contains methods to work with movies info
 * in the database.
 * @author Veronica Bahel
 */
public interface IMovieDAO{
    /**
     * <p>Retrieves all types of movie records. </p>
     * @param offset is the row number of starting movie record in the result set
     * @param numberOfRecords is the row number of ending movie record in the result set
     * @return movies list containing record with row number between given values
     * @throws DAOException
     */
    ArrayList<Movie> showMovies(int offset, int numberOfRecords) throws DAOException;

    /**
     * <p>Retrieves movies that are coming soon. </p>
     * @param offset is the row number of starting movie record in the result set
     * @param numberOfRecords is the row number of ending movie record in the result set
     * @return movies list containing record with row number between given values
     * @throws DAOException
     */
    ArrayList<Movie> showComingSoon(int offset, int numberOfRecords) throws DAOException;

    /**
     * <p>Retrieves movies of a particular genre. </p>
     * @param genreId is used for defining specific genre
     * @param offset is the row number of ending movie record in the result set
     * @param numberOfRecords is the row number of ending movie record in the result set
     * @return movies list containing record with rownumber between given values
     * @throws DAOException
     */
    ArrayList<Movie> showWithGenre(long genreId, int offset, int numberOfRecords) throws DAOException;

    /**
     * <p>Retrieves a single movie item with given id. </p>
     * @param id is the parameter by which the particular movie is found
     * @return particular movie item
     * @throws DAOException
     */
    Movie findMovieById(long id) throws  DAOException;



    /**
     * <p>Inserts basic move information into the db and retrieves generated movieId. </p>
     * @param movie is the Movie entity containing information to be inserted into the db
     * @return generated movieId to simultaneously add it to another tables, such as GENRES and PEOPLE, in service methods
     * @throws DAOException
     */
    long addBasicMovieInformation(Movie movie) throws DAOException;


    /**
     * <p>Performs search by a given criterion. </p>
     * @param value is a given criterion
     * @return movies list containing records satisfying the search criterion
     * @throws DAOException
     */
    ArrayList<Movie> search(String value) throws DAOException;

    /**
     * <p>Updates basic movie information in the database. </p>
     * @param movie is the Movie entity containing information to be updated int the db
     * @throws DAOException
     */
    void updateMovieInformation(Movie movie) throws DAOException;


    /**
     * <p>Adds a particular movie item into a particular user's watchlist. </p>
     * @param userId identifies the particular user's watchlist
     * @param movieId identifies a particular movie item to be added into user's watchlist
     * @throws DAOException
     */
    void addMovieToFavourites(long userId, long movieId) throws DAOException;


    /**
     * <p>Removing a particular movie item from a particular user's watchlist. </p>
     * @param userId identifies the particular user's watchlist
     * @param movieId identifies a particular movie item to be removed from user's watchlist
     * @throws DAOException
     */
    void removeMovieFromFavourites(long userId, long movieId) throws DAOException;


    /**
     * <p>Retrieves top rated movies with row number between and a given value. </p>
     * @param limit is the number of records in the result set
     * @return list of top rated movies
     * @throws DAOException
     */
    ArrayList<Movie> showTopRated(int limit) throws DAOException;


    /**
     * <p>Retrieves most commented movies. </p>
     * @param offset is the row number of starting movie record in the result set
     * @param numberOfRecords is the row number of ending movie record in the result set
     * @return movies list containing record with row number between given values
     * @throws DAOException
     */
    ArrayList<Movie> showMostDiscussed(int offset, int numberOfRecords) throws DAOException;


    /**
     * <p>Retrieves all movies in a particular user's watchlist. </p>
     * @param userId identifies a particular user
     * @param offset is the row number of starting movie record in the result set
     * @param numberOfRecords is the row number of ending movie record in the result set
     * @return list of movies in user's watchlist with row number between given values
     * @throws DAOException
     */
    ArrayList<Movie> viewWatchList(long userId, int offset, int numberOfRecords) throws DAOException;


    /**
     * <p>Retrieves movies a particular person has taken part in (both as an actor and as a director). </p>
     * @param personId identifies a particular person
     * @return list of movies for a person
     * @throws DAOException
     */
    ArrayList<Movie> showMoviesForPerson(long personId) throws DAOException;

}
