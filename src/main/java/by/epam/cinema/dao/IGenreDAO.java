package by.epam.cinema.dao;

import by.epam.cinema.entity.Genre;

import java.util.ArrayList;


/**
 * Interface {@code IGenreDAO} is the class, that contains methods to work with genres info
 * in the database.
 * @author Veronica Bahel
 */
public interface IGenreDAO{

    /**
     * <p>Retrieves all genres for a movie. </p>
     * @param movieId identifies a specific movie item
     * @return list of genres
     * @throws DAOException
     */
    ArrayList<Genre> showMovieGenres(long movieId) throws DAOException;


    /**
     * <p>Retrieves all genres from a database. </p>
     * @return list of genres
     * @throws DAOException
     */
    ArrayList<Genre> showAllGenres() throws DAOException;

    /**
     * <p>Adds list of genres to a movie. </p>
     * @param movieId identifies a specific movie item
     * @param genreId identifies a genre to be added to the movie
     * @throws DAOException
     */
    void addGenreToTheMovie(long movieId, long genreId) throws DAOException;

    /**
     * <p>Deletes list of movie genres. </p>
     * @param movieId identifies a specific movie item
     * @throws DAOException
     */
    void deleteMovieGenres(long movieId) throws DAOException;

    /**
     * <p>Finds a specific genre based on it's label. </p>
     * @param label is a search criterion
     * @return found genre
     * @throws DAOException
     */
    Genre findGenreByLabel(String label) throws DAOException;

}
