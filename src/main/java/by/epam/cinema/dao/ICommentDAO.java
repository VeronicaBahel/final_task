package by.epam.cinema.dao;

import by.epam.cinema.entity.Comment;
import by.epam.cinema.entity.Movie;
import by.epam.cinema.entity.User;

import java.util.Map;

/**
 * Interface {@code ICommentDAO} is the class, that contains methods to work with comments info
 * in the database.
 * @author Veronica Bahel
 */
public interface ICommentDAO {

    /**
     * <p>Retrieves a map of users and their comments for a particular movie item.</p>
     * @param movieId specifies a particular movie item
     * @param startRecordNumber is the row number of starting comment record in the result set
     * @param endRecordNUmber is the row number of ending comment record in the result set
     * @return map of users and their comments
     * @throws DAOException
     */
    Map<User, Comment> showMovieComments(long movieId, int startRecordNumber, int endRecordNUmber) throws DAOException;

    /**
     * <p>Adds comment information to a database.</p>
     * @param comment contains all needed information about a particular comment
     * @throws DAOException
     */
    void addComment(Comment comment) throws DAOException;

    /**
     * <p>Deletes comment information from a database.</p>
     * @param commentId specifies a comment to be deleted
     * @throws DAOException
     */
    void deleteComment(long commentId) throws DAOException;

    /**
     * <p>Retrieves a map of movies and user's comments for it.</p>
     * @param userId specifies a particular user
     * @param startRecordNumber is the row number of starting comment record in the result set
     * @param endRecordNUmber is the row number of ending comment record in the result set
     * @return map of movies and user's comments to it
     * @throws DAOException
     */
    Map<Movie, Comment> showUserComments(long userId, int startRecordNumber, int endRecordNUmber) throws DAOException;

    /**
     * <p>Finds a particular comments based on it's id.</p>
     * @param commentId specifies a comment
     * @return particular comment
     * @throws DAOException
     */
    Comment findCommentById(long commentId) throws DAOException;

}
