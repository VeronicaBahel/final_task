package by.epam.cinema.dao;

import by.epam.cinema.entity.Person;

import java.util.ArrayList;

/**
 * Interface {@code IPersonDAO} is the class, that contains methods to work with people info
 * in the database.
 * @author Veronica Bahel
 */
public interface IPersonDAO {
    /**
     * <p>Add basic info about a person to a database. </p>
     * @param person contains all needed information to be added
     * @throws DAOException
     */
    long addPerson(Person person) throws DAOException;

    /**
     * <p>Retrieves list of all people. </p>
     * @param startRecordNumber is the row number of starting person record in the result set
     * @param endRecordNumber is the row number of ending person record in the result set
     * @return people list containing record with row number between given values
     * @throws DAOException
     */
    ArrayList<Person> showPeople(int startRecordNumber, int endRecordNumber) throws DAOException;

    /**
     * <p>Retrieves list of people born on a particular date. </p>
     * @param day specifies day
     * @param month specifies month
     * @return list of people born today
     * @throws DAOException
     */
    ArrayList<Person> showBornToday(int day, int month) throws DAOException;

    /**
     * <p>Retrieves list of people starring in a particular movie. </p>
     * @param movieId specifies a movie
     * @return list of people
     * @throws DAOException
     */
    ArrayList<Person> showPeopleForTheMovie(long movieId) throws DAOException;


    /**
     * <p>Adds ids of people starring in the movie to this same movie. </p>
     * @param personId specifies person
     * @param movieId specifies movie
     * @param role determines what role that person took in that movie
     * @throws DAOException
     */
    void addPeopleToTheMovie(long personId, long movieId, String role) throws DAOException;

    /**
     * <p>Deletes people ids from a movie item. </p>
     * @param movieId specifies a movie
     * @throws DAOException
     */
    void  deleteMoviePeople(long movieId) throws DAOException;

    /**
     * <p>Finds a particular person based on their id in the database. </p>
     * @param personId specifies person
     * @return particular person
     * @throws DAOException
     */
    Person findPersonById(long personId) throws DAOException;


    /**
     * <p>Updates basic person information in the database. </p>
     * @param person contains all needed information to be updated
     * @throws DAOException
     */
    void updatePersonInfo(Person person) throws DAOException;

}
