package by.epam.cinema.dao;

/**
 * Class {@code DAOException} is the class, that extends {@code Exception} class to get own exceptions for "DAO" layer
 * @author Veronica Bahel
 */
public class DAOException extends Exception{

    public DAOException() {
        super();
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }

    protected DAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
