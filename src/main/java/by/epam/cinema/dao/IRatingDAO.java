package by.epam.cinema.dao;

import by.epam.cinema.entity.Movie;
import by.epam.cinema.entity.Rating;

import java.util.Map;

/**
 * Interface {@code IRatingDAO} is the class, that contains methods to work with ratings info
 * in the database.
 * @author Veronica Bahel
 */
public interface IRatingDAO  {
    /**
     * <p>Add rating to a particular movie item. </p>
     * @param rating contains all needed information to be added
     * @throws DAOException
     */
    void addRating(Rating rating) throws DAOException;

    /**
     * <p>Deleted user's rating to a particular movie. </p>
     * @param userId specifies a user
     * @param movieId specifies a movie item
     * @throws DAOException
     */
    void deleteRating(long userId, long movieId) throws DAOException;

    /**
     * <p>Retrieves average rating value for a particular movie. </p>
     * @param movieId specifies a movie
     * @return average rating
     * @throws DAOException
     */
    double showAverageRating(long movieId) throws DAOException;

    /**
     * <p>Retrieves a rating that user left on a movie item. </p>
     * @param userId specifies user
     * @param movieId specifies movie item
     * @return rating value
     * @throws DAOException
     */
    int retrieveRatingValue(long userId, long movieId) throws DAOException;

    /**
     * <p>Retrieves a map of movies and ratings a specific user has left to it. </p>
     * @param userId specifies a user
     * @param startRecordNumber is the row number of starting record in the result set
     * @param endRecordNumber is the row number of ending record in the result set
     * @return map of movies and ratings for those movies with row number between given values
     * @throws DAOException
     */
    Map<Movie, Rating> showUserRatings(long userId, int startRecordNumber, int endRecordNumber) throws DAOException;
}
