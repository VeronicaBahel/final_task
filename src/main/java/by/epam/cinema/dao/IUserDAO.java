package by.epam.cinema.dao;

import by.epam.cinema.entity.Movie;
import by.epam.cinema.entity.User;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Interface {@code IUserDAO} is the class, that contains methods to work with users info
 * in the database.
 * @author Veronica Bahel
 */
public interface IUserDAO  {
    /**
     * <p>Adds user information into database. </p>
     * @param user contains all needed information to be added
     * @throws DAOException
     */
    void addUser(User user) throws DAOException;

    /**
     * <p>Finds a user based on their login. </p>
     * @param userLogin specifies user
     * @return particular user
     * @throws DAOException
     */
    User findUser(String userLogin) throws DAOException;

    /**
     * <p>Finds a user based on their id. </p>
     * @param userId specifies user
     * @return particular user
     * @throws DAOException
     */
    User findUserById(long userId) throws DAOException;

    /**
     * <p>Updates user information in the database. </p>
     * @param user contains all needed information to be updated
     * @throws DAOException
     */
    void updateUserInfo(User user) throws DAOException;

    /**
     * <p>Sets a default picture for a user's profile image. </p>
     * @param userId specifies user
     * @param defaultImage defines default image
     * @throws DAOException
     */
    void deleteProfilePicture(long userId, String defaultImage) throws DAOException;

    /**
     * <p>Assigns one of two available roles for a user. </p>
     * @param userId specifies user
     * @param role specifies role
     * @throws DAOException
     */
    void assignRoleToUser(long userId, String role) throws DAOException;

    /**
     * <p>Retrieves a user table. </p>
     * @return list of all users and their number of comments and ratings
     * @throws DAOException
     */
    ArrayList<HashMap<User, ArrayList<Integer>>> showUserTable() throws DAOException;

    /**
     * <p>Deletes user information from the database. </p>
     * @param userId specifies user
     * @throws DAOException
     */
    void deleteProfile(Long userId) throws DAOException;


}
