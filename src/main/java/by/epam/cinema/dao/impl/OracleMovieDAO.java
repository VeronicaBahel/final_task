package by.epam.cinema.dao.impl;

import by.epam.cinema.dao.DAOException;
import by.epam.cinema.pool.ConnectionPool;
import by.epam.cinema.dao.IMovieDAO;
import by.epam.cinema.entity.Movie;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class {@code OracleMovieDAO} is the class, that implements {@code IMovieDAO} interface
 * to deal with operations with movies in oracle database
 * @author Veronica Bahel
 */
public class OracleMovieDAO implements IMovieDAO {

    private static Logger logger = Logger.getLogger(OracleMovieDAO.class);

    private static final ConnectionPool POOL = ConnectionPool.getInstance();


    private static final String SQL_SHOW_ALL_MOVIES = "SELECT outer.movie_id, outer.movie_title, " +
            " outer.movie_original_title, outer.movie_world_premiere, outer.movie_budget, " +
            "outer.movie_age_limit, outer.movie_brief_description, outer.movie_poster_url, outer.movie_trailer_url " +
            "   FROM(  SELECT ROWNUM rn, inner.* " +
            "       FROM (SELECT * FROM MOVIES) inner ) outer " +
            "WHERE rn BETWEEN ? AND ? ";

    private static final String SQL_SHOW_COMING_SOON = "SELECT outer.movie_id, outer.movie_title, " +
            " outer.movie_original_title, outer.movie_world_premiere, outer.movie_budget, " +
            " outer.movie_age_limit, outer.movie_brief_description, outer.movie_poster_url, outer.movie_trailer_url " +
            " FROM(  SELECT ROWNUM rn, inner.* " +
            " FROM (SELECT * FROM MOVIES) inner ) outer  " +
            " WHERE (rn BETWEEN ? AND ? ) AND (outer.movie_world_premiere > CURRENT_DATE) " +
            " ORDER BY outer.movie_world_premiere DESC ";

    private static final String SQL_SHOW_WITH_GENRE = "SELECT outer.movie_id, outer.movie_title, "+
            " outer.movie_original_title, outer.movie_world_premiere, outer.movie_budget, "+
            " outer.movie_age_limit, outer.movie_brief_description, outer.movie_poster_url, " +
            "  outer.movie_trailer_url , GENRES.genre_id, GENRES.genre_label "+
            " FROM(  SELECT ROWNUM rn, inner.* "+
            " FROM (SELECT * FROM MOVIES) inner ) outer "+
            " JOIN MOVIE_GENRES ON MOVIE_GENRES.movie_id = outer.movie_id "+
            " JOIN GENRES ON GENRES.genre_id = MOVIE_GENRES.genre_id "+
            " WHERE (rn BETWEEN ? AND ?) AND GENRES.genre_id = ? ";

    private static final String SQL_FIND_MOVIE_BY_ID = "SELECT movie_id, movie_title, movie_original_title, " +
            "movie_world_premiere, movie_budget, movie_age_limit, movie_brief_description, " +
            " movie_poster_url, movie_trailer_url FROM MOVIES WHERE MOVIE_ID = ?";

    private static final String SQL_ADD_MOVIE =
            "INSERT INTO MOVIES(movie_id, movie_title, movie_original_title, movie_world_premiere, " +
                    "movie_budget, movie_age_limit, movie_brief_description," +
                    "movie_poster_url, movie_trailer_url) VALUES (?,?,?,?,?,?,?,?,?)";

    private static final String SQL_UPDATE_MOVIE = "UPDATE MOVIES SET movie_title = ?, movie_original_title = ?, " +
            " movie_world_premiere = ?, movie_budget = ?,  movie_age_limit = ?, movie_brief_description = ?," +
            " movie_poster_url = ?, movie_trailer_url = ? " +
            " WHERE movie_id = ?";

    private static final String SQL_SEARCH = "SELECT movie_id, movie_title, movie_original_title, movie_world_premiere, " +
            " movie_budget, movie_age_limit, movie_brief_description, "+
            " movie_poster_url, movie_trailer_url " +
            " FROM MOVIES WHERE UPPER(movie_title) LIKE UPPER(?) " +
            " OR UPPER(movie_original_title) LIKE UPPER(?) " +
            " OR UPPER(movie_brief_description) LIKE UPPER(?) ESCAPE '!' ";

    private static final String SQL_ADD_TO_FAVOURITES = "INSERT INTO FAVOURITES (movie_id, user_id) VALUES(?,?) ";

    private static final String SQL_DELETE_FROM_FAVOURITES = "DELETE FROM FAVOURITES WHERE movie_id = ? AND user_id = ?";

    private static final String SQL_VIEW_WATCHLIST = "SELECT outer.movie_id, outer.movie_title, " +
            " outer.movie_original_title, outer.movie_world_premiere, outer.movie_budget, " +
            " outer.movie_age_limit, outer.movie_brief_description, outer.movie_poster_url, outer.movie_trailer_url " +
            " FROM (SELECT ROWNUM rn, inner.* FROM (SELECT MOVIES.movie_id, MOVIES.movie_title, " +
            " MOVIES.movie_original_title, MOVIES.movie_world_premiere, MOVIES.movie_budget, " +
            " MOVIES.movie_age_limit, MOVIES.movie_brief_description, MOVIES.movie_poster_url, MOVIES.movie_trailer_url " +
            " FROM MOVIES JOIN FAVOURITES ON MOVIES.movie_id = FAVOURITES.movie_id WHERE FAVOURITES.user_id = ? ) inner) outer " +
            " WHERE rn BETWEEN ? AND ? ";

    private static final String SQL_TOP_RATED = "SELECT outer.movie_id, outer.movie_title, " +
            "outer.movie_original_title, outer.movie_world_premiere, outer.movie_budget, " +
            "outer.movie_age_limit, outer.movie_brief_description, outer.movie_poster_url, outer.movie_trailer_url " +
            "FROM (SELECT ROWNUM rn, inner.* FROM " +
            "  (SELECT RATINGS.movie_id, MOVIES.movie_title, " +
            "        MOVIES.movie_original_title, MOVIES.movie_world_premiere, MOVIES.movie_budget, " +
            "        MOVIES.movie_age_limit, MOVIES.movie_brief_description, MOVIES.movie_poster_url, MOVIES.movie_trailer_url, Round(" +
            "  ( select avg(r.rating_value) from RATINGS r  where RATINGS.movie_id = r.movie_id), 1) avgRating " +
            "  FROM MOVIES JOIN RATINGS ON RATINGS.movie_id = MOVIES.movie_id " +
            "  GROUP by RATINGS.movie_id, MOVIES.movie_title, MOVIES.movie_original_title, MOVIES.movie_world_premiere, MOVIES.movie_budget, " +
            "        MOVIES.movie_age_limit, MOVIES.movie_brief_description, MOVIES.movie_poster_url, MOVIES.movie_trailer_url " +
            "  ORDER by avgRating DESC) inner) outer " +
            "WHERE rn BETWEEN 1 AND ? ";

    private static final String SQL_MOST_DISCUSSED = "SELECT outer.movie_id, outer.movie_title, outer.movie_original_title, " +
            "outer.movie_world_premiere, outer.movie_budget,  outer.movie_age_limit, outer.movie_brief_description, " +
            "outer.movie_poster_url, outer.movie_trailer_url FROM (SELECT ROWNUM rn, inner.* " +
            "FROM (SELECT MOVIES.movie_id, MOVIES.movie_title, MOVIES.movie_original_title, MOVIES.movie_world_premiere, MOVIES.movie_budget, " +
            "MOVIES.movie_age_limit, MOVIES.movie_brief_description, MOVIES.movie_poster_url, " +
            "   MOVIES.movie_trailer_url, MAX(COMMENTS.comment_date) as comments " +
            "FROM MOVIES  " +
            "JOIN COMMENTS ON MOVIES.movie_id = COMMENTS.comment_movie_id " +
            "GROUP BY MOVIES.movie_id, MOVIES.movie_title, MOVIES.movie_original_title, MOVIES.movie_world_premiere, MOVIES.movie_budget, " +
            "MOVIES.movie_age_limit, MOVIES.movie_brief_description, MOVIES.movie_poster_url, MOVIES.movie_trailer_url " +
            "ORDER BY comments DESC ) inner) outer " +
            "WHERE rn BETWEEN ? AND ?";

    private static final String SQL_SHOW_MOVIES_FOR_PERSON = "SELECT DISTINCT MOVIES.movie_id, " +
            " MOVIES.movie_title, MOVIES.movie_original_title,  MOVIES.movie_world_premiere, MOVIES.movie_budget,  " +
            " MOVIES.movie_age_limit, MOVIES.movie_brief_description, MOVIES.movie_poster_url, MOVIES.movie_trailer_url " +
            " FROM MOVIES " +
            " JOIN PEOPLE_MOVIES ON PEOPLE_MOVIES.movie_id = MOVIES.movie_id " +
            " JOIN PEOPLE ON PEOPLE.person_id = PEOPLE_MOVIES.person_id " +
            " WHERE PEOPLE.person_id = ?";



    @Override
    public ArrayList<Movie> showMovies(int startRecordNumber, int endRecordNumber) throws DAOException {
        ResultSet resultSet = null;
        ArrayList<Movie> moviesList = null;
        Connection connection = POOL.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SHOW_ALL_MOVIES)) {
            preparedStatement.setInt(1, startRecordNumber);
            preparedStatement.setInt(2, endRecordNumber);
            resultSet = preparedStatement.executeQuery();
            moviesList = initMovies(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize movies list",e);
        }finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return moviesList;
    }

    @Override
    public ArrayList<Movie> showComingSoon(int startRecordNumber, int endRecordNumber) throws DAOException {
        ResultSet resultSet = null;
        ArrayList<Movie> moviesList = null;
        Connection connection = POOL.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SHOW_COMING_SOON)) {
            preparedStatement.setInt(1, startRecordNumber);
            preparedStatement.setInt(2, endRecordNumber);
            resultSet = preparedStatement.executeQuery();
            moviesList = initMovies(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize movies list",e);
        }finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return moviesList;
    }


    @Override
    public ArrayList<Movie> showWithGenre(long genreId, int startRecordNumber, int endRecordNumber) throws DAOException {
        ResultSet resultSet = null;
        ArrayList<Movie> moviesList = null;
        Connection connection = POOL.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SHOW_WITH_GENRE)) {
            preparedStatement.setInt(1, startRecordNumber);
            preparedStatement.setInt(2, endRecordNumber);
            preparedStatement.setLong(3, genreId);
            resultSet = preparedStatement.executeQuery();
            moviesList = initMovies(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize movies list",e);
        }finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return moviesList;
    }


    @Override
    public Movie findMovieById(long movieId) throws DAOException {
        ResultSet resultSet = null;
        Movie movieItem = new Movie();
        Connection connection = POOL.takeConnection();
        try( PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_MOVIE_BY_ID)) {
            preparedStatement.setLong(1, movieId);
            resultSet = preparedStatement.executeQuery();
            movieItem = initMovieItem(resultSet);
        } catch (SQLException e) {
            throw new DAOException();
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return movieItem;
    }


    @Override
    public long addBasicMovieInformation(Movie movie) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_MOVIE,
                new String[]{"MOVIE_ID"})){
            preparedStatement.setLong(1, movie.getMovieId());
            preparedStatement.setString(2, movie.getTitle());
            preparedStatement.setString(3, movie.getOriginalTitle());
            preparedStatement.setDate(4, Date.valueOf(movie.getWorldPremiere().toString()));
            preparedStatement.setInt(5, movie.getBudget());
            preparedStatement.setInt(6, movie.getAgeLimit());
            preparedStatement.setString(7, movie.getBriefDescription());
            preparedStatement.setString(8, movie.getPosterURL());
            preparedStatement.setString(9, movie.getTrailerURL());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            rs.next();
            int generatedId = rs.getInt(1);
            return generatedId;

        } catch (SQLException e) {
            throw new DAOException("SQLException occurred while adding movie to a database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }
    @Override
    public ArrayList<Movie> search(String value) throws DAOException {
        ResultSet resultSet = null;
        ArrayList<Movie> moviesList = null;
        Connection connection = POOL.takeConnection();
        try( PreparedStatement preparedStatement = connection.prepareStatement(SQL_SEARCH)) {
            preparedStatement.setString(1, "%" + value + "%");
            preparedStatement.setString(2, "%" + value + "%");
            preparedStatement.setString(3, "%" + value + "%");
            resultSet = preparedStatement.executeQuery();
            moviesList = initMovies(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't search a row which contains this string: " + value, e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return moviesList;
    }

    @Override
    public void updateMovieInformation(Movie movie) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_MOVIE)){
            preparedStatement.setString(1, movie.getTitle());
            preparedStatement.setString(2, movie.getOriginalTitle());
            preparedStatement.setDate(3, movie.getWorldPremiere());
            preparedStatement.setInt(4, movie.getBudget());
            preparedStatement.setInt(5, movie.getAgeLimit());
            preparedStatement.setString(6, movie.getBriefDescription());
            preparedStatement.setString(7, movie.getPosterURL());
            preparedStatement.setString(8, movie.getTrailerURL());
            preparedStatement.setLong(9, movie.getMovieId());
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while updating actor info in a database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    @Override
    public void addMovieToFavourites(long userId, long movieId) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_TO_FAVOURITES)){
            preparedStatement.setLong(1, movieId);
            preparedStatement.setLong(2, userId);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while adding favourite movie to a database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    @Override
    public void removeMovieFromFavourites(long userId, long movieId) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_FROM_FAVOURITES)){
            preparedStatement.setLong(1, movieId);
            preparedStatement.setLong(2, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException occurred while deleting favourite movie with id: " + movieId
                    + " from a database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    @Override
    public ArrayList<Movie> showTopRated(int limit) throws DAOException {
        ResultSet resultSet = null;
        ArrayList<Movie> moviesList = null;
        Connection connection = POOL.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_TOP_RATED)) {
            preparedStatement.setInt(1, limit);
            resultSet = preparedStatement.executeQuery();
            moviesList = initMovies(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize top movies list",e);
        }finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return moviesList;
    }

    @Override
    public ArrayList<Movie> showMostDiscussed(int startRecordNumber, int endRecordNumber) throws DAOException {
        ResultSet resultSet = null;
        ArrayList<Movie> moviesList = null;
        Connection connection = POOL.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_MOST_DISCUSSED)) {
            preparedStatement.setInt(1, startRecordNumber);
            preparedStatement.setInt(2, endRecordNumber);
            resultSet = preparedStatement.executeQuery();
            moviesList = initMovies(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize most discussed movies list",e);
        }finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return moviesList;
    }



    @Override
    public ArrayList<Movie> viewWatchList(long userId, int offset, int numberOfRecords) throws DAOException {
        ResultSet resultSet = null;
        ArrayList<Movie> watchlist = null;
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_VIEW_WATCHLIST);) {
            preparedStatement.setLong(1, userId);
            preparedStatement.setInt(2, offset);
            preparedStatement.setInt(3, numberOfRecords);
            resultSet = preparedStatement.executeQuery();
            watchlist = initMovies(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't view watchlist for user:  " + userId, e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return watchlist;


    }

    @Override
    public ArrayList<Movie> showMoviesForPerson(long personId) throws DAOException {
        ResultSet resultSet = null;
        ArrayList<Movie> moviesList = new ArrayList<>();
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_SHOW_MOVIES_FOR_PERSON)) {
            preparedStatement.setLong(1, personId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Movie movie = new Movie();
                movie.setMovieId(resultSet.getLong(1));
                movie.setTitle(resultSet.getString(2));
                movie.setOriginalTitle(resultSet.getString(3));
                movie.setWorldPremiere(resultSet.getDate(4));
                movie.setBudget(resultSet.getInt(5));
                movie.setAgeLimit(resultSet.getInt(6));
                movie.setBriefDescription(resultSet.getString(7));
                movie.setPosterURL(resultSet.getString(8));
                movie.setTrailerURL(resultSet.getString(9));

                moviesList.add(movie);
            }
        } catch (SQLException e) {
            throw new DAOException("Can't view movies list for person:  " + personId, e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return moviesList;
    }


    private ArrayList<Movie> initMovies(ResultSet resultSet) throws SQLException {
        ArrayList<Movie> moviesList = new ArrayList<>();
        logger.info("Initializing movies in OracleMovieDAO");
        while (resultSet.next()) {
            Movie movie = new Movie();
            movie.setMovieId(resultSet.getLong(1));
            movie.setTitle(resultSet.getString(2));
            movie.setOriginalTitle(resultSet.getString(3));
            movie.setWorldPremiere(resultSet.getDate(4));
            movie.setBudget(resultSet.getInt(5));
            movie.setAgeLimit(resultSet.getInt(6));
            movie.setBriefDescription(resultSet.getString(7));
            movie.setPosterURL(resultSet.getString(8));
            movie.setTrailerURL(resultSet.getString(9));

            moviesList.add(movie);
        }
        return moviesList;
    }

    private Movie initMovieItem(ResultSet resultSet) throws SQLException {
        Movie movie = new Movie();
        logger.info("Initializing a single movie in OracleMovieDAO");
        while (resultSet.next()) {
            movie.setMovieId(resultSet.getLong(1));
            movie.setTitle(resultSet.getString(2));
            movie.setOriginalTitle(resultSet.getString(3));
            movie.setWorldPremiere(resultSet.getDate(4));
            movie.setBudget(resultSet.getInt(5));
            movie.setAgeLimit(resultSet.getInt(6));
            movie.setBriefDescription(resultSet.getString(7));
            movie.setPosterURL(resultSet.getString(8));
            movie.setTrailerURL(resultSet.getString(9));
        }
        return movie;
    }

}
