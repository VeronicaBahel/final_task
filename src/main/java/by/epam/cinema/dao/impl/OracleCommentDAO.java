package by.epam.cinema.dao.impl;

import by.epam.cinema.dao.DAOException;
import by.epam.cinema.dao.ICommentDAO;
import by.epam.cinema.pool.ConnectionPool;
import by.epam.cinema.entity.Comment;
import by.epam.cinema.entity.Movie;
import by.epam.cinema.entity.User;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Class {@code OracleCommentDAO} is the class, that implements {@code ICommentDAO} interface
 * to deal with operations with comments in oracle database
 * @author Veronica Bahel
 */
public class OracleCommentDAO implements ICommentDAO {

    private static Logger logger = Logger.getLogger(OracleCommentDAO.class);
    private static final ConnectionPool POOL = ConnectionPool.getInstance();

    private static final String SQL_ADD_COMMENT = "INSERT INTO COMMENTS(comment_id, user_id," +
            "comment_content, comment_date, comment_movie_id) VALUES(?,?,?,?,?)";


    private static final String SQL_SHOW_MOVIE_COMMENTS = "SELECT outer.comment_id, outer.comment_content, " +
            "outer.comment_date, outer.comment_movie_id, USERS.user_id, USERS.user_login, USERS.user_imageurl " +
            " FROM (SELECT ROWNUM rn, inner.* " +
            "      FROM (SELECT * FROM COMMENTS) inner) outer " +
            " JOIN USERS ON USERS.user_id = outer.user_id " +
            " JOIN MOVIES ON MOVIES.movie_id = outer.comment_movie_id " +
            " WHERE movie_id = ? AND (rn BETWEEN ? AND ?) ";

    private static final String SQL_DELETE_COMMENT = "DELETE FROM COMMENTS WHERE comment_id = ?";

    public static final String SQL_SHOW_USER_COMMENTS = "SELECT outer.comment_id, outer.comment_content, " +
            "outer.comment_date, outer.comment_movie_id, USERS.user_id, MOVIES.movie_title " +
            " FROM (SELECT ROWNUM rn, inner.* " +
            "            FROM (SELECT * FROM COMMENTS) inner) outer " +
            " JOIN USERS ON USERS.user_id = outer.user_id " +
            " JOIN MOVIES ON MOVIES.movie_id = outer.comment_movie_id " +
            " WHERE USERS.user_id = ? AND (rn BETWEEN ? AND ?) " +
            " ORDER BY outer.comment_date DESC";

    public static final String SQL_FIND_COMMENT_BY_ID = "SELECT COMMENTS.comment_id, COMMENTS.user_id, " +
            "COMMENTS.comment_content, COMMENTS.comment_date, COMMENTS.comment_movie_id " +
            "FROM COMMENTS " +
            "WHERE COMMENTS.comment_id = ?";


    @Override
    public Map<User, Comment> showMovieComments(long movieId, int startRecordNumber, int endRecordNumber) throws DAOException {
        ResultSet resultSet = null;
        Map<User, Comment> movieComments = null;
        Connection connection = POOL.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SHOW_MOVIE_COMMENTS);) {
            preparedStatement.setLong(1, movieId);
            preparedStatement.setInt(2, startRecordNumber);
            preparedStatement.setInt(3, endRecordNumber);
            resultSet = preparedStatement.executeQuery();
            movieComments = initMovieComments(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize comments ",e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return movieComments;
    }

    @Override
    public Map<Movie, Comment> showUserComments(long userId, int startRecordNumber, int endRecordNumber) throws DAOException {
        ResultSet resultSet = null;
        Map<Movie, Comment> userComments = null;
        Connection connection = POOL.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SHOW_USER_COMMENTS);) {
            preparedStatement.setLong(1, userId);
            preparedStatement.setInt(2, startRecordNumber);
            preparedStatement.setInt(3, endRecordNumber);
            resultSet = preparedStatement.executeQuery();
            userComments = initUserComments(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize comments ",e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return userComments;
    }

    @Override
    public Comment findCommentById(long commentId) throws DAOException {
        ResultSet resultSet = null;
        Comment particularComment = null;
        Connection connection = POOL.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_COMMENT_BY_ID);) {
            preparedStatement.setLong(1, commentId);
            resultSet = preparedStatement.executeQuery();
            particularComment = initComment(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize comment ",e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return particularComment;
    }


    @Override
    public void addComment(Comment comment) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_COMMENT);){
            preparedStatement.setLong(1, comment.getCommentId());
            preparedStatement.setLong(2, comment.getUserId());
            preparedStatement.setString(3, comment.getContent());
            preparedStatement.setDate(4, Date.valueOf(comment.getDate().toString()));
            preparedStatement.setLong(5, comment.getMovieId());
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while adding comment to a database ", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    @Override
    public void deleteComment(long commentId) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT);){
            preparedStatement.setLong(1, commentId);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while deleting a comment with id: " + commentId +
                    " from a database ", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }


    private Comment initComment(ResultSet resultSet) throws SQLException {
        Comment comment = new Comment();
        logger.info("Initializing a single comment in OracleCommentDAO");
        while (resultSet.next()) {
            comment.setCommentId(resultSet.getLong(1));
            comment.setUserId(resultSet.getLong(2));
            comment.setContent(resultSet.getString(3));
            comment.setDate(resultSet.getDate(4));
            comment.setMovieId(resultSet.getLong(5));
        }
        return comment;
    }


    private Map<User, Comment> initMovieComments(ResultSet resultSet) throws SQLException {
        Map<User, Comment> movieComments = new HashMap<User, Comment>();
        logger.info("Initializing movie comments in OracleCommentDAO");
        while (resultSet.next()) {
            Comment comment = new Comment();
            comment.setCommentId(resultSet.getLong(1));
            comment.setContent(resultSet.getString(2));
            comment.setDate(Date.valueOf(resultSet.getDate(3).toString()));
            comment.setMovieId(resultSet.getLong(4));

            User user = new User();
            user.setUserId(resultSet.getLong(5));
            user.setLogin(resultSet.getString(6));
            user.setImageURL(resultSet.getString(7));
            movieComments.put(user, comment);
        }
        return movieComments;
    }


    private Map<Movie, Comment> initUserComments(ResultSet resultSet) throws SQLException {
        Map<Movie, Comment> userComments = new HashMap<>();
        logger.info("Initializing user's comments in OracleCommentDAO");
        while (resultSet.next()) {
            Comment comment = new Comment();
            comment.setCommentId(resultSet.getLong(1));
            comment.setContent(resultSet.getString(2));
            comment.setDate(Date.valueOf(resultSet.getDate(3).toString()));
            comment.setMovieId(resultSet.getLong(4));
            comment.setUserId(resultSet.getLong(5));

            Movie movie = new Movie();
            movie.setMovieId(resultSet.getLong(4));
            movie.setTitle(resultSet.getString(6));

            userComments.put(movie, comment);


        }
        return userComments;
    }



}
