package by.epam.cinema.dao.impl;

import by.epam.cinema.dao.DAOException;
import by.epam.cinema.entity.Movie;
import by.epam.cinema.pool.ConnectionPool;
import by.epam.cinema.dao.IRatingDAO;
import by.epam.cinema.entity.Rating;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Class {@code OracleRatingDAO} is the class, that implements {@code IRatingDAO} interface
 * to deal with operations with ratings in oracle database
 * @author Veronica Bahel
 */
public class OracleRatingDAO implements IRatingDAO {

    private static Logger logger = Logger.getLogger(OracleRatingDAO.class);
    private static final ConnectionPool POOL = ConnectionPool.getInstance();


    private static final String SQL_ADD_RATING = "INSERT INTO RATINGS(user_id, movie_id, rating_value, rating_date)" +
            "VALUES(?,?,?,?)";


    private static final String SQL_DELETE_RATING = "DELETE FROM RATINGS WHERE user_id = ? AND movie_id = ?";

    private static final String SQL_SHOW_AVERAGE_RATING = "SELECT ROUND(AVG(rating_value), 1) " +
            "FROM RATINGS WHERE movie_id = ?";

    public static final String SQL_SHOW_USER_RATINGS = "SELECT outer.rating_value, " +
            " outer.movie_id, MOVIES.movie_title, MOVIES.movie_poster_url " +
            " FROM (SELECT ROWNUM rn, inner.* " +
            "      FROM (SELECT * FROM RATINGS) inner) outer " +
            " JOIN USERS ON USERS.user_id = outer.user_id " +
            " JOIN MOVIES ON MOVIES.movie_id = outer.movie_id " +
            " WHERE USERS.user_id = ? AND (rn BETWEEN ? AND ?) " +
            " ORDER BY outer.rating_date DESC ";

    private static final String SQL_RETRIEVE_RATING = "SELECT RATINGS.rating_value " +
            "FROM RATINGS " +
            "WHERE RATINGS.movie_id = ?  AND RATINGS.user_id = ?";

    @Override
    public void addRating(Rating rating) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_RATING);){
            preparedStatement.setLong(1, rating.getUserId());
            preparedStatement.setLong(2, rating.getMovieId());
            preparedStatement.setInt(3, rating.getValue());
            preparedStatement.setDate(4, Date.valueOf(rating.getDate().toString()));
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while adding rating to a database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }


    @Override
    public void deleteRating(long userId, long movieId) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_RATING);){
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, movieId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException occurred while deleting rating from user with id: " + userId
                    + " from a database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    @Override
    public double showAverageRating(long movieId) throws DAOException {
        ResultSet resultSet = null;
        double averageRating = 0;
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_SHOW_AVERAGE_RATING);){
            preparedStatement.setLong(1, movieId);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                averageRating = resultSet.getDouble(1);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException occurred while getting average rating on movie: " + movieId +
                    " from a database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return averageRating;
    }

    @Override
    public int retrieveRatingValue(long userId, long movieId) throws DAOException {
        ResultSet resultSet = null;
        int ratingValue = 0;
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_RETRIEVE_RATING);){
            preparedStatement.setLong(1, movieId);
            preparedStatement.setLong(2, userId);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                ratingValue = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException occurred while getting average rating on movie: " + movieId +
                    " from a database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return ratingValue;
    }

    @Override
    public Map<Movie, Rating> showUserRatings(long userId, int startRecordNumber, int endRecordNumber) throws DAOException {
        ResultSet resultSet = null;
        Map<Movie, Rating> userRates = null;
        Connection connection = POOL.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SHOW_USER_RATINGS);) {
            preparedStatement.setLong(1, userId);
            preparedStatement.setInt(2, startRecordNumber);
            preparedStatement.setInt(3, endRecordNumber);
            resultSet = preparedStatement.executeQuery();
            userRates = initUserRatings(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize ratings ",e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return userRates;
    }

    private Map<Movie, Rating> initUserRatings(ResultSet resultSet) throws SQLException {
        Map<Movie, Rating> userRatings = new HashMap<>();
        logger.info("Initializing user's crates in OracleRatingDAO");
        while (resultSet.next()) {
            Rating rating = new Rating();
            rating.setValue(resultSet.getInt(1));
            rating.setMovieId(resultSet.getLong(2));

            Movie movie = new Movie();
            movie.setMovieId(resultSet.getLong(2));
            movie.setTitle(resultSet.getString(3));
            movie.setPosterURL(resultSet.getString(4));

            userRatings.put(movie, rating);
        }
        return userRatings;
    }


}
