package by.epam.cinema.dao.impl;

import by.epam.cinema.dao.DAOException;
import by.epam.cinema.dao.IUserDAO;
import by.epam.cinema.pool.ConnectionPool;
import by.epam.cinema.entity.User;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class {@code OracleUserDAO} is the class, that implements {@code IUserDAO} interface
 * to deal with operations with users in database
 * @author Veronica Bahel
 */
public class OracleUserDAO implements IUserDAO {

    private static Logger logger = Logger.getLogger(OracleUserDAO.class);
    private static final ConnectionPool POOL = ConnectionPool.getInstance();



    private static final String SQL_ADD_USER =
            "INSERT INTO USERS(user_firstName, user_lastName, user_email, user_login," +
                    " user_password, user_imageURL, user_id, user_role, user_location, user_birthday) VALUES (?,?,?,?,?,?,?,?,?,?)";

    private static final String SQL_FIND_USER_BY_LOGIN = "SELECT user_firstName, user_lastName, user_email, user_login, " +
            " user_password, user_imageURL, user_location, user_id, user_role, user_birthday " +
            " FROM USERS WHERE USER_LOGIN = ? OR USER_EMAIL = ?";

    private static final String SQL_FIND_USER_BY_ID = "SELECT user_firstName, user_lastName, user_email, user_login, " +
            "  user_password, user_imageURL, user_location, user_id, user_role, user_birthday FROM USERS WHERE USER_ID = ?";


    private static final String SQL_UPDATE_USER_INFO = "UPDATE USERS SET user_firstname = ?, user_lastname = ?," +
            "user_email = ?, user_imageurl = ?, user_location = ?, user_birthday = ? WHERE user_id = ?";

    private static final String SQL_DELETE_PROFILE_PICTURE = "UPDATE USERS SET USER_IMAGEURL = ? WHERE USER_ID = ?";

    private static final String SQL_ASSIGN_ROLE = "UPDATE USERS SET user_role = ? WHERE user_id = ? ";

    private static final String SQL_VIEW_USER_TABLE = "SELECT USERS.user_id, USERS.user_login, USERS.user_role, " +
            "( SELECT COUNT(COMMENTS.user_id) FROM COMMENTS WHERE COMMENTS.user_id = USERS.user_id ), " +
            "( SELECT COUNT(RATINGS.user_id) FROM RATINGS WHERE RATINGS.user_id = USERS.user_id ) " +
            " FROM USERS ";

    private static final String SQL_DELETE_PROFILE = "DELETE FROM USERS WHERE user_id = ? ";




    @Override
    public void addUser(User user) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_USER);){
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getLogin());
            preparedStatement.setString(5, user.getPassword());
            preparedStatement.setString(6, user.getImageURL());
            preparedStatement.setLong(7, user.getUserId());
            preparedStatement.setString(8, user.getRole());
            preparedStatement.setString(9, user.getLocation());
            preparedStatement.setDate(10, user.getBirthday());
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while adding user to a database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }



    @Override
    public User findUser(String userLogin) throws DAOException {
        ResultSet resultSet = null;
        User particularUser = null;
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_USER_BY_LOGIN);) {
            preparedStatement.setString(1, userLogin);
            preparedStatement.setString(2, userLogin);
            resultSet = preparedStatement.executeQuery();
            particularUser = initSingleUser(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Cant' find user with such login ", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return particularUser;
    }

    @Override
    public User findUserById(long userId) throws DAOException {
        ResultSet resultSet = null;
        User particularUser = null;
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_USER_BY_ID);) {
            preparedStatement.setLong(1, userId);
            resultSet = preparedStatement.executeQuery();
            particularUser = initSingleUser(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Cant' find user with such id ", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return particularUser;
    }

    @Override
    public void updateUserInfo(User user) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_USER_INFO);){
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getImageURL());
            preparedStatement.setString(5, user.getLocation());
            preparedStatement.setDate(6, user.getBirthday());
            preparedStatement.setLong(7, user.getUserId());
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while updating user info in a database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    @Override
    public void deleteProfilePicture(long userId, String defaultImage) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_PROFILE_PICTURE);){
            preparedStatement.setString(1, defaultImage);
            preparedStatement.setLong(2, userId);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while setting default user image in a database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    @Override
    public void assignRoleToUser(long userId, String role) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_ASSIGN_ROLE)){
            preparedStatement.setString(1, role);
            preparedStatement.setLong(2, userId);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while banning user in a database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    @Override
    public ArrayList<HashMap<User, ArrayList<Integer>>> showUserTable() throws DAOException {
        ResultSet resultSet = null;
        ArrayList<HashMap<User, ArrayList<Integer>>> userList = null;
        Connection connection = POOL.takeConnection();
        try (Statement statement = connection.createStatement();) {
            resultSet = statement.executeQuery(SQL_VIEW_USER_TABLE);
            userList = initUserTable(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize user table",e);
        }finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return userList;
    }

    @Override
    public void deleteProfile(Long userId) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_PROFILE);){
            preparedStatement.setLong(1, userId);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while deleting a comment with id: " + userId +
                    " from a database ", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    private User initSingleUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        logger.info("Initializing a single user in OracleUserDAO");
        while (resultSet.next()) {
            user.setFirstName(resultSet.getString(1));
            user.setLastName(resultSet.getString(2));
            user.setEmail(resultSet.getString(3));
            user.setLogin(resultSet.getString(4));
            user.setPassword(resultSet.getString(5));
            user.setImageURL(resultSet.getString(6));
            user.setLocation(resultSet.getString(7));
            user.setUserId(resultSet.getLong(8));
            user.setRole(resultSet.getString(9));
            user.setBirthday(resultSet.getDate(10));
        }
        return user;
    }

    private ArrayList<HashMap<User, ArrayList<Integer>>> initUserTable(ResultSet resultSet) throws SQLException {
        ArrayList<HashMap<User, ArrayList<Integer>>> userList = new ArrayList<>();
        logger.info("Initializing user table in OracleUserDAO");
        while (resultSet.next()) {
            User user = new User();
            ArrayList<Integer> statisticValues = new ArrayList<>();
            HashMap<User, ArrayList<Integer>> statistic = new HashMap<>();

            user.setUserId(resultSet.getLong(1));
            user.setLogin(resultSet.getString(2));
            user.setRole(resultSet.getString(3));

            statisticValues.add(resultSet.getInt(4));
            statisticValues.add(resultSet.getInt(5));

            statistic.put(user, statisticValues);

            userList.add(statistic);
        }
        return userList;
    }


}
