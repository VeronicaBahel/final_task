package by.epam.cinema.dao.impl;


import by.epam.cinema.dao.DAOException;
import by.epam.cinema.pool.ConnectionPool;
import by.epam.cinema.dao.IPersonDAO;
import by.epam.cinema.entity.Person;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

/**
 * Class {@code OraclePersonDAO} is the class, that implements {@code IPersonDAO} interface
 * to deal with operations with people in oracle database
 * @author Veronica Bahel
 */
public class OraclePersonDAO implements IPersonDAO {

    private static Logger logger = Logger.getLogger(OraclePersonDAO.class);
    private static final ConnectionPool POOL = ConnectionPool.getInstance();

    private static final String SQL_ADD_PERSON =
            "INSERT INTO PEOPLE(person_id, person_first_name, person_last_name, person_birthday, person_birth_place, " +
                    " person_picture_url) VALUES (?,?,?,?,?,?)";

    private static final String SQL_FIND_PERSON_BY_ID = "SELECT person_id, person_first_name, person_last_name, " +
            "person_birthday, person_birth_place, person_picture_url FROM PEOPLE WHERE person_id = ?";

    private static final String SQL_SHOW_PEOPLE = "SELECT outer.person_id, outer.person_first_name, " +
            "outer.person_last_name, outer.person_birthday, outer.person_birth_place, outer.person_picture_url " +
            " FROM(  SELECT ROWNUM rn, inner.* " +
            "       FROM (SELECT * FROM PEOPLE) inner ) outer " +
            " WHERE rn BETWEEN ? AND ? ";

    private static final String SQL_UPDATE_PERSON_INFO = "UPDATE PEOPLE SET person_first_name=?, person_last_name=?," +
            " person_birthday=?, person_birth_place=?, person_picture_url=? WHERE person_id=?";

    private static final String SQL_ADD_PERSON_FOR_THE_MOVIE = "INSERT INTO PEOPLE_MOVIES (person_id, movie_id, person_role)" +
            "VALUES (?,?,?)";

    private static final String SQL_SHOW_PEOPLE_FOR_MOVIE = "SELECT PEOPLE.person_id, PEOPLE.person_first_name, " +
            "  PEOPLE.person_last_name, PEOPLE.person_birthday, PEOPLE.person_birth_place, " +
            " PEOPLE.person_picture_url, PEOPLE_MOVIES.person_role  " +
            " FROM PEOPLE JOIN PEOPLE_MOVIES ON PEOPLE_MOVIES.person_id = PEOPLE.person_id " +
            " WHERE PEOPLE_MOVIES.movie_id = ?";

    private static final String SQL_DELETE_MOVIE_PEOPLE = "DELETE FROM PEOPLE_MOVIES WHERE PEOPLE_MOVIES.movie_id = ?";

    private static final String SQL_BORN_TODAY = "SELECT PEOPLE.person_id, PEOPLE.person_first_name, " +
            "          PEOPLE.person_last_name, PEOPLE.person_birthday, PEOPLE.person_birth_place, PEOPLE.person_picture_url " +
            "FROM PEOPLE " +
            "WHERE EXTRACT(DAY FROM PEOPLE.person_birthday) = ? AND EXTRACT(MONTH FROM PEOPLE.person_birthday) = ?";


    @Override
    public long addPerson(Person person) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_PERSON,
                new String[]{"PERSON_ID"})){
            preparedStatement.setLong(1, person.getPersonId());
            preparedStatement.setString(2, person.getFirstName());
            preparedStatement.setString(3, person.getLastName());
            preparedStatement.setDate(4, Date.valueOf(person.getBirthday().toString()));
            preparedStatement.setString(5, person.getBirthPlace());
            preparedStatement.setString(6, person.getPictureURL());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            rs.next();
            int generatedId = rs.getInt(1);
            return generatedId;
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while adding person to the database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    @Override
    public ArrayList<Person> showPeople(int startRecordNumber, int endRecordNumber) throws DAOException {

        ArrayList<Person> peopleList = null;
        Connection connection = POOL.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SHOW_PEOPLE);) {
            preparedStatement.setInt(1, startRecordNumber);
            preparedStatement.setInt(2, endRecordNumber);
            ResultSet resultSet = preparedStatement.executeQuery();
            peopleList = initAllPeople(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize people list",e);
        }finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return peopleList;
    }

    @Override
    public ArrayList<Person> showBornToday(int day, int month) throws DAOException {
        ArrayList<Person> peopleList = null;
        Connection connection = POOL.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_BORN_TODAY);) {
            preparedStatement.setInt(1, day);
            preparedStatement.setInt(2, month);
            ResultSet resultSet = preparedStatement.executeQuery();
            peopleList = initAllPeople(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize people list",e);
        }finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return peopleList;
    }

    @Override
    public ArrayList<Person> showPeopleForTheMovie(long movieId) throws DAOException {

        ArrayList<Person> peopleList = null;
        Connection connection = POOL.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SHOW_PEOPLE_FOR_MOVIE)) {
            preparedStatement.setLong(1, movieId);
            ResultSet resultSet = preparedStatement.executeQuery();
            peopleList = initMoviePeople(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize people ",e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return peopleList;
    }

    @Override
    public void addPeopleToTheMovie(long personId, long movieId, String role) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_PERSON_FOR_THE_MOVIE);){
            preparedStatement.setLong(1, personId);
            preparedStatement.setLong(2, movieId);
            preparedStatement.setString(3, role);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while adding person with id: " + personId +
                    " to the movie with id: " + movieId, e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    @Override
    public void deleteMoviePeople(long movieId) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_MOVIE_PEOPLE)){
            preparedStatement.setLong(1, movieId);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while deleting people for the movie with id: " + movieId, e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    @Override
    public Person findPersonById(long personId) throws DAOException {
        ResultSet resultSet = null;
        Person personItem = new Person();
        Connection connection = POOL.takeConnection();
        try( PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_PERSON_BY_ID)) {
            preparedStatement.setLong(1, personId);
            resultSet = preparedStatement.executeQuery();
            personItem = initPersonItem(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't find person with id: " + personId);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return personItem;
    }

    @Override
    public void updatePersonInfo(Person person) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_PERSON_INFO);){
            preparedStatement.setString(1, person.getFirstName());
            preparedStatement.setString(2, person.getLastName());
            preparedStatement.setDate(3, Date.valueOf(person.getBirthday().toString()));
            preparedStatement.setString(4, person.getBirthPlace());
            preparedStatement.setString(5, person.getPictureURL());
            preparedStatement.setLong(6, person.getPersonId());
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while updating person info in the database", e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    private Person initPersonItem(ResultSet resultSet) throws SQLException {
        Person person = new Person();
        logger.info("Initializing a single person in OraclePersonDAO");
        while (resultSet.next()) {
            person.setPersonId(resultSet.getLong(1));
            person.setFirstName(resultSet.getString(2));
            person.setLastName(resultSet.getString(3));
            person.setBirthday(resultSet.getDate(4));
            person.setBirthPlace(resultSet.getString(5));
            person.setPictureURL(resultSet.getString(6));
        }
        return person;
    }


    private ArrayList<Person> initAllPeople(ResultSet resultSet) throws SQLException {
        ArrayList<Person> peopleList = new ArrayList<Person>();
        logger.info("Initializing people in OraclePersonDAO");
        while (resultSet.next()) {
            Person person = new Person();
            person.setPersonId(resultSet.getLong(1));
            person.setFirstName(resultSet.getString(2));
            person.setLastName(resultSet.getString(3));
            person.setBirthday(resultSet.getDate(4));
            person.setBirthPlace(resultSet.getString(5));
            person.setPictureURL(resultSet.getString(6));
            peopleList.add(person);
        }
        return peopleList;
    }

    private ArrayList<Person> initMoviePeople(ResultSet resultSet) throws SQLException {
        ArrayList<Person> peopleList = new ArrayList<Person>();
        logger.info("Initializing people participating in a certain movie in OraclePersonDAO");
        while (resultSet.next()) {
            Person person = new Person();
            person.setPersonId(resultSet.getLong(1));
            person.setFirstName(resultSet.getString(2));
            person.setLastName(resultSet.getString(3));
            person.setBirthday(resultSet.getDate(4));
            person.setBirthPlace(resultSet.getString(5));
            person.setPictureURL(resultSet.getString(6));
            person.setRole(resultSet.getString(7));
            peopleList.add(person);
        }
        return peopleList;
    }
}
