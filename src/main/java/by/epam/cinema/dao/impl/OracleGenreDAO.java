package by.epam.cinema.dao.impl;

import by.epam.cinema.dao.DAOException;
import by.epam.cinema.dao.IGenreDAO;
import by.epam.cinema.pool.ConnectionPool;
import by.epam.cinema.entity.Genre;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

/**
 * Class {@code OracleGenreDAO} is the class, that implements {@code IGenreDAO} interface
 * to deal with operations with genres in oracle database
 * @author Veronica Bahel
 */
public class OracleGenreDAO implements IGenreDAO {

    private static Logger logger = Logger.getLogger(OracleGenreDAO.class);
    private static final ConnectionPool POOL = ConnectionPool.getInstance();

    private static final String SQL_SHOW_MOVIE_GENRES = "SELECT GENRES.genre_id, GENRES.genre_label " +
            "FROM GENRES JOIN MOVIE_GENRES ON MOVIE_GENRES.genre_id = GENRES.genre_id " +
            "WHERE MOVIE_GENRES.movie_id = ?";

    private static final String SQL_ADD_GENRE_FOR_THE_MOVIE = "INSERT INTO MOVIE_GENRES (movie_id, genre_id)" +
            "VALUES (?,?)";

    private static final String SQL_SHOW_ALL_GENRES = "SELECT genre_id, genre_label  FROM GENRES";

    private static final String SQL_DELETE_MOVIE_GENRES = "DELETE FROM MOVIE_GENRES WHERE MOVIE_GENRES.movie_id = ?";

    private static final String SQL_FIND_GENRE_BY_LABEL = "SELECT GENRES.genre_id, GENRES.genre_label " +
            " FROM GENRES " +
            " WHERE GENRES.genre_label = ?";


    @Override
    public ArrayList<Genre> showMovieGenres(long movieId) throws DAOException {
        ResultSet resultSet = null;
        ArrayList<Genre> genresList = null;
        Connection connection = POOL.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SHOW_MOVIE_GENRES);) {
            preparedStatement.setLong(1, movieId);
            resultSet = preparedStatement.executeQuery();
            genresList = initGenres(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize genres ",e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return genresList;
    }

    @Override
    public ArrayList<Genre> showAllGenres() throws DAOException {
        ResultSet resultSet = null;
        ArrayList<Genre> genresList = null;
        Connection connection = POOL.takeConnection();
        try (Statement statement = connection.createStatement();) {
            resultSet = statement.executeQuery(SQL_SHOW_ALL_GENRES);
            genresList = initGenres(resultSet);
        } catch (SQLException e) {
            throw new DAOException("Can't initialize genres list",e);
        }finally{
            if (connection != null){
                POOL.returnConnection(connection);

            }
        }
        return genresList;
    }

    @Override
    public void addGenreToTheMovie(long movieId, long genreId) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_GENRE_FOR_THE_MOVIE);){
            preparedStatement.setLong(1, movieId);
            preparedStatement.setLong(2, genreId);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while adding genre with id: " + genreId +
                    "to the movie with id: " + movieId, e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    @Override
    public void deleteMovieGenres(long movieId) throws DAOException {
        Connection connection = POOL.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_MOVIE_GENRES);){
            preparedStatement.setLong(1, movieId);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException("SQLException occurred while deleting genres for the movie with id: " + movieId, e);
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
    }

    @Override
    public Genre findGenreByLabel(String label) throws DAOException {
        ResultSet resultSet = null;
        Genre genre = new Genre();
        Connection connection = POOL.takeConnection();
        try( PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_GENRE_BY_LABEL)) {
            preparedStatement.setString(1, label);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                genre.setGenreId(resultSet.getLong(1));
                genre.setLabel(resultSet.getString(2));
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally{
            if (connection != null){
                POOL.returnConnection(connection);
            }
        }
        return genre;
    }

    private ArrayList<Genre> initGenres(ResultSet resultSet) throws SQLException{
        ArrayList<Genre> genresList = new ArrayList<Genre>();
        logger.info("Initializing genres in OracleGenreDAO");
        while (resultSet.next()) {
            Genre genre = new Genre();
            genre.setGenreId(resultSet.getLong(1));
            genre.setLabel(resultSet.getString(2));
            genresList.add(genre);
        }
        return genresList;
    }


}
