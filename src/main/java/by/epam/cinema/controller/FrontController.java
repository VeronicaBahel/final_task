package by.epam.cinema.controller;

import by.epam.cinema.command.CommandEnum;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.CommandException;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.pool.ConnectionPool;
import by.epam.cinema.pool.ConnectionPoolException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class {@code FrontController} is the class, that extends {@code HttpServlet} class to
 * deal with user operations and forwarding user to a particular page.
 * @author Veronica Bahel
 */
@WebServlet("/controller")
@MultipartConfig
public class FrontController extends HttpServlet{

    private static Logger logger = Logger.getLogger(FrontController.class);

    /**
     * <p>Starts when servlet is processing GET request.</p>
     * @param request is a request we are processing
     * @param response is a response we are creating
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page = null;
        try {
            String action = request.getParameter(ParameterConst.PARAMETER_COMMAND);
            ICommand command = CommandHelper.defineCommand(action);
            page = command.execute(request);
            request.getServletContext().getRequestDispatcher(page).forward(request, response);
        } catch (CommandException e) {
            logger.error("Exception occurred while executing a command", e);
            page = PageConst.PAGE_GENERAL_ERROR;
            request.getServletContext().getRequestDispatcher(page).forward(request, response);
        }
    }

    /**
     * <p>Starts when servlet is processing POST request.</p>
     * @param request is a request we are processing
     * @param response is a response we are creating
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page = null;
        try {
            String action = request.getParameter(ParameterConst.PARAMETER_COMMAND);
            ICommand command = CommandHelper.defineCommand(action);
            page = command.execute(request);
            if (action.equalsIgnoreCase(CommandEnum.LOGIN.name()) || action.equalsIgnoreCase(CommandEnum.REGISTER.name()) ||
                    action.equalsIgnoreCase(CommandEnum.SEARCH.name())){
                request.getServletContext().getRequestDispatcher(page).forward(request, response);
            } else {
                response.sendRedirect( page);
            }
        } catch (CommandException e) {
            logger.error("Exception occurred while executing a command", e);
            page = PageConst.PAGE_GENERAL_ERROR;
            request.getServletContext().getRequestDispatcher(page).forward(request, response);
        }
    }



    @Override
    public void destroy(){
        try{
            ConnectionPool.getInstance().destroyPool();
        } catch (ConnectionPoolException e) {
            logger.error("Exception occurred while destroying ConnectionPool in FrontController ", e);
        }
    }


}
