package by.epam.cinema.controller;

import by.epam.cinema.command.CommandEnum;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.impl.common.EmptyCommand;
import org.apache.log4j.Logger;

/**
 * Class {@code CommandHelper} is the class, that deals with defining a particular command
 * @author Veronica Bahel
 */
public class CommandHelper {

    private static Logger logger = Logger.getLogger(CommandHelper.class);

    /**
     * <p>Defines which command to return based on it's name.</p>
     * @param action defines a command we need to process
     * @return concrete realisation of a command
     */
    public static ICommand defineCommand(String action){
        ICommand current = new EmptyCommand();

        logger.info("Processing command: " + action);
        if(action == null || action.isEmpty()){
            return current;
        }
        CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
        current = currentEnum.geCurrentCommand();
        return current;
    }
}
