package by.epam.cinema.command.constant;

/**
 * Class {@code PageConst} contains constants of pages in the system
 * @author Veronica Bahel
 */
public class PageConst {
    public static final String PAGE_GENERAL_ERROR = "/jsp/generalerror.jsp";
    public static final String PAGE_VIEW = "/jsp/view.jsp";
    public static final String PAGE_COMING_SOON = "/jsp/comingsoon.jsp";
    public static final String PAGE_ALL_MOVIES = "/jsp/allmovies.jsp";
    public static final String PAGE_TOP_10 = "/jsp/top.jsp";
    public static final String PAGE_BORN_TODAY = "/jsp/borntoday.jsp";
    public static final String PAGE_INDEX = "/index.jsp";
    public static final String PAGE_SIGN_IN_OR_UP = "/jsp/signinorup.jsp";
    public static final String PAGE_ADD_MOVIE = "/jsp/addmovie.jsp";
    public static final String PAGE_SINGLE_MOVIE = "/jsp/movie.jsp";
    public static final String PAGE_PEOPLE = "/jsp/people.jsp";
    public static final String PAGE_PERSON = "/jsp/person.jsp";
    public static final String PAGE_USER_PROFILE = "/jsp/profile.jsp";
    public static final String PAGE_UPDATE_USER_PROFILE = "/jsp/updateprofile.jsp";
    public static final String PAGE_ADD_PERSON = "/jsp/addperson.jsp";
    public static final String PAGE_COMMENTS = "/jsp/comments.jsp";
    public static final String PAGE_RATES = "/jsp/rates.jsp";
    public static final String PAGE_SEARCH_RESULTS = "/jsp/searchresults.jsp";
    public static final String PAGE_USER_TABLE = "/jsp/usertable.jsp";
    public static final String PAGE_WATCHLIST = "/jsp/watchlist.jsp";
}
