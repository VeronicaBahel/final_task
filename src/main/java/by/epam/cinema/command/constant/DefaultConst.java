package by.epam.cinema.command.constant;


/**
 * Class {@code DefaultConst} contains constants to be used in default situations
 * @author Veronica Bahel
 */
public class DefaultConst {
    public static final String DEFAULT_USER_PROFILE_PICTURE = "upload/defaultUserPic.png";
    public static final String DEFAULT_MOVIE_POSTER = "upload/defaultPoster.jpg";
    public static final String DEFAULT_PERSON_PICTURE = "upload/defaultActorPic.png";
    public static final String DEFAULT_VIEW_GENRE = "tvSeries";

    public static final int DEFAULT_START_PAGE_NUMBER = 1;
    public static final int DEFAULT_RECORDS_PER_PAGE_NUMBER = 10;

    public static final int DEFAULT_MAIN_LIMIT = 6;
    public static final int DEFAULT_TOP_LIMIT = 10;


}
