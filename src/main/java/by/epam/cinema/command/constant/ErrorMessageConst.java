package by.epam.cinema.command.constant;


/**
 * Class {@code ErrorMessageConst} contains constants to be used as error messages
 * @author Veronica Bahel
 */
public class ErrorMessageConst {
    
    public static final String MESSAGE_WRONG_LOGIN_OR_PASSWORD = "Oops! Your username or password don't match. Please, try again.";
    public static final String MESSAGE_NOT_DISTINCT_INPUT = "Oops! Looks like such username already exists. Please, try another one.";
    public static final String MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED = "Oops! It seems like you are not allowed to perform this operation.";
    public static final String MESSAGE_RATING_ALREADY_EXISTS = "You've already rated that movie!";
    public static final String MESSAGE_NO_ACCESSIBLE_RATING = "You haven't rated that movie yet!";
    public static final String MESSAGE_INVALID_DATA = "You've entered wrong data ";
    public static final String MESSAGE_LOGGED_IN = "You've already logged in";


}
