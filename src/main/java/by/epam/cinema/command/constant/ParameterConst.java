package by.epam.cinema.command.constant;

/**
 * Class {@code ParameterConst} contains constants to be received in request
 * @author Veronica Bahel
 */
public class ParameterConst {
    public static final String PARAMETER_COMMAND = "command";
    public static final String PARAMETER_USER_ID = "userId";
    public static final String PARAMETER_COMMENT_ID = "commentId";
    public static final String PARAMETER_GENRE = "genre";
    public static final String PARAMETER_PAGE = "page";
    public static final String PARAMETER_PREVIOUS_PAGE = "previousPage";
    public static final String PARAMETER_PAGE_NUMBER = "pageNumber";
    public static final String PARAMETER_DEFAULT_MAIN_GENRE = "series";

    public static final String PARAMETER_REGISTER_LOGIN = "registerLogin";
    public static final String PARAMETER_REGISTER_EMAIL = "registerEmail";
    public static final String PARAMETER_REGISTER_PASSWORD = "registerPassword";
    public static final String PARAMETER_LOGIN = "login";
    public static final String PARAMETER_EMAIL = "email";
    public static final String PARAMETER_PASSWORD = "password";
    public static final String PARAMETER_MOVIE_ID = "movieId";

    public static final String PARAMETER_TITLE = "title";
    public static final String PARAMETER_ORIGINAL_TITLE = "originalTitle";
    public static final String PARAMETER_GENRES = "genres";
    public static final String PARAMETER_ACTORS = "actors";
    public static final String PARAMETER_DIRECTORS = "directors";
    public static final String PARAMETER_COUNTRIES = "countries";
    public static final String PARAMETER_WORLD_PREMIERE = "worldPremiere";
    public static final String PARAMETER_BUDGET = "budget";
    public static final String PARAMETER_AGE_LIMIT = "ageLimit";
    public static final String PARAMETER_BRIEF_DESCRIPTION = "briefDescription";
    public static final String PARAMETER_POSTER_URL = "posterURL";
    public static final String PARAMETER_TRAILER_URL = "trailerURL";

    public static final String PARAMETER_COMMENT_CONTENT = "commentContent";
    public static final String PARAMETER_RATING = "rating";

    public static final String PARAMETER_PERSON_ID = "personId";
    public static final String PARAMETER_PERSON_FIRST_NAME = "personFirstName";
    public static final String PARAMETER_PERSON_LAST_NAME = "personLastName";
    public static final String PARAMETER_PERSON_BIRTHDAY = "personBirthday";
    public static final String PARAMETER_PERSON_BIRTH_PLACE = "personBirthPlace";
    public static final String PARAMETER_PERSON_PICTURE_URL = "personPictureURL";

    public static final String PARAMETER_USER_FIRST_NAME = "userFirstName";
    public static final String PARAMETER_USER_LAST_NAME = "userLastName";
    public static final String PARAMETER_USER_EMAIL = "userEmail";
    public static final String PARAMETER_USER_IMAGE = "userImageURL";
    public static final String PARAMETER_USER_LOCATION = "userLocation";
    public static final String PARAMETER_USER_BIRTHDAY = "userBirthday";

    public static final String PARAMETER_GENERAL = "parameter";

}
