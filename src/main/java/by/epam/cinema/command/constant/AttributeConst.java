package by.epam.cinema.command.constant;

/**
 * Class {@code AttributeConst} contains constants to be used as attributes
 * @author Veronica Bahel
 */
public class AttributeConst {

    public static final String ATTR_PERSON = "person";
    public static final String ATTR_PEOPLE = "people";
    public static final String ATTR_AMBIGUOUS_COMMAND = "ambiguousCommand";
    public static final String ATTR_MOVIES = "movies";
    public static final String ATTR_MOVIE = "singleMovie";
    public static final String ATTR_ERROR_MESSAGE = "errorMessage";
    public static final String ATTR_COMMENTS = "comments";
    public static final String ATTR_RATES = "rates";
    public static final String ATTR_LOGGED_USER = "loggedUser";
    public static final String ATTR_USER = "user";
    public static final String ATTR_GENRES = "genres";
    public static final String ATTR_TOP = "topMovies";
    public static final String ATTR_MOST_DISCUSSED = "mostDiscussed";
    public static final String ATTR_RATING = "loggedUserRating";
    public static final String ATTR_WATCHLIST = "watchlist";
    public static final String ATTR_STATISTIC = "userStatistic";
    public static final String ATTR_IS_IN_WATCHLIST = "inWatchlist";
    public static final String ATTR_PERSON_MOVIES = "personMovies";

    public static final String ATTR_USERS = "users";

    public static final String ATTR_CHECKED_GENRES = "checkedGenres";
    public static final String ATTR_CHECKED_PEOPLE = "checkedPeople";

    public static final String ATTR_NUMBER_OF_PAGES = "numberOfPages";
    public static final String ATTR_CURRENT_PAGE_NUMBER = "currentPageNumber";



}
