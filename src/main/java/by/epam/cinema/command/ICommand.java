package by.epam.cinema.command;

import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Interface {@code Command} provides behaviour for commands
 * @author Veronica Bahel
 */
public interface ICommand {
    /**
     * <p>Return a particular page. </p>
     * @param request is an incoming request
     * @return {@code String} url of necessary page.
     */
    String execute(HttpServletRequest request) throws CommandException;


    /**
     * <p>Determines whether or not logged user is an administrator. </p>
     * @param request is an incoming request
     * @return true if logged user is an admin, false otherwise
     */
    default boolean isAdmin(HttpServletRequest request){
        if (null != (User) request.getSession().getAttribute(AttributeConst.ATTR_LOGGED_USER)){
            String role = ((User) request.getSession().getAttribute(AttributeConst.ATTR_LOGGED_USER)).getRole();
            return role.equalsIgnoreCase(UserRole.ADMIN.name());
        }
        return false;
    }

    /**
     * <p>Determines whether or not logged user is a regular user. </p>
     * @param request is an incoming request
     * @return true if logged user is a regular user, false otherwise
     */
    default boolean isRegular(HttpServletRequest request){
        if (null != (User) request.getSession().getAttribute(AttributeConst.ATTR_LOGGED_USER)){
            String role = ((User) request.getSession().getAttribute(AttributeConst.ATTR_LOGGED_USER)).getRole();
            return UserRole.REGULAR.name().equalsIgnoreCase(role);
        }
        return false;
    }

    /**
     * <p>Determines whether or not logged user is banned. </p>
     * @param request is an incoming request
     * @return true if a logged user is banned, false otherwise
     */
    default boolean isBanned(HttpServletRequest request){
        if ( null != (User) request.getSession().getAttribute(AttributeConst.ATTR_LOGGED_USER)) {
            String role = ((User) request.getSession().getAttribute(AttributeConst.ATTR_LOGGED_USER)).getRole();
            return role.equalsIgnoreCase(UserRole.BANNED.name());
        }
        return false;
    }

    /**
     * <p>Determines whether or not user logged into the system. </p>
     * @param request is an incoming request
     * @return true if user hasn't logged into the system, false otherwise
     */
    default boolean isGuest(HttpServletRequest request){
        if ( null == (User) request.getSession().getAttribute(AttributeConst.ATTR_LOGGED_USER)) {
            return true;
        }
        return false;
    }

    /**
     * <p>Determines whether or not logged user in right to perform an operation. </p>
     * @param request is an incoming request
     * @param stringUserId is incoming user's id
     * @return true if logged user's id equals incoming user's id thus user is allowed to perform an operation
     */
    default boolean isLoggedUser(HttpServletRequest request, String stringUserId){
        long userId = Long.parseLong(stringUserId);
        User sessionUser = (User) request.getSession().getAttribute(AttributeConst.ATTR_LOGGED_USER);
        return sessionUser.getUserId() == userId;
    }

}
