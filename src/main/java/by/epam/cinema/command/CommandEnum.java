package by.epam.cinema.command;

import by.epam.cinema.command.impl.admin.*;
import by.epam.cinema.command.impl.common.*;
import by.epam.cinema.command.impl.user.*;


/**
 * Class {@code CommandEnum} is the class, that contains enumeration of all commands and returns a specific command
 * based on it's name
 * @author Veronica Bahel
 */

public enum CommandEnum {


    LOGIN (new LoginCommand()),
    LOGOUT ( new LogoutCommand()),
    REGISTER (new RegisterCommand()),
    LOGIN_PAGE (new LoginPageCommand()),
    VIEW_MOVIES (new ViewAllMoviesCommand()),
    VIEW_COMING_SOON (new ViewComingSoonCommand()),
    VIEW_TOP_100 (new ViewTop100Command()),
    VIEW_BORN_TODAY (new ViewBornTodayCommand()),
    VIEW_WITH_GENRE (new ViewMoviesByGenreCommand()),
    VIEW_SINGLE_MOVIE (new ViewSingleMovieCommand()),
    ADD_MOVIE_PAGE (new AddMoviePageCommand()),
    ADD_MOVIE (new AddMovieCommand()),
    ADD_COMMENT (new AddCommentCommand()),
    UPDATE_PROFILE_PAGE (new UpdateProfilePageCommand()),
    UPDATE_PROFILE (new UpdateProfileCommand()),
    VIEW_PROFILE (new ViewUserProfileCommand()),
    DELETE_PROFILE_PICTURE (new DeleteProfilePictureCommand()),
    RATE (new RateMovieCommand()),
    DELETE_RATING (new DeleteRatingCommand()),
    BAN_USER (new BanUserCommand()),
    REMOVE_BAN_FROM_USER (new RemoveBanFromUserCommand()),
    UPDATE_MOVIE_INFO (new UpdateMovieInfoCommand()),
    UPDATE_MOVIE_PAGE (new UpdateMoviePageCommand()),
    VIEW_USER_COMMENTS (new ViewUserCommentsCommand()),
    VIEW_USER_RATES (new ViewUserRatingsCommand()),
    SEARCH (new SearchCommand()),
    DELETE_COMMENT (new DeleteCommentCommand()),
    VIEW_USER_TABLE (new ViewUserTableCommand()),
    VIEW_PEOPLE (new ViewPeopleCommand()),
    VIEW_PERSON (new ViewPersonCommand()),
    ADD_PERSON_PAGE (new AddPersonPageCommand()),
    ADD_PERSON (new AddPersonCommand()),
    UPDATE_PERSON_INFO (new UpdatePersonInfoCommand()),
    UPDATE_PERSON_PAGE (new UpdatePersonPageCommand()),
    ADD_TO_FAVOURITES (new AddToFavouritesCommand()),
    REMOVE_FROM_FAVOURITES (new RemoveFromFavouritesCommand()),
    VIEW_WATCHLIST (new ViewWatchlistCommand()),
    DELETE_PROFILE (new DeleteProfileCommand()),
    DEFAULT_COMMAND (new ViewMainPageCommand()),
    CHANGE_LOCALE (new ChangeLocaleCommand());


    private ICommand command;
    CommandEnum(ICommand instance){
        this.command = instance;
    }
    public ICommand geCurrentCommand(){
        return command;
    }
}
