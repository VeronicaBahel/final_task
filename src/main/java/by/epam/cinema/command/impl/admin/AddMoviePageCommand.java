package by.epam.cinema.command.impl.admin;

import by.epam.cinema.command.constant.*;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.GenreService;
import by.epam.cinema.service.PersonService;
import by.epam.cinema.command.CommandEnum;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for forwarding on a movie adding page <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class AddMoviePageCommand implements ICommand {

    /**
     * <p>Checks if logged users is allowed to perform this operation then sets all necessary attributes to the session. </p>
     * @param request is an incoming request
     * @return url of add movie page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_GENERAL_ERROR;

        try {
            if(isAdmin(request)) {
                request.getSession().removeAttribute(AttributeConst.ATTR_MOVIE);
                request.getSession().removeAttribute(AttributeConst.ATTR_CHECKED_GENRES);
                request.getSession().removeAttribute(AttributeConst.ATTR_CHECKED_PEOPLE);

                request.getSession().setAttribute(AttributeConst.ATTR_GENRES, GenreService.showAllGenres());
                request.getSession().setAttribute(AttributeConst.ATTR_PEOPLE, PersonService.showWholeListOfPeople());

                request.getSession().setAttribute(AttributeConst.ATTR_AMBIGUOUS_COMMAND, CommandEnum.ADD_MOVIE.toString().toLowerCase());
                page = PageConst.PAGE_ADD_MOVIE;
            } else{
                request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
            }
        } catch (ServiceException e) {
            throw new CommandException("Can't initialize available genres, actors and directors ", e);
        }
        return page;
    }


}
