package by.epam.cinema.command.impl.user;

import by.epam.cinema.command.constant.ErrorMessageConst;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.UserService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.command.CommandException;
import by.epam.cinema.util.LogicUtil;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;

/**
 * Command for updating profile information <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class UpdateProfileCommand implements ICommand {

    private static final String SRC = "/controller?command=view_profile&userId=";
    private static final String ERROR_SRC = "/controller?command=update_profile_page&userId=";


    /**
     * <p>Collects all needed parameters, sends them to {@code UserService} and sets session attributes </p>
     * @param request is an incoming request
     * @return url of the user profile page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        String userId = request.getParameter(ParameterConst.PARAMETER_USER_ID);
        String requestPage = request.getServletContext().getRealPath("/");
        String firstName = request.getParameter(ParameterConst.PARAMETER_USER_FIRST_NAME);
        String lastName = request.getParameter(ParameterConst.PARAMETER_USER_LAST_NAME);
        String email = request.getParameter(ParameterConst.PARAMETER_USER_EMAIL);
        String location = request.getParameter(ParameterConst.PARAMETER_USER_LOCATION);
        String birthday = request.getParameter(ParameterConst.PARAMETER_USER_BIRTHDAY);
        if(LogicUtil.isAnEmail(email)) {
            try {
                Part imagePart = request.getPart(ParameterConst.PARAMETER_USER_IMAGE);
                String currentImage = UserService.findUserById(userId).getImageURL();
                String currentPassword = UserService.findUserById(userId).getPassword();
                String login = UserService.findUserById(userId).getLogin();
                UserService.updateUserInfo(requestPage, userId, firstName, lastName,
                        email, currentPassword, imagePart, login, currentImage, location, birthday);
                request.getSession().setAttribute(AttributeConst.ATTR_LOGGED_USER, UserService.findUserById(userId));
                request.getSession().setAttribute(AttributeConst.ATTR_USER, UserService.findUserById(userId));
                page = SRC + userId;
            } catch (ServiceException | IOException | ServletException e) {
                throw new CommandException("Can't update user info ", e);
            }
        } else {
            page = ERROR_SRC + userId;
            request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_INVALID_DATA);
        }
        return page;
    }

}
