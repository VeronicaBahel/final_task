package by.epam.cinema.command.impl.common;


import by.epam.cinema.command.CommandException;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.DefaultConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.service.GenreService;
import by.epam.cinema.service.MovieService;
import by.epam.cinema.service.ServiceException;
import org.w3c.dom.Attr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for viewing main page <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class ViewMainPageCommand implements ICommand{

    /**
     * <p>Invokes {@code MovieService} methods and sets session attributes </p>
     * @param request is an incoming request
     * @return url of the view page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        try{
            request.getSession().setAttribute(AttributeConst.ATTR_TOP, MovieService.showTopRated(DefaultConst.DEFAULT_MAIN_LIMIT));
            request.getSession().setAttribute(AttributeConst.ATTR_MOST_DISCUSSED, MovieService.showMostDiscussed());
            request.getSession().setAttribute(DefaultConst.DEFAULT_VIEW_GENRE,
                    MovieService.showMoviesWithGenre(ParameterConst.PARAMETER_DEFAULT_MAIN_GENRE,
                            String.valueOf(DefaultConst.DEFAULT_START_PAGE_NUMBER)));
            page = PageConst.PAGE_VIEW;
        } catch (ServiceException e) {
            throw new CommandException("Can't show top rated movies", e);
        }
        return page;
    }


}