package by.epam.cinema.command.impl.user;

import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.PageConst;
import org.apache.log4j.Logger;
import org.w3c.dom.Attr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for logging out of the system <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class LogoutCommand  implements ICommand{

    /**
     * <p>Invalidates session</p>
     * @param request is an incoming request
     * @return url of the index page
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = PageConst.PAGE_INDEX;
        request.getSession().invalidate();
        return page;
    }


}
