package by.epam.cinema.command.impl.common;

import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for forwarding to a login page <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class LoginPageCommand implements ICommand {

    /**
     * <p>Retrieves current page and sets session attributes</p>
     * @param request is an incoming request
     * @return url os the sign in page
     */
    @Override
    public String execute(HttpServletRequest request){
        String page = PageConst.PAGE_SIGN_IN_OR_UP;
        String currentPage = request.getParameter(ParameterConst.PARAMETER_PAGE);
        request.getSession().setAttribute(ParameterConst.PARAMETER_PREVIOUS_PAGE, currentPage);
        return page;
    }

}
