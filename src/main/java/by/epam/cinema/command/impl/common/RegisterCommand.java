package by.epam.cinema.command.impl.common;

import by.epam.cinema.command.UserRole;
import by.epam.cinema.command.constant.*;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.UserService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.entity.User;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for registering in the system <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class RegisterCommand implements ICommand{


    /**
     * <p>Collects all necessary parameters and send them to {@code UserService}, then sets session attributes</p>
     * @param request is an incoming request
     * @return url of the view page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_SIGN_IN_OR_UP;
        String email = request.getParameter(ParameterConst.PARAMETER_REGISTER_EMAIL);
        String login = request.getParameter(ParameterConst.PARAMETER_REGISTER_LOGIN);
        String password = request.getParameter(ParameterConst.PARAMETER_REGISTER_PASSWORD);
        if(isGuest(request)) {
            try {
                UserService.addUser(email, login, password, DefaultConst.DEFAULT_USER_PROFILE_PICTURE,
                        UserRole.REGULAR.toString().toUpperCase());
                User user = UserService.findUser(login);
                request.getSession().setAttribute(AttributeConst.ATTR_LOGGED_USER, user);
                page = PageConst.PAGE_VIEW;
            } catch (ServiceException e) {
                request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_NOT_DISTINCT_INPUT);
                throw new CommandException("Can't register user with such input values", e);
            }
        } else{
            request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_LOGGED_IN);
        }

        return page;
    }


}
