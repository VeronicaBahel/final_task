package by.epam.cinema.command.impl.user;

import by.epam.cinema.command.CommandException;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.ErrorMessageConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.UserService;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for deleting profile<br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class DeleteProfileCommand implements ICommand {

    /**
     * <p>Determines whether or not a user trying to perform this operation is logged,
     * collects all necessary parameters, sends them to {@code UserService} and sets session attributes</p>
     * @param request is an incoming request
     * @return url of the index page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_GENERAL_ERROR;
        String userId = request.getParameter(ParameterConst.PARAMETER_USER_ID);
        try{
            if(isLoggedUser(request, userId)){
                UserService.deleteProfile(userId);
                request.getSession().invalidate();
                page = PageConst.PAGE_INDEX;
            } else{
                request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE,
                        ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
            }
        } catch (ServiceException e) {
            throw new CommandException("Cant delete profile of a user with id: "+ userId, e);
        }
        return page;
    }

}
