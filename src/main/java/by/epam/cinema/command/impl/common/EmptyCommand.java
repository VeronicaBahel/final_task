package by.epam.cinema.command.impl.common;

import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.PageConst;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Basic command processed when user accidentally or on purpose called the wrong action<br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class EmptyCommand implements ICommand {


    /**
     * @param request is an incoming request
     * @return url of the index page
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = PageConst.PAGE_INDEX;
        return page;
    }
}
