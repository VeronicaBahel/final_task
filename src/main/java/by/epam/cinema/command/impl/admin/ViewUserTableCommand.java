package by.epam.cinema.command.impl.admin;


import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.UserService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.ErrorMessageConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for viewing statistical information about all users <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class ViewUserTableCommand implements ICommand {


    /**
     * <p>Determines whether or not logged user is allowed to perform this operation and sets session attributes</p>
     * @param request is an incoming request
     * @return url of the user table page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_GENERAL_ERROR;
        try{
            if(isAdmin(request)){
                request.getSession().setAttribute(AttributeConst.ATTR_USERS, UserService.showUserTable());
                page = PageConst.PAGE_USER_TABLE;
            } else{
                request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
            }

        } catch (ServiceException e) {
            throw new CommandException("Can't view user table ", e);
        }
        return page;
    }


}
