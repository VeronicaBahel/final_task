package by.epam.cinema.command.impl.common;

import by.epam.cinema.command.CommandException;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.service.PersonService;
import by.epam.cinema.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

/**
 * Command for viewing celebrities that were born today <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class ViewBornTodayCommand implements ICommand {

    /**
     * <p>Determines current day and month, then sends them to {@code PersonService} and sets session attributes </p>
     * @param request is an incoming request
     * @return url of the bor today page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        LocalDateTime now = LocalDateTime.now();
        int day = now.getDayOfMonth();
        int month = now.getMonthValue();
        try {
            request.getSession().setAttribute(AttributeConst.ATTR_PEOPLE, PersonService.showBornToday(day, month));
            page = PageConst.PAGE_BORN_TODAY;
        } catch (ServiceException e) {
            throw new CommandException("Can't load movies list", e);
        }
        return page;
    }


}
