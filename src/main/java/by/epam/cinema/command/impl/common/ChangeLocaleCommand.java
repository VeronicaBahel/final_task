package by.epam.cinema.command.impl.common;


import by.epam.cinema.command.CommandException;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.filter.LocaleConst;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for changing locale int he system <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class ChangeLocaleCommand implements ICommand {


    /**
     * <p>Retrieves current page and locale and sets session attributes </p>
     * @param request is an incoming request
     * @return current page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String currentPage = request.getParameter(ParameterConst.PARAMETER_PAGE);
        String locale = request.getParameter(LocaleConst.ATTR_LOCALE);
        request.getSession().setAttribute(LocaleConst.ATTR_LOCALE, locale);

        return currentPage;
    }

}
