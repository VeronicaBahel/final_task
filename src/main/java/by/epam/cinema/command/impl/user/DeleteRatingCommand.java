package by.epam.cinema.command.impl.user;

import by.epam.cinema.command.constant.ErrorMessageConst;
import by.epam.cinema.entity.User;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.MovieService;
import by.epam.cinema.service.RatingService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for deleting rating from a movie<br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class DeleteRatingCommand implements ICommand {

    /**
     * <p>Determines whether or not a user trying to perform this operation is logged,
     * collects all necessary parameters, sends them to {@code RatingService} as well as {@code MovieService}
     * and sets session attributes</p>
     * @param request is an incoming request
     * @return url of the movie page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_GENERAL_ERROR;
        String userId = request.getParameter(ParameterConst.PARAMETER_USER_ID);
        String movieId = request.getParameter(ParameterConst.PARAMETER_MOVIE_ID);
        try {
            User sessionUser = (User) request.getSession().getAttribute(AttributeConst.ATTR_LOGGED_USER);
            String sessionUserId = String.valueOf(sessionUser.getUserId());

            if(RatingService.retrieveRatingValue(movieId, sessionUserId) != 0 && isLoggedUser(request, userId)){
                RatingService.deleteRating(userId, movieId);
                request.getSession().setAttribute(AttributeConst.ATTR_MOVIE, MovieService.findMovieById(movieId));
                request.setAttribute(AttributeConst.ATTR_RATING, RatingService.retrieveRatingValue(movieId, sessionUserId));
                page = PageConst.PAGE_SINGLE_MOVIE;
            } else{
                request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE,
                        ErrorMessageConst.MESSAGE_NO_ACCESSIBLE_RATING);
            }

        } catch (ServiceException e) {
            throw new CommandException("Can't delete rating from user: " + userId, e);
        }
        return page;
    }


}
