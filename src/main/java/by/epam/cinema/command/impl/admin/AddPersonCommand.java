package by.epam.cinema.command.impl.admin;

import by.epam.cinema.command.constant.*;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.PersonService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.CommandException;
import by.epam.cinema.util.LogicUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;

/**
 * Command for adding basic person info <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class AddPersonCommand implements ICommand {
    private static final String SRC = "/controller?command=view_person&personId=";
    private static final String ERROR_SRC = "/controller?command=add_person_page";


    /**
     * <p>Collects all necessary attributes from a request, sends them to {@code PersonService}
     * and sets attributes to the session.</p>
     * @param request is an incoming request
     * @return url of a person page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_ADD_PERSON;
        String requestPage = request.getServletContext().getRealPath("/");
        String firstName = request.getParameter(ParameterConst.PARAMETER_PERSON_FIRST_NAME);
        String lastName = request.getParameter(ParameterConst.PARAMETER_PERSON_LAST_NAME);
        String birthday = request.getParameter(ParameterConst.PARAMETER_PERSON_BIRTHDAY);
        String birthPlace = request.getParameter(ParameterConst.PARAMETER_PERSON_BIRTH_PLACE);
        String pageNumber = (request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) != null) ?
                request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) :
                String.valueOf(DefaultConst.DEFAULT_START_PAGE_NUMBER);
        if(LogicUtil.isNotNull(firstName, lastName, birthday, birthPlace)) {
            try {
                if (isAdmin(request)) {
                    Part imgPart = request.getPart(ParameterConst.PARAMETER_PERSON_PICTURE_URL);
                    long generatedPersonId = PersonService.addPerson(requestPage, firstName, lastName, birthday,
                            DefaultConst.DEFAULT_PERSON_PICTURE, birthPlace, imgPart);

                    request.getSession().setAttribute(AttributeConst.ATTR_PERSON,
                            PersonService.findPersonById(String.valueOf(generatedPersonId)));

                    page = SRC + generatedPersonId;
                } else {
                    page = PageConst.PAGE_GENERAL_ERROR;
                    request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
                }
            } catch (ServiceException | ServletException | IOException e) {
                throw new CommandException("Can't add person ", e);
            }
        } else{
            page = ERROR_SRC;
            request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_INVALID_DATA);
        }
        return page;
    }


}
