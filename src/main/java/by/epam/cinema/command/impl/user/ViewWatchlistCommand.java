package by.epam.cinema.command.impl.user;

import by.epam.cinema.command.CommandException;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.DefaultConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.entity.User;
import by.epam.cinema.service.MovieService;
import by.epam.cinema.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for viewing user's cwatchlist<br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class ViewWatchlistCommand implements ICommand {

    /**
     * <p>Retrieves logged user from session, invokes {@code MovieService} methods and sets session attributes </p>
     * @param request is an incoming request
     * @return url of the watchlist page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_WATCHLIST;
        String pageNumber = (request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) != null) ?
                request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) :
                String.valueOf(DefaultConst.DEFAULT_START_PAGE_NUMBER);
        try {
            User sessionUser = (User) request.getSession().getAttribute(AttributeConst.ATTR_LOGGED_USER);
            String sessionUserId = String.valueOf(sessionUser.getUserId());
            request.getSession().setAttribute(AttributeConst.ATTR_WATCHLIST,
                    MovieService.viewWatchlist(sessionUserId, pageNumber));
            request.getSession().setAttribute(AttributeConst.ATTR_NUMBER_OF_PAGES,
                    MovieService.countWatchlistRecords(sessionUserId));
            request.getSession().setAttribute(AttributeConst.ATTR_CURRENT_PAGE_NUMBER, pageNumber);
        } catch (ServiceException e) {
            throw new CommandException("Can't view watchlist for logged user ", e);
        }
        return page;
    }


}
