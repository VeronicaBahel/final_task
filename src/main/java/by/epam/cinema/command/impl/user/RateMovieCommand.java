package by.epam.cinema.command.impl.user;

import by.epam.cinema.command.constant.*;
import by.epam.cinema.entity.User;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.CommentService;
import by.epam.cinema.service.MovieService;
import by.epam.cinema.service.RatingService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for rating a movie <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class RateMovieCommand implements ICommand {

    private static final String SRC = "/controller?command=view_single_movie&movieId=";

    /**
     * <p>Determines whether or not a user trying to perform this operation is logged,
     * collects all necessary parameters, sends them to a bunch of different services and sets session attributes</p>
     * @param request is an incoming request
     * @return url of the movie page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_GENERAL_ERROR;
        String rating = request.getParameter(ParameterConst.PARAMETER_RATING);
        String userId = request.getParameter(ParameterConst.PARAMETER_USER_ID);
        String movieId = request.getParameter(ParameterConst.PARAMETER_MOVIE_ID);
        String pageNumber = (request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) != null) ?
                request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) :
                String.valueOf(DefaultConst.DEFAULT_START_PAGE_NUMBER);

        long time = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(time);

        try {
            User sessionUser = (User) request.getSession().getAttribute(AttributeConst.ATTR_LOGGED_USER);
            String sessionUserId = String.valueOf(sessionUser.getUserId());
            if(RatingService.retrieveRatingValue(movieId, sessionUserId) == 0 && isLoggedUser(request, userId)) {
                RatingService.addRating(userId, movieId, rating, date);
                request.getSession().setAttribute(AttributeConst.ATTR_COMMENTS, CommentService.showMovieCommentsByPages(movieId, pageNumber));
                request.getSession().setAttribute(AttributeConst.ATTR_NUMBER_OF_PAGES, CommentService.countMovieComments(pageNumber));
                request.getSession().setAttribute(AttributeConst.ATTR_CURRENT_PAGE_NUMBER, pageNumber);

                request.getSession().setAttribute(AttributeConst.ATTR_MOVIE, MovieService.findMovieById(movieId));
                request.getSession().setAttribute(AttributeConst.ATTR_RATING, RatingService.retrieveRatingValue(movieId, sessionUserId));
                page = SRC + movieId;
            } else{
                request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_RATING_ALREADY_EXISTS);
            }
        } catch (ServiceException e) {
            throw new CommandException("Can't add rating ", e);
        }
        return page;
    }


}
