package by.epam.cinema.command.impl.admin;

import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.ErrorMessageConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.PersonService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.CommandException;
import by.epam.cinema.util.LogicUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;

/**
 * Command for updating person information <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class UpdatePersonInfoCommand implements ICommand {

    private static final String SRC = "/controller?command=view_person&personId=";
    private static final String ERROR_SRC = "/controller?command=update_person_page&personId=";


    /**
     * <p>Determines whether or not logged user is allowed to perform this operation,
     * collects all necessary parameters, sends them to {@code MovieService} and sets session attributes</p>
     * @param request is an incoming request
     * @return url of person page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_ADD_PERSON;

        String requestPage = request.getServletContext().getRealPath("/");
        String personId = request.getParameter(ParameterConst.PARAMETER_PERSON_ID);
        String firstName = request.getParameter(ParameterConst.PARAMETER_PERSON_FIRST_NAME);
        String lastName = request.getParameter(ParameterConst.PARAMETER_PERSON_LAST_NAME);
        String birthday = request.getParameter(ParameterConst.PARAMETER_PERSON_BIRTHDAY);
        String birthPlace = request.getParameter(ParameterConst.PARAMETER_PERSON_BIRTH_PLACE);
        if(LogicUtil.isNotNull(firstName, lastName, birthday, birthPlace)) {
            try {
                if (isAdmin(request)) {
                    String previousPictureUrl = PersonService.findPersonById(personId).getPictureURL();
                    Part imagePart = request.getPart(ParameterConst.PARAMETER_PERSON_PICTURE_URL);
                    PersonService.updatePersonInfo(requestPage, personId, firstName, lastName, birthday,
                            birthPlace, imagePart, previousPictureUrl);
                    request.getSession().setAttribute(AttributeConst.ATTR_PERSON,
                            PersonService.findPersonById(personId));
                    page = SRC + personId;
                } else {
                    page = PageConst.PAGE_GENERAL_ERROR;
                    request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
                }
            } catch (ServiceException | ServletException | IOException e) {
                throw new CommandException("Can't update info about person with id " + personId, e);
            }
        } else{
            page = ERROR_SRC + personId;
            request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_INVALID_DATA);
        }
        return page;
    }


}
