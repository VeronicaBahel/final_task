package by.epam.cinema.command.impl.common;

import by.epam.cinema.command.constant.DefaultConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.PersonService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for viewing people <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class ViewPeopleCommand implements ICommand {

    /**
     * <p>Invokes {@code PersonService} methods and sets session attributes </p>
     * @param request is an incoming request
     * @return url of the people page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        String pageNumber = (request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) != null) ?
                request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) :
                String.valueOf(DefaultConst.DEFAULT_START_PAGE_NUMBER);
        try {
            request.getSession().setAttribute(AttributeConst.ATTR_PEOPLE, PersonService.showPeople(pageNumber));
            request.getSession().setAttribute(AttributeConst.ATTR_NUMBER_OF_PAGES, PersonService.countPeople());
            request.getSession().setAttribute(AttributeConst.ATTR_CURRENT_PAGE_NUMBER, pageNumber);
            page = PageConst.PAGE_PEOPLE;
        } catch (ServiceException e) {
            throw new CommandException("Can't initialize people list ", e);
        }
        return page;
    }

}
