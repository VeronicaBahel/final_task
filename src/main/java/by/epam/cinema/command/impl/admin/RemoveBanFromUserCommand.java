package by.epam.cinema.command.impl.admin;

import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.UserService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.command.UserRole;
import by.epam.cinema.command.CommandException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for removing ban from user <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class RemoveBanFromUserCommand implements ICommand {

    /**
     * <p>Determines whether or not logged user is allowed to perform this operation, collects all necessary parameters
     * and sends them to {@code UserService}, then sets session attributes</p>
     * @param request is an incoming request
     * @return url of a user table page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_GENERAL_ERROR;
        String userId = request.getParameter(ParameterConst.PARAMETER_USER_ID);
        try {
            if(isAdmin(request)) {
                UserService.assignRoleToUser(userId, UserRole.REGULAR.toString().toUpperCase());
                request.getSession().setAttribute(AttributeConst.ATTR_USERS, UserService.showUserTable());
                page = PageConst.PAGE_USER_TABLE;
            }
        } catch (ServiceException e) {
            throw new CommandException("Can't remove ban from user with id: " + userId);
        }
        return page;
    }


}
