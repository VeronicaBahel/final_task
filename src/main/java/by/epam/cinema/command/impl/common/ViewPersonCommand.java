package by.epam.cinema.command.impl.common;

import by.epam.cinema.service.MovieService;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.PersonService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for a single person <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class ViewPersonCommand implements ICommand {

    /**
     * <p>Retrieves person id, then sends it to {@code PersonService} and sets session attributes </p>
     * @param request is an incoming request
     * @return url of the person page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        String personId = request.getParameter(ParameterConst.PARAMETER_PERSON_ID);
        try {
            request.getSession().setAttribute(AttributeConst.ATTR_PERSON, PersonService.findPersonById(personId));
            request.getSession().setAttribute(AttributeConst.ATTR_PERSON_MOVIES, MovieService.showMoviesWithPerson(personId));

            page = PageConst.PAGE_PERSON;
        } catch (ServiceException e) {
            throw new CommandException("Can't find person with such id: " + personId, e);
        }
        return page;
    }


}
