package by.epam.cinema.command.impl.user;

import by.epam.cinema.command.CommandException;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.*;
import by.epam.cinema.entity.User;
import by.epam.cinema.service.MovieService;
import by.epam.cinema.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for removing a movie from watchlist<br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class RemoveFromFavouritesCommand implements ICommand{

    /**
     * <p>Determines whether or not a user trying to perform this operation is logged,
     * collects all necessary parameters, sends them to {@code MovieService} and sets session attributes</p>
     * @param request is an incoming request
     * @return url of the watchlist page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_GENERAL_ERROR;
        String userId = request.getParameter(ParameterConst.PARAMETER_USER_ID);
        String movieId = request.getParameter(ParameterConst.PARAMETER_MOVIE_ID);
        String pageNumber = (request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) != null) ?
                request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) :
                String.valueOf(DefaultConst.DEFAULT_START_PAGE_NUMBER);
        try{
            User sessionUser = (User) request.getSession().getAttribute(AttributeConst.ATTR_LOGGED_USER);
            long sessionUserId = sessionUser.getUserId();
            if(isLoggedUser(request, userId)){
                MovieService.removeMovieFromFavourites(userId, movieId);
                request.getSession().setAttribute(AttributeConst.ATTR_IS_IN_WATCHLIST, MovieService.isInWatchlist(String.valueOf(sessionUserId), movieId));
                page = PageConst.PAGE_SINGLE_MOVIE;
            } else{
                request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE,
                        ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
            }
        } catch (ServiceException e) {
            throw new CommandException("Can't remove movie with id " + movieId + " from the user's " + userId + " favourites list");
        }
        return page;
    }


}
