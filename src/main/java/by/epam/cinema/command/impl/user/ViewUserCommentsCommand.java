package by.epam.cinema.command.impl.user;

import by.epam.cinema.command.constant.*;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.CommentService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for viewing user's comments<br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class ViewUserCommentsCommand implements ICommand {


    /**
     * <p>Determines whether or not a user trying to perform this operation is not a guest or banned,
     * invokes {@code CommentService} and sets session attributes</p>
     * @param request is an incoming request
     * @return url of the comments page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_GENERAL_ERROR;
        String userId = request.getParameter(ParameterConst.PARAMETER_USER_ID);
        String pageNumber = (request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) != null) ?
                request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) :
                String.valueOf(DefaultConst.DEFAULT_START_PAGE_NUMBER);
        try {
            if(!isGuest(request) && !isBanned(request)) {
                request.getSession().setAttribute(AttributeConst.ATTR_COMMENTS,
                        CommentService.showUserComments(userId));

                request.getSession().setAttribute(AttributeConst.ATTR_NUMBER_OF_PAGES,
                        CommentService.countUserComments(userId));

                request.getSession().setAttribute(AttributeConst.ATTR_CURRENT_PAGE_NUMBER, pageNumber);

                page = PageConst.PAGE_COMMENTS;
            } else{
                request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
            }
        } catch (ServiceException e) {
            throw new CommandException("Can't show comments for user with id: " + userId, e);
        }
        return page;
    }


}
