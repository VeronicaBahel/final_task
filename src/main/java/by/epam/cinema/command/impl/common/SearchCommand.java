package by.epam.cinema.command.impl.common;

import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.MovieService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for performing search operation <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class SearchCommand implements ICommand {

    /**
     * <p>Retrieves a search parameter, sends it to {@code MovieService}, then sets session attributes </p>
     * @param request is an incoming request
     * @return url of the search page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        String parameter = request.getParameter(ParameterConst.PARAMETER_GENERAL);
        try {
            request.getSession().setAttribute(AttributeConst.ATTR_MOVIES,
                    MovieService.searchMovie(parameter));
            request.getSession().setAttribute(ParameterConst.PARAMETER_GENERAL, parameter);
            page = PageConst.PAGE_SEARCH_RESULTS;
        } catch (ServiceException e) {
            throw new CommandException("Can't find any movie ", e);
        }
        return page;
    }

}
