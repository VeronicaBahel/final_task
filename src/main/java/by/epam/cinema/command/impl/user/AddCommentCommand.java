package by.epam.cinema.command.impl.user;

import by.epam.cinema.command.constant.*;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.CommentService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.CommandException;
import by.epam.cinema.util.LogicUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for adding a comment <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class AddCommentCommand implements ICommand {
    private static final String SRC = "/controller?command=view_single_movie&movieId=";

    /**
     * <p>Collects all necessary parameters, defined current date, send this dta to {@code CommentService} and then
     * sets session attributes </p>
     * @param request
     * @return url of the movie page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_GENERAL_ERROR;
        String content = request.getParameter(ParameterConst.PARAMETER_COMMENT_CONTENT);
        String pageNumber = (request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) != null) ?
                request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) :
                String.valueOf(DefaultConst.DEFAULT_START_PAGE_NUMBER);

        long time = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(time);

        String userId = request.getParameter(ParameterConst.PARAMETER_USER_ID);
        String movieId = request.getParameter(ParameterConst.PARAMETER_MOVIE_ID);

        if(LogicUtil.isNotNull(content)) {
            try {
                CommentService.addComment(content, userId, date, movieId);
                request.getSession().setAttribute(AttributeConst.ATTR_COMMENTS, CommentService.showMovieCommentsByPages(movieId, pageNumber));
                request.getSession().setAttribute(AttributeConst.ATTR_NUMBER_OF_PAGES, CommentService.countMovieComments(pageNumber));
                request.getSession().setAttribute(AttributeConst.ATTR_CURRENT_PAGE_NUMBER, pageNumber);
                page = SRC + movieId;
            } catch (ServiceException e) {
                throw new CommandException("Couldn't add comment ", e);
            }
        } else {
            request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_INVALID_DATA);
        }
        return page;
    }


}
