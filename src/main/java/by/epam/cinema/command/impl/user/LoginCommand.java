package by.epam.cinema.command.impl.user;

import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.ErrorMessageConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.UserService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.entity.User;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for logging into the system <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class LoginCommand implements ICommand {

    /**
     * <p>Collects all necessary parameters, sends them to {@code UserService} and sets session attributes </p>
     * @param request is an incoming request
     * @return previous page or sign in page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_SIGN_IN_OR_UP;
        String login = request.getParameter(ParameterConst.PARAMETER_LOGIN);
        String password = request.getParameter(ParameterConst.PARAMETER_PASSWORD);
        if(isGuest(request)) {
            try {
                User user = UserService.findUser(login);
                UserService.checkUser(user, password);
                request.getSession().setAttribute(AttributeConst.ATTR_LOGGED_USER, user);
                page = (String) request.getSession().getAttribute(ParameterConst.PARAMETER_PREVIOUS_PAGE);
            } catch (ServiceException e) {
                request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_WRONG_LOGIN_OR_PASSWORD);
                throw new CommandException("Can't log in a user with such input values", e);
            }
        } else{
            request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_LOGGED_IN);
        }
        return page;
    }


}

