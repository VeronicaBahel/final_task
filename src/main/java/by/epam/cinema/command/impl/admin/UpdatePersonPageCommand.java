package by.epam.cinema.command.impl.admin;

import by.epam.cinema.command.CommandException;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.ErrorMessageConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.CommandEnum;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.service.PersonService;
import by.epam.cinema.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for forwarding to adding person page for updating person information <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class UpdatePersonPageCommand implements ICommand {

    /**
     * <p>Determines whether or not logged user is allowed to perform this operation,
     * collects all necessary parameters, sends them to {@code PersonService} and sets session attributes</p>
     * @param request is an incoming request
     * @return url of an add person page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_GENERAL_ERROR;
        String personId = request.getParameter(ParameterConst.PARAMETER_PERSON_ID);
        try {
            if (isAdmin(request)) {
                request.setAttribute(AttributeConst.ATTR_AMBIGUOUS_COMMAND, CommandEnum.UPDATE_PERSON_INFO.name().toLowerCase());
                request.getSession().setAttribute(AttributeConst.ATTR_PERSON, PersonService.findPersonById(personId));
                page = PageConst.PAGE_ADD_PERSON;
            } else {
                request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
            }
        } catch (ServiceException e) {
            throw new CommandException("Can't find person with id: " + personId, e);
        }

        return page;
    }


}
