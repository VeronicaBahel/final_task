package by.epam.cinema.command.impl.common;

import by.epam.cinema.command.constant.DefaultConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.MovieService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for viewing all movies <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class ViewAllMoviesCommand implements ICommand {


    /**
     * <p>Invokes {@code MovieService} methods and sets session attributes </p>
     * @param request is an incoming request
     * @return url of the movies page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String pageNumber = (request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) != null) ?
                request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) :
                String.valueOf(DefaultConst.DEFAULT_START_PAGE_NUMBER);
        try {
            request.getSession().setAttribute(AttributeConst.ATTR_MOVIES, MovieService.showMovies(pageNumber));
            request.getSession().setAttribute(AttributeConst.ATTR_NUMBER_OF_PAGES,
                    MovieService.countMovies());
            request.getSession().setAttribute(AttributeConst.ATTR_CURRENT_PAGE_NUMBER, pageNumber);
        } catch (ServiceException e) {
            throw new CommandException("Can't load movies list", e);
        }
        return PageConst.PAGE_ALL_MOVIES;
    }

}
