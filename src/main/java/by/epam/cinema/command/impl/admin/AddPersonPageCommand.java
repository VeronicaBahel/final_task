package by.epam.cinema.command.impl.admin;

import by.epam.cinema.command.CommandEnum;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.ErrorMessageConst;
import by.epam.cinema.command.constant.PageConst;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for forwarding to an adding person page <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class AddPersonPageCommand implements ICommand {

    /**
     * <p>Determines whether or not logged user is allowed to perform this operation,
     * then sets all necessary attributes to the session</p>
     * @param request is an incoming request
     * @return url of an add person page or error page
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = PageConst.PAGE_GENERAL_ERROR;
        if(isAdmin(request)) {
            request.getSession().removeAttribute(AttributeConst.ATTR_PERSON);
            request.setAttribute(AttributeConst.ATTR_AMBIGUOUS_COMMAND, CommandEnum.ADD_PERSON.toString().toLowerCase());
            page = PageConst.PAGE_ADD_PERSON;
        } else{
            request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
        }
        return page;
    }
}
