package by.epam.cinema.command.impl.user;

import by.epam.cinema.command.constant.ErrorMessageConst;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.UserService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;


/**
 * Command for forwarding to a updating profile page<br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class UpdateProfilePageCommand implements ICommand {

    /**
     * <p>Determines whether or not a user trying to perform this operation is logged
     * and sets session attributes</p>
     * @param request is an incoming request
     * @return url of the update profile page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_GENERAL_ERROR;
        String userId = request.getParameter(ParameterConst.PARAMETER_USER_ID);
        try {
            if(isLoggedUser(request, userId)) {
                request.getSession().setAttribute(AttributeConst.ATTR_USER, UserService.findUserById(userId));
                page = PageConst.PAGE_UPDATE_USER_PROFILE;
            } else{
                request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE,
                        ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
            }
        } catch (ServiceException e) {
            throw new CommandException("Can't find user with such login ", e);
        }
        return page;
    }
}
