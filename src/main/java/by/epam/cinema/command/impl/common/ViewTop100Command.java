package by.epam.cinema.command.impl.common;


import by.epam.cinema.command.CommandException;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.DefaultConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.service.MovieService;
import by.epam.cinema.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command for viewing top 100 rated movies <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class ViewTop100Command implements ICommand {

    /**
     * <p>Invokes {@code MovieService} method and sets session attributes </p>
     * @param request
     * @return url of the top100 page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        try {
            request.getSession().setAttribute(AttributeConst.ATTR_MOVIES, MovieService.showTopRated(DefaultConst.DEFAULT_TOP_LIMIT));
            page = PageConst.PAGE_TOP_10;
        } catch (ServiceException e) {
            throw new CommandException("Can't load movies list", e);
        }
        return page;
    }


}
