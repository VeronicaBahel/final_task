package by.epam.cinema.command.impl.admin;

import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.MovieService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.ErrorMessageConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.command.CommandException;
import by.epam.cinema.util.LogicUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Arrays;

/**
 * Command for updating movie information <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class UpdateMovieInfoCommand implements ICommand {

    private static final String SRC = "/controller?command=view_single_movie&movieId=";
    private static final String ERROR_SRC = "/controller?command=update_movie_page&movieId=";

    /**
     * <p>Determines whether or not logged user is allowed to perform this operation,
     * collects all necessary parameters, sends them to {@code MovieService} and sets session attributes</p>
     * @param request is an incoming request
     * @return url of an updated movie page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        String movieId = request.getParameter(ParameterConst.PARAMETER_MOVIE_ID);
        String requestPage = request.getServletContext().getRealPath("/");
        String[] genres = request.getParameterValues(ParameterConst.PARAMETER_GENRES);
        String[] actors = request.getParameterValues(ParameterConst.PARAMETER_ACTORS);
        String[] directors = request.getParameterValues(ParameterConst.PARAMETER_DIRECTORS);
        String title = request.getParameter(ParameterConst.PARAMETER_TITLE);
        String originalTitle = request.getParameter(ParameterConst.PARAMETER_ORIGINAL_TITLE);
        String date = request.getParameter(ParameterConst.PARAMETER_WORLD_PREMIERE);
        String budget = request.getParameter(ParameterConst.PARAMETER_BUDGET);
        String ageLimit = request.getParameter(ParameterConst.PARAMETER_AGE_LIMIT);
        String briefDescription = request.getParameter(ParameterConst.PARAMETER_BRIEF_DESCRIPTION);
        String trailerURL = request.getParameter(ParameterConst.PARAMETER_TRAILER_URL);
        if(LogicUtil.isNotNull(title, date, budget, ageLimit, briefDescription) &&
                LogicUtil.isNumberPositive(budget, ageLimit)
                && LogicUtil.isNotNull(genres) && LogicUtil.isNotNull(actors) && LogicUtil.isNotNull(directors)) {
            try {
                if (isAdmin(request)) {
                    String previousPosterURL = MovieService.findMovieById(movieId).getPosterURL();
                    Part imagePart = request.getPart(ParameterConst.PARAMETER_POSTER_URL);
                    MovieService.updateMovieInfo(requestPage, movieId, title, originalTitle, genres, actors,
                            directors, date, budget, ageLimit, briefDescription, previousPosterURL, imagePart, trailerURL);
                    request.getSession().setAttribute(AttributeConst.ATTR_MOVIE, MovieService.findMovieById(movieId));
                    page = SRC + movieId;
                } else {
                    page = PageConst.PAGE_GENERAL_ERROR;
                    request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
                }
            } catch (ServiceException | ServletException | IOException e) {
                throw new CommandException("Can't update movie info ", e);
            }
        } else{
            page = ERROR_SRC + movieId;
            request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_INVALID_DATA);
        }

        return page;
    }

}
