package by.epam.cinema.command.impl.user;

import by.epam.cinema.command.constant.*;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.UserService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for deleting profile picture <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class DeleteProfilePictureCommand implements ICommand {


    /**
     * <p>Determines whether or not a user trying to perform this operation is logged,
     * retrieves user id, sends it and the default picture url to {@code UserService} and sets session attributes</p>
     * @param request is an incoming request
     * @return
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_GENERAL_ERROR;
        String userId = request.getParameter(ParameterConst.PARAMETER_USER_ID);
        try {
            if(isLoggedUser(request, userId)){
                UserService.deleteProfilePicture(userId, DefaultConst.DEFAULT_USER_PROFILE_PICTURE);
                request.getSession().setAttribute(AttributeConst.ATTR_USER, UserService.findUserById(userId));
                request.getSession().setAttribute(AttributeConst.ATTR_LOGGED_USER, UserService.findUserById(userId));
                page = PageConst.PAGE_USER_PROFILE;
            } else{
                request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE,
                        ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
            }
        } catch (ServiceException e) {
            throw new CommandException("Can't delete current image ", e);
        }
        return page;
    }

}
