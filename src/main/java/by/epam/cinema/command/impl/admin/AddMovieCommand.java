package by.epam.cinema.command.impl.admin;

import by.epam.cinema.command.constant.*;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.MovieService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.CommandException;
import by.epam.cinema.util.LogicUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;

/**
 * Command for adding basic movie info <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class AddMovieCommand implements ICommand {

    private static final String SRC = "/controller?command=view_single_movie&movieId=";
    private static final String ERROR_SRC = "/controller?command=add_movie_page";

    /**
     * <p>Determines whether or not logged user is allowed to perform this operation,
     * collects all necessary parameters, sends them to {@code MovieService} and sets attributes to session</p>
     * @param request is an incoming request
     * @return url of added movie page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_ADD_MOVIE;
        String requestPage = request.getServletContext().getRealPath("/");
        String[] genres = request.getParameterValues(ParameterConst.PARAMETER_GENRES);
        String[] actors = request.getParameterValues(ParameterConst.PARAMETER_ACTORS);
        String[] directors = request.getParameterValues(ParameterConst.PARAMETER_DIRECTORS);
        String title = request.getParameter(ParameterConst.PARAMETER_TITLE);
        String originalTitle = request.getParameter(ParameterConst.PARAMETER_ORIGINAL_TITLE);
        String date = request.getParameter(ParameterConst.PARAMETER_WORLD_PREMIERE);
        String budget = request.getParameter(ParameterConst.PARAMETER_BUDGET);
        String ageLimit = request.getParameter(ParameterConst.PARAMETER_AGE_LIMIT);
        String briefDescription = request.getParameter(ParameterConst.PARAMETER_BRIEF_DESCRIPTION);
        String trailerURL = request.getParameter(ParameterConst.PARAMETER_TRAILER_URL);
        if(LogicUtil.isNotNull(title, date, budget, ageLimit, briefDescription) &&
                LogicUtil.isNumberPositive(budget, ageLimit)
                && LogicUtil.isNotNull(genres) && LogicUtil.isNotNull(actors) && LogicUtil.isNotNull(directors)){
            try {
                if(isAdmin(request)) {
                    Part imgPart = request.getPart(ParameterConst.PARAMETER_POSTER_URL);
                    String generatedMovieId = MovieService.addMovie(requestPage, title, originalTitle, genres,
                            actors, directors, date, budget, ageLimit, briefDescription,
                            DefaultConst.DEFAULT_MOVIE_POSTER, imgPart, trailerURL);
                    request.getSession().setAttribute(AttributeConst.ATTR_MOVIE,
                            MovieService.findMovieById(generatedMovieId));
                    page = SRC + generatedMovieId;
                } else{
                    page = PageConst.PAGE_GENERAL_ERROR;
                    request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
                }
            } catch(ServiceException | ServletException | IOException  e){
                throw new CommandException("Can't add movie ", e);
            }
        } else{
            page = ERROR_SRC;
            request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_INVALID_DATA);
        }
        return page;
    }

}
