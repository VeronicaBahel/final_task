package by.epam.cinema.command.impl.common;

import by.epam.cinema.command.constant.DefaultConst;
import by.epam.cinema.entity.User;
import by.epam.cinema.service.RatingService;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.CommentService;
import by.epam.cinema.service.MovieService;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for a single movie <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class ViewSingleMovieCommand implements ICommand {

    /**
     * <p>Retrieves movie id, then sends it to {@code MovieService} and sets session attributes </p>
     * @param request
     * @return url of the movie page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        String movieId = request.getParameter(ParameterConst.PARAMETER_MOVIE_ID);
        String pageNumber = (request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) != null) ?
                request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) :
                String.valueOf(DefaultConst.DEFAULT_START_PAGE_NUMBER);
        try {
            User sessionUser = (User) request.getSession().getAttribute(AttributeConst.ATTR_LOGGED_USER);
            String sessionUserId = (sessionUser != null) ? String.valueOf(sessionUser.getUserId()) : "-1";

            if(!isGuest(request)){
                request.getSession().setAttribute(AttributeConst.ATTR_IS_IN_WATCHLIST, MovieService.isInWatchlist(sessionUserId, movieId));
            }

            request.getSession().setAttribute(AttributeConst.ATTR_MOVIE, MovieService.findMovieById(movieId));
            request.getSession().setAttribute(AttributeConst.ATTR_RATING, RatingService.retrieveRatingValue(movieId, sessionUserId));
            request.getSession().setAttribute(AttributeConst.ATTR_COMMENTS, CommentService.showMovieCommentsByPages(movieId, pageNumber));
            request.getSession().setAttribute(AttributeConst.ATTR_NUMBER_OF_PAGES, CommentService.countMovieComments(pageNumber));
            request.getSession().setAttribute(AttributeConst.ATTR_CURRENT_PAGE_NUMBER, pageNumber);

            page = PageConst.PAGE_SINGLE_MOVIE;
        } catch (ServiceException e) {
            throw new CommandException("Can't load movie with comments and average rating for it ", e);
        }
        return page;
    }

}
