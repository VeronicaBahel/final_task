package by.epam.cinema.command.impl.admin;

import by.epam.cinema.command.constant.*;
import by.epam.cinema.service.ServiceException;
import by.epam.cinema.service.GenreService;
import by.epam.cinema.service.MovieService;
import by.epam.cinema.service.PersonService;
import by.epam.cinema.command.CommandEnum;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for forwarding to an adding movie page for updating movie information<br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class UpdateMoviePageCommand implements ICommand {

    /**
     * <p>Determines whether or not logged user is allowed to perform this operation,
     * collects all necessary parameters, sends them to {@code MovieService} and sets attributes to session</p>
     * @param request is an incoming request
     * @return url of an add movie page or error page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PageConst.PAGE_GENERAL_ERROR;
        String movieId = request.getParameter(ParameterConst.PARAMETER_MOVIE_ID);
        String pageNumber = (request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) != null) ?
                request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) :
                String.valueOf(DefaultConst.DEFAULT_START_PAGE_NUMBER);

        try {
            if(isAdmin(request)) {
                request.getSession().setAttribute(AttributeConst.ATTR_AMBIGUOUS_COMMAND, CommandEnum.UPDATE_MOVIE_INFO.name());

                request.getSession().setAttribute(AttributeConst.ATTR_GENRES, GenreService.showAllGenres());
                request.getSession().setAttribute(AttributeConst.ATTR_PEOPLE, PersonService.showWholeListOfPeople());


                request.getSession().setAttribute(AttributeConst.ATTR_MOVIE, MovieService.findMovieById(movieId));
                request.getSession().setAttribute(AttributeConst.ATTR_CHECKED_GENRES, GenreService.showGenresForTheMovie(movieId));
                request.getSession().setAttribute(AttributeConst.ATTR_CHECKED_PEOPLE, PersonService.showPeopleForTheMovie(movieId));
                page = PageConst.PAGE_ADD_MOVIE;
            } else{
                request.setAttribute(AttributeConst.ATTR_ERROR_MESSAGE, ErrorMessageConst.MESSAGE_ACCESS_TO_THIS_COMMAND_DENIED);
            }
        } catch (ServiceException e) {
            throw new CommandException("Can't find movie with such id ", e);
        }
        return page;
    }

}
