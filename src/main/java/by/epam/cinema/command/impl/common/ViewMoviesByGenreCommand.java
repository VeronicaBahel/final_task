package by.epam.cinema.command.impl.common;


import by.epam.cinema.command.CommandException;
import by.epam.cinema.command.ICommand;
import by.epam.cinema.command.constant.AttributeConst;
import by.epam.cinema.command.constant.DefaultConst;
import by.epam.cinema.command.constant.PageConst;
import by.epam.cinema.command.constant.ParameterConst;
import by.epam.cinema.service.MovieService;
import by.epam.cinema.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for viewing movies with a specific genre <br/>
 * Implements {@link by.epam.cinema.command.ICommand}
 * @author Veronica Bahel
 */
public class ViewMoviesByGenreCommand implements ICommand {

    /**
     * <p>Retrieves a genre, then sends it to {@code MovieService} and sets session attributes </p>
     * @param request is an incoming request
     * @return url of the search page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        String pageNumber = (request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) != null) ?
                request.getParameter(ParameterConst.PARAMETER_PAGE_NUMBER) :
                String.valueOf(DefaultConst.DEFAULT_START_PAGE_NUMBER);
        String genre = request.getParameter(ParameterConst.PARAMETER_GENRE);
        try {
            request.getSession().setAttribute(AttributeConst.ATTR_MOVIES, MovieService.showMoviesWithGenre(genre, pageNumber));
            request.getSession().setAttribute(AttributeConst.ATTR_NUMBER_OF_PAGES,
                    MovieService.countMoviesWithGenre(genre));
            request.getSession().setAttribute(AttributeConst.ATTR_CURRENT_PAGE_NUMBER, pageNumber);

            request.getSession().setAttribute(ParameterConst.PARAMETER_GENERAL , genre);
            page = PageConst.PAGE_SEARCH_RESULTS;
        } catch (ServiceException e) {
            throw new CommandException("Can't load movies list", e);
        }
        return page;
    }

}
