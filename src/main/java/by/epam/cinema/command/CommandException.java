package by.epam.cinema.command;

/**
 * Class {@code CommandException} is the class, that extends {@code Exception} class to get own exceptions
 * for "Command" layer
 * @author Veronica Bahel
 */
public class CommandException extends Exception{

    public CommandException() {
        super();
    }

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandException(Throwable cause) {
        super(cause);
    }

    protected CommandException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
