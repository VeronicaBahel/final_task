package by.epam.cinema.command;


/**
 * Class {@code UserRole} is the class, that contains enumeration of user roles in the system
 * @author Veronica Bahel
 */
public enum UserRole {
    ADMIN, REGULAR, BANNED
}
