package by.epam.cinema.test;

import by.epam.cinema.util.SHA256Util;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class SHA256UtilTest  extends TestCase {

    private String password;
    @Before
    public void setUp() throws Exception {
        password = "Qwerty123";
    }

    @After
    public void tearDown() throws Exception {
        password = null;
    }

    @Test
    public void testSHA256Util() throws Exception{
        String password1 = SHA256Util.encrypt(password);
        String password2 = SHA256Util.encrypt(password);
        assertEquals(password1, password2);
    }

}