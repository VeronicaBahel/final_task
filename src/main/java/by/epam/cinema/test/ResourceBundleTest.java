package by.epam.cinema.test;


import junit.framework.TestCase;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ResourceBundle;

public class ResourceBundleTest extends TestCase {
    private String resourceBundleName;
    private String driverClass;

    @Before
    public void setUp() throws Exception {
        resourceBundleName = "db";
        driverClass = "DB_DRIVER_CLASS";
    }

    @After
    public void tearDown() throws Exception {
        resourceBundleName = null;
        driverClass = null;
    }

    @Test
    public void testExistingOfResourceBundle() throws Exception {
        ResourceBundle bundle = ResourceBundle.getBundle(resourceBundleName);
        boolean actualResult = bundle.containsKey(driverClass);
        assertTrue(actualResult);
    }


}
