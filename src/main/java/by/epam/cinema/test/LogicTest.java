package by.epam.cinema.test;


import by.epam.cinema.util.LogicUtil;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class LogicTest  {
    String email;
    @Before
    public void setUp(){
        email = "veronicabahel@gmail.com";
    }

    @After
    public void tearDown(){
        email = null;
    }

    @Test
    public void testLogicUtil(){
        boolean actualResult = LogicUtil.isAnEmail(email);
        assertTrue(actualResult);
    }
}
