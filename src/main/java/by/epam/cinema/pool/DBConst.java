package by.epam.cinema.pool;


class DBConst {
    static final String BUNDLE = "db";
    static final String URL = "DB_URL";
    static final String USER = "DB_USERNAME";
    static final String PASSWORD = "DB_PASSWORD";
    static final String DRIVER_LOCATION = "DB_DRIVER_CLASS";
    static final String CONNECTION_AMOUNT = "DB_CONNECTION_AMOUNT";
    static final int DEFAULT_AMOUNT = 5;
}
