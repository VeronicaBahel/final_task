package by.epam.cinema.pool;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class {@code ConnectionPool} is the class that deals with distributing connections in the pool
 * @author Veronica Bahel
 */
public class ConnectionPool {
    private static Logger logger = Logger.getLogger(ConnectionPool.class);

    private static ReentrantLock lock = new ReentrantLock();

    private BlockingQueue<Connection> availableConnectionsQueue;
    private BlockingQueue<Connection> usedConnectionsQueue;

    private String url;
    private String user;
    private String password;
    private String locationOfDriver;
    private int connectionAmount;


    private static ConnectionPool instance;
    private static AtomicBoolean instanceCreated = new AtomicBoolean(false);


    /**
     * <p>Returns new instance of the connection pool or returns an existing one</p>
     * @return instance of the connection pool
     */
    public static ConnectionPool getInstance(){

        if (!instanceCreated.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                    instanceCreated.set(true);
                }
            }
            finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * <p>Private constructor that sets driver parameters with values from that bundle and invokes method
     * of initialization of the connection pool</p>
     */
    private ConnectionPool(){
        try {
            ResourceBundle bundle = ResourceBundle.getBundle(DBConst.BUNDLE);
            this.url = bundle.getString(DBConst.URL);
            this.user = bundle.getString(DBConst.USER);
            this.password = bundle.getString(DBConst.PASSWORD);
            this.locationOfDriver = bundle.getString(DBConst.DRIVER_LOCATION);
            this.connectionAmount = Integer.parseInt(bundle.getString(DBConst.CONNECTION_AMOUNT));
            this.availableConnectionsQueue = new ArrayBlockingQueue<>(connectionAmount);
            this.usedConnectionsQueue = new ArrayBlockingQueue<>(connectionAmount);
            initPool();
        } catch (MissingResourceException e){
            logger.fatal("JDBC: Can't find resource bundle ", e);
            throw new RuntimeException("JDBC: Can't find resource bundle", e);
        } catch(NumberFormatException e){
            this.connectionAmount = DBConst.DEFAULT_AMOUNT;
        } catch (ConnectionPoolException e) {
            throw new RuntimeException("JDBC: Incorrect initialization of the class");
        }
    }

    /**
     * Initializes connection pool
     * @throws ConnectionPoolException
     */
    private void initPool() throws ConnectionPoolException {
        logger.info("Creating connection pool");
        try {
            Class.forName(locationOfDriver);
            Locale.setDefault(Locale.ENGLISH);
            for(int i=0; i < connectionAmount; i++) {
                Connection connection = DriverManager.getConnection(url, user, password);
                availableConnectionsQueue.put(connection);
                logger.info("Connection "+ i +" is created and put into the queue.");
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("JDBC: Can't find driver class ", e);
        } catch (SQLException | InterruptedException e) {
            logger.error("SQLException or InterruptedException occurred" +
                    "while initializing pool pool");
        }
    }


    public Connection takeConnection() {
        Connection connection = null;
        try {
            connection = availableConnectionsQueue.take();
            usedConnectionsQueue.put(connection);
        } catch (InterruptedException e) {
            logger.error("Interrupted exception occurred while taking connection from the pool ", e);
        }
        logger.info("Connection is taken.");
        return connection;
    }

    public void returnConnection(Connection connection) {
        try {
            logger.info("Trying to return connection to the connection pool...");
            usedConnectionsQueue.remove(connection);
            availableConnectionsQueue.put(connection);
            logger.info("Connection is successfully returned.");
        } catch (InterruptedException e) {
            logger.error("InterruptedException occurred while returning a connection");
        }

    }


    public void destroyPool() throws ConnectionPoolException {
        logger.info("Destroying connection pool.");
        try{
            for(int i = 0; i<connectionAmount; i++) {
                Connection connection = ConnectionPool.getInstance().takeConnection();
                connection.close();
                logger.info("Connection "+ i +" is destroyed.");
            }
        } catch (SQLException e){
            logger.warn("Can't destroy connection pool.");
            throw new ConnectionPoolException("Can't destroy connection pool.", e);
        }
    }



}