package by.epam.cinema.entity;

import java.io.Serializable;


/**
 * Class {@code Genre} is is an entity class that contains all information about the genre
 * @author Veronica Bahel
 */
public class Genre implements Serializable {
    private long genreId;
    private String label;

    public Genre() {
    }

    public Genre(long genreId, String label) {
        this.genreId = genreId;
        this.label = label;
    }

    public long getGenreId() {
        return genreId;
    }

    public void setGenreId(long genreId) {
        this.genreId = genreId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "genreId=" + genreId +
                ", label='" + label + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Genre genre = (Genre) o;

        if (genreId != genre.genreId) return false;
        return label.equals(genre.label);
    }

    @Override
    public int hashCode() {
        int result = (int) (genreId ^ (genreId >>> 32));
        result = 31 * result + label.hashCode();
        return result;
    }
}
