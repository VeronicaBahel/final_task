package by.epam.cinema.entity;

import java.io.Serializable;
import java.sql.Date;


/**
 * Class {@code User} is is an entity class that contains all information about the user
 * @author Veronica Bahel
 */
public class User implements Serializable {
    private long userId;
    private String firstName;
    private String lastName;
    private String email;
    private String login;
    private String password;
    private String imageURL;
    private String location;
    private String role;
    private Date birthday;




    public User(long userId, String firstName, String lastName, String email, String login, String password,
                String imageURL, String location, String role) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.login = login;
        this.password = password;
        this.imageURL = imageURL;
        this.location = location;
        this.role = role;
    }

    public User(String email, String login, String password, String imageURL, String role) {
        this.login = login;
        this.email = email;
        this.password = password;
        this.imageURL = imageURL;
        this.role = role;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public User() {
    }


    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", location='" + location + '\'' +
                ", role='" + role + '\'' +
                '}';
    }



}
