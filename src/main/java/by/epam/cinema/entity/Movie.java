package by.epam.cinema.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;


/**
 * Class {@code Movie} is is an entity class that contains all basic information about the movie
 * @author Veronica Bahel
 */
public class Movie implements Serializable {

    private long movieId;
    private String title;
    private String originalTitle;
    private ArrayList<Genre> genres;
    private Date worldPremiere;
    private int budget;
    private int ageLimit;
    private String briefDescription;
    private String posterURL;
    private String trailerURL;
    private double averageRating;
    private ArrayList<Person> people;

    public Movie() {

    }

    public Movie(long movieId, String title, String originalTitle, ArrayList<Genre> genres, Date worldPremiere,
                 int budget, int ageLimit, String briefDescription, String posterURL, String trailerURL, double averageRating,
                 ArrayList<Person> people) {
        this.movieId = movieId;
        this.title = title;
        this.originalTitle = originalTitle;
        this.genres = genres;;
        this.worldPremiere = worldPremiere;
        this.budget = budget;
        this.ageLimit = ageLimit;
        this.briefDescription = briefDescription;
        this.posterURL = posterURL;
        this.trailerURL = trailerURL;
        this.averageRating = averageRating;
        this.people = people;
    }

    public Movie(String title, String originalTitle, Date worldPremiere,
                 int budget, int ageLimit, String briefDescription, String posterURL, String trailerURL) {
        this.title = title;
        this.originalTitle = originalTitle;
        this.worldPremiere = worldPremiere;
        this.budget = budget;
        this.ageLimit = ageLimit;
        this.briefDescription = briefDescription;
        this.posterURL = posterURL;
        this.trailerURL = trailerURL;
    }

    public long getMovieId() {
        return movieId;
    }

    public void setMovieId(long movieId) {
        this.movieId = movieId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public ArrayList<Genre> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<Genre> genres) {
        this.genres = genres;
    }

    public Date getWorldPremiere() {
        return worldPremiere;
    }

    public void setWorldPremiere(Date worldPremiere) {
        this.worldPremiere = worldPremiere;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public int getAgeLimit() {
        return ageLimit;
    }

    public void setAgeLimit(int ageLimit) {
        this.ageLimit = ageLimit;
    }

    public String getPosterURL() {
        return posterURL;
    }

    public void setPosterURL(String posterURL) {
        this.posterURL = posterURL;
    }

    public String getTrailerURL() {
        return trailerURL;
    }

    public void setTrailerURL(String trailerURL) {
        this.trailerURL = trailerURL;
    }

    public String getBriefDescription() {
        return briefDescription;
    }

    public void setBriefDescription(String briefDescription) {
        this.briefDescription = briefDescription;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    public ArrayList<Person> getPeople() {
        return people;
    }

    public void setPeople(ArrayList<Person> people) {
        this.people = people;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "movieId=" + movieId +
                ", title='" + title + '\'' +
                ", originalTitle='" + originalTitle + '\'' +
                ", genres=" + genres.toString()  +
                ", worldPremiere=" + worldPremiere +
                ", budget=" + budget +
                ", ageLimit=" + ageLimit +
                ", briefDescription='" + briefDescription + '\'' +
                ", posterURL='" + posterURL + '\'' +
                ", trailerURL='" + trailerURL + '\'' +
                ", averageRating='" + trailerURL + '\'' +
                ", people=" + people.toString()  +
                '}';
    }


}
