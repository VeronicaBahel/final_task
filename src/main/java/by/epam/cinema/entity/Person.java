package by.epam.cinema.entity;

import java.io.Serializable;
import java.sql.Date;

/**
 * Class {@code Person} is is an entity class that contains all information about the person
 * @author Veronica Bahel
 */
public class Person implements Serializable {
    private long personId;
    private String firstName;
    private String lastName;
    private Date birthday;
    private String birthPlace;
    private String pictureURL;
    private String role;

    public Person() {
    }

    public Person(long personId, String firstName, String lastName, Date birthday, String birthPlace, String pictureURL) {
        this.personId = personId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.birthPlace = birthPlace;
        this.pictureURL = pictureURL;
    }

    public Person(String firstName, String lastName, Date birthday, String birthPlace, String pictureURL) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.birthPlace = birthPlace;
        this.pictureURL = pictureURL;
    }

    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (personId != person.personId) return false;
        if (!firstName.equals(person.firstName)) return false;
        if (!lastName.equals(person.lastName)) return false;
        if (!birthday.equals(person.birthday)) return false;
        if (!birthPlace.equals(person.birthPlace)) return false;
        return pictureURL.equals(person.pictureURL);
    }

    @Override
    public int hashCode() {
        int result = (int) (personId ^ (personId >>> 32));
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + birthday.hashCode();
        result = 31 * result + birthPlace.hashCode();
        result = 31 * result + pictureURL.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "personId=" + personId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                ", birthPlace='" + birthPlace + '\'' +
                ", pictureURL='" + pictureURL + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
