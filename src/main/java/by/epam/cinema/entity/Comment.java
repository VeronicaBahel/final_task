package by.epam.cinema.entity;

import java.io.Serializable;
import java.sql.Date;

/**
 * Class {@code Comment} is an entity class that contains all information about the comment
 * @author Veronica Bahel
 */
public class Comment implements Serializable {
    private long commentId;
    private long userId;
    private String content;
    private Date date;
    private long movieId;

    public Comment() {
    }

    public Comment( long commentId, long userId, String content, Date date, long movieId) {
        this.commentId = commentId;
        this.userId = userId;
        this.content = content;
        this.date = date;
        this.movieId = movieId;
    }

    public Comment(long userId, String content, Date date, long movieId) {
        this.userId = userId;
        this.content = content;
        this.date = date;
        this.movieId = movieId;
    }

    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getMovieId() {
        return movieId;
    }

    public void setMovieId(long movieId) {
        this.movieId = movieId;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commentId=" + commentId +
                ", userId='" + userId + '\'' +
                ", content='" + content + '\'' +
                ", date=" + date +
                ", movieId=" + movieId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comment comment = (Comment) o;

        if (commentId != comment.commentId) return false;
        if (userId != comment.userId) return false;
        if (movieId != comment.movieId) return false;
        if (!content.equals(comment.content)) return false;
        return date.equals(comment.date);
    }

    @Override
    public int hashCode() {
        int result = (int) (commentId ^ (commentId >>> 32));
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 31 * result + content.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + (int) (movieId ^ (movieId >>> 32));
        return result;
    }
}
