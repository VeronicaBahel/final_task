package by.epam.cinema.entity;

import java.io.Serializable;
import java.sql.Date;

/**
 * Class {@code Rating} is is an entity class that contains all information about the rating
 * @author Veronica Bahel
 */
public class Rating implements Serializable {
    private long userId;
    private long movieId;
    private int value;
    private Date date;

    public Rating() {
    }


    public Rating(long userId, long movieId, int value, Date date) {
        this.userId = userId;
        this.movieId = movieId;
        this.value = value;
        this.date = date;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getMovieId() {
        return movieId;
    }

    public void setMovieId(long movieId) {
        this.movieId = movieId;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rating rating = (Rating) o;

        if (userId != rating.userId) return false;
        if (movieId != rating.movieId) return false;
        if (value != rating.value) return false;
        return date.equals(rating.date);
    }

    @Override
    public int hashCode() {
        int result = (int) (userId ^ (userId >>> 32));
        result = 31 * result + (int) (movieId ^ (movieId >>> 32));
        result = 31 * result + value;
        result = 31 * result + date.hashCode();
        return result;
    }


    @Override
    public String toString() {
        return "Rating{" +
                "userLogin='" + userId + '\'' +
                ", movieId=" + movieId +
                ", value=" + value +
                ", date=" + date +
                '}';
    }


}
