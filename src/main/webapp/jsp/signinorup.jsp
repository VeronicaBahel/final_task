<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>

    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.sign_in_lr" bundle="${lang}" var="sign_in_lr" />
    <fmt:message key="locale.sign_up" bundle="${lang}" var="sign_up" />
    <fmt:message key="locale.welcome" bundle="${lang}" var="welcome" />
    <fmt:message key="locale.no_account_yet" bundle="${lang}" var="no_account_yet" />
    <fmt:message key="locale.sign_up_proposition" bundle="${lang}" var="sign_up_proposition" />
    <fmt:message key="locale.get_acquainted" bundle="${lang}" var="get_acquainted" />
    <fmt:message key="locale.already_have_an_account" bundle="${lang}" var="already_have_an_account" />
    <fmt:message key="locale.sign_in_proposition" bundle="${lang}" var="sign_in_proposition" />
    <fmt:message key="locale.username" bundle="${lang}" var="username" />
    <fmt:message key="locale.password" bundle="${lang}" var="password" />
    <fmt:message key="locale.repeat_password" bundle="${lang}" var="repeat_password" />
    <fmt:message key="locale.email" bundle="${lang}" var="email" />
    <fmt:message key="locale.username_tip" bundle="${lang}" var="username_tip" />
    <fmt:message key="locale.password_tip" bundle="${lang}" var="password_tip" />
    <fmt:message key="locale.repeat_password_tip" bundle="${lang}" var="repeat_password_tip" />


    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/settings.css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>
    <script src="${pageContext.request.contextPath}/js/validation.js"></script>


</head>

<body>
<%@ include file="header.jsp" %>
<div class="wrapper">

    <section class="container">



        <div class="mt"></div>
        <div class="mt"></div>

        <c:if test="${not empty errorMessage}">
            <div class="alert alert-danger" id="success-alert">
                <button type="button" class="close" data-dismiss="alert"></button>
                    ${errorMessage}
            </div>
        </c:if>

        <form name="loginForm" id="login_form" class="login" action="controller" method="POST" accept-charset="utf-8">
            <input type="hidden" name="command" value="login" />
            <p class="login__title">${sign_in_lr} <br><span class="login-edition">${welcome}</span></p>

            <div class="field-wrap">
                <input type="text" id='username_login' name="login" placeholder="${username}" class="login__input" required>
                <input type="password" id='password_login' name="password" placeholder="${password}" class="login__input" required>

            </div>

            <div class="login__control">
                <input type="submit" id="submit_button_login" class="btn btn-md btn--warning btn--wider" value="${sign_in_proposition}" />

                <span class="register_message" >${no_account_yet} <a href="#" class="login__tracker form__tracker" >${sign_up_proposition}</a>  </span>
            </div>
        </form>


        <form name="registerForm" id="register_form" class="login register"  action="controller" method="POST" accept-charset="utf-8">
            <input type="hidden" name="command" value="register" />
            <p class="login__title">${sign_up} <br><span class="login-edition">${get_acquainted}</span></p>

            <div class="field-wrap">
                <input type="text" id="username_register" name="registerLogin" placeholder="${username}" class="login__input" onkeyup="validateOnKeyUp()" required />
                <div class="tip"><span class="tip-message">${username_tip}</span></div>
                <input type="email" id="email_register" name="registerEmail" placeholder="${email}" class="login__input" onkeyup="validateOnKeyUp()" required>
                <input type="password" id="password_register" name="registerPassword" placeholder="${password}" class="login__input" onkeyup="validateOnKeyUp()" required>
                <div class="tip"><span class="tip-message">${password_tip}</span></div>
                <input type="password" id="repeat_password_register" placeholder="${repeat_password}" class="login__input" onkeyup="validateOnKeyUp()" required>
                <div class="tip"><span class="tip-message">${repeat_password_tip}</span></div>


            </div>

            <div class="login__control">
                <input type="submit" id="submit_button_register" class="btn btn-md btn--warning btn--wider" value="${sign_up_proposition}" disabled />

                <span class="register_message" > ${already_have_an_account} <a href="#" class="login__tracker form__tracker" >${sign_in_proposition}</a>  </span>
            </div>
        </form>


    </section>
    <div class="clearfix"></div>
</div>

<%@ include file="footer.jsp" %>


</body>
</html>
