<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>

    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.coming_soon" bundle="${lang}" var="coming_soon" />
    <fmt:message key="locale.no_movies" bundle="${lang}" var="no_movies" />
    <fmt:message key="locale.previous" bundle="${lang}" var="previous" />
    <fmt:message key="locale.next" bundle="${lang}" var="next" />

    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/settings.css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>



</head>

<body>
<div class="wrapper">

    <%@ include file="header.jsp" %>
    <%@ include file="search.jsp" %>

    <section class="container">


        <div class="clearfix"></div>

        <h2 class="page-heading heading--outcontainer">${coming_soon}</h2>

        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-8 col-md-9">
                    <c:choose>
                        <c:when test="${not empty movies}">
                            <c:forEach items="${movies}" var="item">
                                <div class="movie movie--test movie--test--light movie--test--left">
                                    <div class="movie__images">
                                        <a href="controller?command=view_single_movie&movieId=${item.movieId}" class="movie-beta__link">
                                            <div class="thumbnail">
                                                <img src="${item.posterURL}" class="portrait">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="movie__info">
                                        <a href="controller?command=view_single_movie&movieId=${item.movieId}" class="movie__title">${item.title}</a>

                                        <p class="movie__option">
                                            <c:forEach items="${item.genres}" var="genre" varStatus="genreLoop">
                                                <a href="controller?command=view_with_genre&genre=${genre.label}">${genre.label}</a> ${!genreLoop.last ? '| ' : ''}
                                            </c:forEach>
                                        </p>

                                        <div class="movie__rate">
                                            <div class="score"></div>
                                            <span class="movie__rating">${item.averageRating}</span>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>${no_movies}</c:otherwise>
                    </c:choose>

                </div>
            </div>
        </div>
        <div class="coloum-wrapper">
            <div class="pagination paginatioon--full">


                <c:if test="${currentPageNumber != 1}">
                    <a href="controller?command=view_coming_soon&pageNumber=${currentPageNumber - 1}" class="pagination__prev">${previous}</a>
                </c:if>

                <center>
                    <c:forEach begin="1" end="${numberOfPages}" var="i">
                        <c:choose>
                            <c:when test="${currentPageNumber eq i}">
                                <td>${i}</td>
                            </c:when>
                            <c:otherwise>
                                <td><a href="controller?command=view_coming_soon&pageNumber=${i}">${i}</a></td>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </center>

                <c:if test="${currentPageNumber lt numberOfPages}">
                    <td><a href="controller?command=view_coming_soon&pageNumber=${currentPageNumber + 1}" class="pagination__next">${next}</a></td>
                </c:if>
            </div>
        </div>

    </section>

    <div class="clearfix"></div>

</div>

<%@ include file="footer.jsp" %>

</body>
</html>
