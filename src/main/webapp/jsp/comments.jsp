<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>

    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.delete" bundle="${lang}" var="delete" />
    <fmt:message key="locale.to_the_movie" bundle="${lang}" var="to_the_movie" />
    <fmt:message key="locale.comments" bundle="${lang}" var="comments_label" />
    <fmt:message key="locale.no_comments" bundle="${lang}" var="no_comments" />

    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/settings.css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>
</head>
<body>
<%@ include file="header.jsp" %>

<div class="wrapper">

    <%@ include file="search.jsp" %>

    <div class="clearfix"></div>

    <section class="container">

        <div class="comment-wrapper">

            <h2 class="page-heading heading--outcontainer">${comments_label}</h2>

            <div class="comment-sets comment--light">
                <c:choose>
                    <c:when test="${not empty comments}">
                        <c:forEach items="${comments}" var="entry">
                            <div class="comment">
                                <a href='#' class="comment__author">${to_the_movie} '${entry.key.title}'</a>
                                <p class="comment__date">${entry.value.date}</p>
                                <p class="comment__message">${entry.value.content}</p>
                                <c:choose>
                                    <c:when test="${(loggedUser.role eq 'ADMIN') or (loggerUser.userId eq entry.value.userId)}">
                                        <a href="controller?command=delete_comment&userId=${entry.value.userId}&commentId=${entry.value.commentId}&movieId=${entry.value.movieId}" class="comment__reply">${delete}</a>
                                    </c:when>
                                </c:choose>
                            </div>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        ${no_comments}
                    </c:otherwise>
                </c:choose>



            </div>
        </div>
    </section>


    <div class="clearfix"></div>


</div>


<%@ include file="footer.jsp" %>
</body>
</html>
