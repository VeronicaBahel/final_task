<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/taglib/ageTaglib.tld" prefix="age"%>

<html>
<head>

    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.edit" bundle="${lang}" var="edit" />
    <fmt:message key="locale.delete_picture" bundle="${lang}" var="delete_picture" />
    <fmt:message key="locale.delete_profile" bundle="${lang}" var="delete_profile" />
    <fmt:message key="locale.no_data" bundle="${lang}" var="no_data" />
    <fmt:message key="locale.rates_count" bundle="${lang}" var="rates_count" />
    <fmt:message key="locale.comments_count" bundle="${lang}" var="comments_count" />
    <fmt:message key="locale.no_stats_yet" bundle="${lang}" var="no_stats_yet" />
    <fmt:message key="locale.sure" bundle="${lang}" var="sure" />
    <fmt:message key="locale.delete_your_profile" bundle="${lang}" var="delete_your_profile" />
    <fmt:message key="locale.delete_image" bundle="${lang}" var="delete_image" />
    <fmt:message key="locale.cancel" bundle="${lang}" var="cancel" />
    <fmt:message key="locale.delete" bundle="${lang}" var="delete" />
    <fmt:message key="locale.banned_message" bundle="${lang}" var="banned_message" />



    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/settings.css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/external/jquery.selectbox.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/external/idangerous.swiper.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>

</head>

<body>
<%@ include file="header.jsp" %>
<div class="wrapper">



    <%@ include file="search.jsp" %>

    <section class="container">
        <c:if test="${loggedUser.role eq 'BANNED'}">
            <div class="alert alert-danger" id="success-alert">
                    ${banned_message}
            </div>
        </c:if>

        <h2 class=" heading--outcontainer center">
            <img src="${user.imageURL}" class="round-img width100" />

            <c:if test="${loggedUser.userId eq user.userId}">
                <div><a href="#" data-toggle="modal" data-target="#pictureModal" class="contact__delete-pic mt">${delete_picture}</a></div>


            </c:if>

            <div class="col-sm-6 col-sm-offset-3 modal fade" id="pictureModal" tabindex="-1" role="dialog" aria-labelledby="pictureModal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <center>${sure} ${delete_picture}</center>

                        </div>
                        <div class="modal-footer">
                            <a href="controller?command=delete_profile_picture&userId=${loggedUser.userId}"
                               class="btn btn-md btn--danger comment-form__btn btn-modal-left">${delete}</a>
                            <a href="" class="btn btn-md btn--danger comment-form__btn" data-dismiss="modal">${cancel}</a>
                        </div>
                    </div>
                </div>
            </div>
        </h2>
        <div class="contact">
            <p class="contact__title"> ${user.login}, <br>
                <span class="contact__describe">
                    <c:if test="${(user.firstName eq null) and (user.lastName eq null)}">${no_data}</c:if>
                    <c:if test="${user.firstName ne null}">${user.firstName}</c:if>
                    <c:if test="${user.lastName ne null}"> ${user.lastName}</c:if>
                </span>
            </p>

            <span class="contact__location">
                <c:choose>
                    <c:when test="${user.location ne null}">${user.location}</c:when>
                    <c:otherwise>${no_data}</c:otherwise>
                </c:choose>
            </span>
            <span class="contact__mail">${user.email}</span>
            <span class="contact__birthday">
                <c:choose>
                    <c:when test="${user.birthday ne null}"><age:calculateAge birthday="${user.birthday}"/></c:when>
                    <c:otherwise>${no_data}</c:otherwise>
                </c:choose>

            </span> <br/>

            <c:if test="${loggedUser.userId eq user.userId}">
                <a href="controller?command=update_profile_page&userId=${loggedUser.userId}" class="contact__edit">${edit}</a>
                <a href="#" data-toggle="modal" data-target="#profileModal" class="contact__delete-profile">${delete_profile}</a>

                <div class="col-sm-6 col-sm-offset-3 modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="profileModal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <center>${sure} ${delete_your_profile}</center>

                            </div>
                            <div class="modal-footer">
                                <a href="controller?command=delete_profile&userId=${loggedUser.userId}"
                                   class="btn btn-md btn--danger comment-form__btn btn-modal-left">${delete}</a>
                                <a href="" class="btn btn-md btn--danger comment-form__btn" data-dismiss="modal">${cancel}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>
        </div>

        <c:choose>
            <c:when test="${loggedUser.role ne 'BANNED'}">
                <div class="statistics">
                    <a href="controller?command=view_user_rates&userId=${user.userId}" class="statistic_value">
                <span class="statistic_number">
                    <c:choose>
                        <c:when test="${userStatistic[1] != 0}">${userStatistic[1]}</c:when>
                        <c:otherwise>
                            ${no_stats_yet}
                        </c:otherwise>
                    </c:choose>

                </span>
                        <span class="statistic_label">${rates_count}</span>
                    </a>
                    <a href="controller?command=view_user_comments&userId=${user.userId}" class="statistic_value">
                <span class="statistic_number">
                    <c:choose>
                        <c:when test="${userStatistic[0] != 0 }">${userStatistic[0]}</c:when>
                        <c:otherwise>
                            ${no_stats_yet}
                        </c:otherwise>
                    </c:choose>
                </span>
                        <span class="statistic_label">${comments_count}</span>
                    </a>
                </div>
            </c:when>
        </c:choose>



    </section>


    <div>


    </div>




    <div class="clearfix"></div>

</div>



<%@ include file="footer.jsp" %>

</body>
</html>
