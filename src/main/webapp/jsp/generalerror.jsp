<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isErrorPage="true" %>

<html>
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.slogan" bundle="${lang}" var="slogan" />
    <fmt:message key="locale.return_to_homepage" bundle="${lang}" var="return_to_homepage" />
    <fmt:message key="locale.something_went_wrong" bundle="${lang}" var="something_went_wrong" />
    <fmt:message key="locale.copyright" bundle="${lang}" var="copyright" />

    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />


</head>

<body>
<div class="wrapper">
    <div class="error-wrapper">
        <a href="controller?command=default_command" class="logo logo--dark">
            <img src="${pageContext.request.contextPath}/img/logo-dark.png">
            <p class="slogan--dark">${slogan}</p>
        </a>

        <div class="error">
            <img src='${pageContext.request.contextPath}/img/oops.png' class="error__image">
            <h1 class="error__text">${pageContext.errorData.statusCode}</h1>
            <h1 class="error__text">${something_went_wrong}</h1>
            <h1 class="error__text">${errorMessage}</h1>
            <a href="controller?command=default_command" class="btn btn-md btn--warning">${return_to_homepage}</a>
        </div>
    </div>

    <div class="copy-bottom">
        <p class="copy">${copyright}</p>
    </div>

</div>

</body>
</html>

