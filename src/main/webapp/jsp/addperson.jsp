<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.first_name" bundle="${lang}" var="first_name" />
    <fmt:message key="locale.last_name" bundle="${lang}" var="last_name" />
    <fmt:message key="locale.birthday" bundle="${lang}" var="birthday" />
    <fmt:message key="locale.birth_place" bundle="${lang}" var="birth_place" />
    <fmt:message key="locale.picture" bundle="${lang}" var="picture" />
    <fmt:message key="locale.save" bundle="${lang}" var="save" />

    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/settings.css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>
    <script src="${pageContext.request.contextPath}/js/validation.js"></script>
</head>
<body>

<%@ include file="header.jsp" %>


<div class="wrapper">

    <%@ include file="search.jsp" %>

    <div class="clearfix"></div>
    <section class="container">
        <div class="contact-form-wrapper">
        <div class="container mt ">
            <div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">


                <form id='contact-form' class="form row" method='post' action="controller" accept-charset="utf-8" enctype="multipart/form-data">
                    <input type="hidden" name="command" value="${ambiguousCommand}" />
                    <input type="hidden" name="personId" value="${person.personId}"/>

                    <div class="form-group">
                        <label for="personFirstName" class="col-sm-2 pt ">${first_name}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form__name" id="personFirstName" name="personFirstName" onkeyup="validateAddingPerson()"
                                   value="${person.firstName}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="personLastName" class="col-sm-2 pt ">${last_name}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form__name" id="personLastName" name="personLastName" onkeyup="validateAddingPerson()"
                                   value="${person.lastName}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="personBirthday" class="col-sm-2 pt ">${birthday}</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control form__name" id="personBirthday" name="personBirthday" onkeyup="validateAddingPerson()"
                                   value="${person.birthday}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="personBirthPlace" class="col-sm-2 pt ">${birth_place}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form__name" id="personBirthPlace" name="personBirthPlace"  onkeyup="validateAddingPerson()"
                                   value="${person.birthPlace}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="personPictureURL" class="col-sm-2 pt ">${picture}</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control form__name" id="personPictureURL" accept="image/*" name="personPictureURL">
                        </div>
                    </div>

                    <input type="submit" class='btn btn-md btn--danger' id="saveButton" value="${save}" disabled />

                </form>
            </div>
        </div>
    </div>
    </section>

    <div class="clearfix"></div>


</div>


<%@ include file="footer.jsp" %>
</body>
</html>
