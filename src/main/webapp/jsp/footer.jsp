<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="localization" var="lang" />
<fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
<fmt:message key="locale.in_the_social_media" bundle="${lang}" var="in_the_social_media" />
<fmt:message key="locale.copyright" bundle="${lang}" var="copyright" />
<fmt:message key="locale.coming_soon" bundle="${lang}" var="coming_soon" />
<fmt:message key="locale.top100" bundle="${lang}" var="top100" />
<fmt:message key="locale.english" bundle="${lang}" var="english" />
<fmt:message key="locale.russian" bundle="${lang}" var="russian" />






<footer class="footer-wrapper">
    <section class="container">
        <div class="col-xs-4 col-md-2 footer-nav">
            <ul class="nav-link">
                <li class="nav-link__item"><a href="controller?command=view_coming_soon">${coming_soon}</a></li>
                <li class="nav-link__item"><a href="controller?command=view_top_100">${top100}</a></li>
            </ul>
        </div>
        <div class="col-xs-4 col-md-2 footer-nav">
            <ul class="nav-link">
                <li><a href="controller?command=change_locale&locale=en&page=${pageContext.request.requestURI}" class="nav-link__item">${english}</a></li>
                <li><a href="controller?command=change_locale&locale=ru&page=${pageContext.request.requestURI}" class="nav-link__item">${russian}</a></li>
            </ul>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="footer-info">
                <p class="heading-special--small">${amovie}<br><span class="title-edition">${in_the_social_media}</span></p>


                <div class="clearfix"></div>
                <p class="copy">${copyright}</p>
            </div>
        </div>
    </section>
</footer>

<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.themepunch.revolution.min.js"></script>
<script src="${pageContext.request.contextPath}/js/form.js"></script>
<script src="${pageContext.request.contextPath}/js/custom.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>