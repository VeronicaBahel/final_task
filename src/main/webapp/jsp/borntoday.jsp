<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.no_one_born_today" bundle="${lang}" var="no_one_born_today" />
    <fmt:message key="locale.people_label" bundle="${lang}" var="people_label" />
    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/settings.css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>
</head>
<body>
<div class="wrapper">

    <%@ include file="header.jsp" %>

    <div class="wrapper">

        <%@ include file="search.jsp" %>

        <section class="container">
            <div class="col-sm-12">
                <h2 class="page-heading">${people_label}</h2>

                <div class="row">
                    <div class="gallery-wrapper">
                        <c:choose>
                            <c:when test="${people.size() ne 0}">
                                <c:forEach items="${people}" var="person">
                                    <div class="col-sm-4 col-md-3">
                                        <div class="gallery-item">
                                            <a href='controller?command=view_person&personId=${person.personId}' class="gallery-item__image gallery-item--video">
                                                <img alt='' src="${person.pictureURL}">
                                            </a>
                                            <a href='controller?command=view_person&personId=${person.personId}' class="gallery-item__descript gallery-item--video-link">
                                                <span class="gallery-item__icon"></span>
                                                <p class="gallery-item__name">${person.firstName} ${person.lastName}</p>
                                            </a>
                                        </div>
                                    </div>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                ${no_one_born_today}
                            </c:otherwise>
                        </c:choose>


                    </div>
                </div>
            </div>

        </section>

        <div class="clearfix"></div>

    </div>

    <%@ include file="footer.jsp" %>

</body>
</html>

