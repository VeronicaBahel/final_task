
var tagRegexp = /^[^<>]*$/;
var usernameRegEx = /^[a-zA-Z]+([-_\'][a-zA-Z]+)*$/;
var emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var passwordRegEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,25}$/;

function validateOnKeyUp(){
	var loginForm = document.forms["login_form"];
    var loginUsername = loginForm["username_login"];
    var loginPassword = loginForm["password_login"];
	var loginValidationArray = new Array();
	loginValidationArray.push(validateUsername(loginUsername, usernameRegEx));
	loginValidationArray.push(validateWithRegExp(loginPassword, passwordRegEx));
	if(loginValidationArray.includes(false)){
		document.getElementById('submit_button_login').disabled = true;
	} else {
		document.getElementById('submit_button_login').disabled = false; 
	}

	var registerForm = document.forms["register_form"];
	var registerUsername = registerForm["username_register"];
	var registerEmail = registerForm["email_register"];
    var registerPassword = registerForm["password_register"];
    var registerRepeatPassword = registerForm["repeat_password_register"];
    var registerValidationArray = new Array();
    registerValidationArray.push(validateUsername(registerUsername, usernameRegEx));
    registerValidationArray.push(validateWithRegExp(registerEmail, emailRegEx));
	registerValidationArray.push(validateWithRegExp(registerPassword, passwordRegEx));
	registerValidationArray.push(validatePasswordConfirmation(registerRepeatPassword, registerPassword));
	if(registerValidationArray.includes(false)){
		document.getElementById('submit_button_register').disabled = true;
	} else {
		document.getElementById('submit_button_register').disabled = false; 
	}

}

function validateAddingComment(){
    var commentForm = document.forms["addCommentForm"];
    var content = commentForm["commentContent"];
    var commentValidationArray = new Array();
    commentValidationArray.push(validateComment(content, tagRegexp));
    if(commentValidationArray.includes(false)){
        document.getElementById('addButton').disabled = true;
    } else {
        document.getElementById('addButton').disabled = false;
    }
}

function validateAddingMovie(){
    var addingForm = document.forms["contact-form"];
    var title = addingForm["title"];
    var budget = addingForm["budget"];
    var briefDesc = addingForm["briefDescription"];
    var ageLimit = addingForm["ageLimit"];
    var worldPremiere = addingForm["worldPremiere"];
    var descValidationArray = new Array();
    descValidationArray.push(validateWithRegExp(briefDesc, tagRegexp));
    descValidationArray.push(validateBriefDesc(briefDesc));
    descValidationArray.push(validateBudget(budget));
    descValidationArray.push(validateAgeLimit(ageLimit));
    descValidationArray.push(validateWithRegExp(title, tagRegexp));
    descValidationArray.push(validateNotNull(worldPremiere));
    if(descValidationArray.includes(false)){
        document.getElementById('saveButton').disabled = true;
    } else {
        document.getElementById('saveButton').disabled = false;
    }
}

function validateAddingPerson(){
    var addingForm = document.forms["contact-form"];
    var firstName = addingForm["personFirstName"];
    var lastName = addingForm["personLastName"];
    var birthday = addingForm["personBirthday"];
    var birthPlace = addingForm["personBirthPlace"];

    var descValidationArray = new Array();
    descValidationArray.push(validateWithRegExp(firstName, tagRegexp));
    descValidationArray.push(validateWithRegExp(lastName, tagRegexp));
    descValidationArray.push(validateWithRegExp(birthPlace, tagRegexp));
    descValidationArray.push(validateNotNull(birthday));
    if(descValidationArray.includes(false)){
        document.getElementById('saveButton').disabled = true;
    } else {
        document.getElementById('saveButton').disabled = false;
    }
}


function makeValid(element){
	if(element.className == "invalid" || element.className == "login__input") element.className = "valid";
}

function makeInvalid(element){
	element.className = "invalid";
}

function validateUsername(username, usernameRegEx){
	if((username.value.match(usernameRegEx) == null) || (username.value.length < 5) || (username.value.length > 100)){
		makeInvalid(username);
		return false;
	} else {
		makeValid(username);
		return true;
	}
}

function validateComment(content, tagRegexp){
    if((content.value.match(tagRegexp) == null) || (content.value.length < 10)){
        makeInvalid(content);
        return false;
    } else {
        makeValid(content);
        return true;
    }
}


function validateWithRegExp(element, regExp){
	if(element.value.match(regExp) == null || element.value == ""){
		makeInvalid(element);
		return false;
	} else{
		makeValid(element);
		return true;
	}
}

function validatePasswordConfirmation(confirmPassword, password){
	if(confirmPassword.value != password.value || confirmPassword.value == "" ){
		makeInvalid(confirmPassword);
		return false;
	} else {
		makeValid(confirmPassword);
		return true;
	}
}
function validateBudget(budget) {
	if(budget.value < 1000){
		makeInvalid(budget);
		return false;
	} else{
		makeValid(budget);
		return true;
	}
	
}

function validateAgeLimit(ageLimit) {
    if(ageLimit.value < 0 || ageLimit.value > 21){
        makeInvalid(ageLimit);
        return false;
    } else{
        makeValid(ageLimit);
        return true;
    }

}

function validateBriefDesc(briefDesc) {
    if(briefDesc.value.length < 60 ){
        makeInvalid(briefDesc);
        return false;
    } else{
        makeValid(briefDesc);
        return true;
    }

}

function validateNonEfficient(element, regExp){
    if(element.value.match(element) == null && element.value != ""  ){
        makeInvalid(element);
        return false;
    } else{
        makeValid(element);
        return true;
    }
}

function validateNotNull(element){
	if(!element.value){
        makeInvalid(element);
        return false;
	} else{
        makeValid(element);
        return true;
	}
}