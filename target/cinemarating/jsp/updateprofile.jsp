<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>

    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.your_first_name" bundle="${lang}" var="your_first_name" />
    <fmt:message key="locale.your_last_name" bundle="${lang}" var="your_last_name" />
    <fmt:message key="locale.your_email" bundle="${lang}" var="your_email" />
    <fmt:message key="locale.your_location" bundle="${lang}" var="your_location" />
    <fmt:message key="locale.apply_changes" bundle="${lang}" var="apply_changes" />

    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/settings.css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>
    <script src="${pageContext.request.contextPath}/js/validation.js"></script>

</head>

<body>
<div class="wrapper">


    <%@ include file="header.jsp" %>

    <div class="contact-form-wrapper">
        <div class="container">
            <div class="col-sm-12  col-lg-8 col-lg-offset-2">
                <form name="updateProfileForm" id='contact-form' class="form row" action="controller" method="POST" accept-charset="utf-8" enctype="multipart/form-data" >

                    <input type="hidden" name="command" value="update_profile" />
                    <input type="hidden" name="userId" value="${loggedUser.userId}" />

                    <h2 class=" heading--outcontainer center">
                        <img src="${loggedUser.imageURL}" class="round-img width100" /><br/>
                        <input type="file" class="form__file" accept="image/*" name="userImageURL" />


                    </h2>
                    <div class="col-sm-6">
                        <input type='text' id="userFirstName" name="userFirstName" placeholder="${your_first_name}"
                               value="${loggedUser.firstName}" class="form__name">
                    </div>
                    <div class="col-sm-6">
                        <input type='text' id="userLastName" name="userLastName" placeholder="${your_last_name}"
                               value="${loggedUser.lastName}" class="form__name">
                    </div>
                    <div class="col-sm-12">
                        <input type='email' id="userEmail" name="userEmail" placeholder="${your_email}"
                               value="${loggedUser.email}" class="form__mail" required>
                    </div>
                    <div class="col-sm-12">
                        <input type='text' id="userLocation" name="userLocation" placeholder="${your_location}"
                               value="${loggedUser.location}" class="form__name">
                    </div>

                    <div class="col-sm-12">
                        <input type='date' name="userBirthday"
                               value="${loggedUser.birthday}" class="form__name">
                    </div>

                    <button type="submit" id="saveButton" class='btn btn-md btn--danger'>${apply_changes}</button>
                </form>
            </div>
        </div>
    </div>


    <div class="clearfix"></div>


</div>
<%@ include file="footer.jsp" %>

</body>
</html>
