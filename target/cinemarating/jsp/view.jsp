<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>

    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.best_choice" bundle="${lang}" var="best_choice" />
    <fmt:message key="locale.find_movie_due" bundle="${lang}" var="find_movie_due" />
    <fmt:message key="locale.your_mood" bundle="${lang}" var="your_mood" />
    <fmt:message key="locale.find" bundle="${lang}" var="find" />
    <fmt:message key="locale.the_best" bundle="${lang}" var="the_best" />
    <fmt:message key="locale.most_discussed" bundle="${lang}" var="most_discussed" />
    <fmt:message key="locale.tv" bundle="${lang}" var="tv" />

    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/settings.css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>



</head>

<body>
<div class="wrapper">

    <%@ include file="header.jsp" %>

    <section class="container">
        <div class="movie-best">
            <div class="col-sm-10 col-sm-offset-1 movie-best__rating">${best_choice}</div>
            <div class="col-sm-12 change--col">
                <c:forEach items="${topMovies}" var="item">
                    <a href="controller?command=view_single_movie&movieId=${item.movieId}"><div class="movie-beta__item ">
                        <img alt='' src="${item.posterURL}">
                        <span class="best-rate">${item.averageRating}</span>
                    </div></a>
                </c:forEach>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="mega-select-present mega-select-top mega-select--full">
                <div class="mega-select-marker">

                    <div class="marker-indecator film-category">
                        <p class="select-marker"><span>${find_movie_due} </span> <br> ${your_mood}</p>
                    </div>
                </div>

                <div class="mega-select pull-right">
                    <ul class="mega-select__sort">
                        <li class="filter-wrap">
                            <a href="#" class="mega-select__filter filter--active" data-filter='film-category'></a>
                        </li>
                    </ul>
                    <form name="searchForm" id="searchForm" action="controller" method="POST" accept-charset="utf-8">
                        <input type="hidden" name="command" value="search" />
                        <input name="parameter" type='text' class="select__field">
                        <div class="select__btn">
                            <button type="submit" class="btn btn-md btn--danger film-category">${find} <span class="hidden-exrtasm">${the_best}</span></button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <h2 class="page-heading heading--outcontainer">${most_discussed}</h2>

        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-8 col-md-9">
                    <c:forEach items="${mostDiscussed}" var="item" begin="0" end="10">
                        <div class="movie movie--test movie--test--light movie--test--left">
                            <div class="movie__images">
                                <a href="controller?command=view_single_movie&movieId=${item.movieId}" class="movie-beta__link">
                                    <div class="thumbnail">
                                        <img src="${item.posterURL}" class="portrait">
                                    </div>
                                </a>
                            </div>
                            <div class="movie__info">
                                <a href="controller?command=view_single_movie&movieId=${item.movieId}" class="movie__title">${item.title}</a>


                                <p class="movie__option">
                                    <c:forEach items="${item.genres}" var="genre" varStatus="genreLoop">
                                        <a href="controller?command=view_with_genre&genre=${genre.label}">${genre.label}</a> ${!genreLoop.last ? '| ' : ''}
                                    </c:forEach>
                                </p>

                                <div class="movie__rate">
                                    <div class="score"></div>
                                    <span class="movie__rating">${item.averageRating}</span>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <h2 class="page-heading">${tv}</h2>

            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-8 col-md-9">
                        <c:forEach items="${tvSeries}" var="item" begin="0" end="10">
                            <div class="movie movie--test movie--test--light movie--test--left">
                                <div class="movie__images">
                                    <a href="controller?command=view_single_movie&movieId=${item.movieId}" class="movie-beta__link">
                                        <div class="thumbnail">
                                            <img src="${item.posterURL}" class="portrait">
                                        </div>
                                    </a>
                                </div>
                                <div class="movie__info">
                                    <a href="controller?command=view_single_movie&movieId=${item.movieId}" class="movie__title">${item.title}</a>

                                    <p class="movie__time">146 min</p>

                                    <p class="movie__option">
                                        <c:forEach items="${item.genres}" var="genre" varStatus="genreLoop">
                                            <a href="controller?command=view_with_genre&genre=${genre.label}">${genre.label}</a> ${!genreLoop.last ? '| ' : ''}
                                        </c:forEach>
                                    </p>

                                    <div class="movie__rate">
                                        <div class="score"></div>
                                        <span class="movie__rating">${item.averageRating}</span>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <div class="clearfix"></div>

</div>

<%@ include file="footer.jsp" %>

</body>
</html>
