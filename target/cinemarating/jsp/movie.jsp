<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>

    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.categories" bundle="${lang}" var="categories" />
    <fmt:message key="locale.release_date" bundle="${lang}" var="release_date" />
    <fmt:message key="locale.directors" bundle="${lang}" var="directors" />
    <fmt:message key="locale.actors" bundle="${lang}" var="actors" />
    <fmt:message key="locale.age_restriction" bundle="${lang}" var="age_restriction" />
    <fmt:message key="locale.budget" bundle="${lang}" var="budget" />
    <fmt:message key="locale.comments" bundle="${lang}" var="comments_name" />
    <fmt:message key="locale.edit" bundle="${lang}" var="edit" />
    <fmt:message key="locale.add_to_watchlist" bundle="${lang}" var="add_to_watchlist" />
    <fmt:message key="locale.remove_from_watchlist" bundle="${lang}" var="remove_from_watchlist" />
    <fmt:message key="locale.the_plot" bundle="${lang}" var="the_plot" />
    <fmt:message key="locale.media" bundle="${lang}" var="media" />
    <fmt:message key="locale.photos" bundle="${lang}" var="photos" />
    <fmt:message key="locale.videos" bundle="${lang}" var="videos" />
    <fmt:message key="locale.characters_left" bundle="${lang}" var="characters_left" />
    <fmt:message key="locale.add_comment" bundle="${lang}" var="add_comment" />
    <fmt:message key="locale.delete" bundle="${lang}" var="delete" />
    <fmt:message key="locale.your_vote" bundle="${lang}" var="your_vote" />
    <fmt:message key="locale.top6" bundle="${lang}" var="top6" />
    <fmt:message key="locale.movies_lr" bundle="${lang}" var="movies_lr" />
    <fmt:message key="locale.the_most" bundle="${lang}" var="the_most" />
    <fmt:message key="locale.discussed_posts" bundle="${lang}" var="discussed_posts" />
    <fmt:message key="locale.add_your_comment" bundle="${lang}" var="add_your_comment" />
    <fmt:message key="locale.not_allowed" bundle="${lang}" var="not_allowed" />
    <fmt:message key="locale.no_trailer" bundle="${lang}" var="no_trailer" />
    <fmt:message key="locale.sure" bundle="${lang}" var="sure" />
    <fmt:message key="locale.delete_comment" bundle="${lang}" var="delete_comment" />
    <fmt:message key="locale.delete_rating" bundle="${lang}" var="delete_rating" />
    <fmt:message key="locale.cancel" bundle="${lang}" var="cancel" />
    <fmt:message key="locale.permission" bundle="${lang}" var="permission" />
    <fmt:message key="locale.remove_movie" bundle="${lang}" var="remove_movie" />
    <fmt:message key="locale.from_favourites" bundle="${lang}" var="from_favourites" />
    <fmt:message key="locale.comment_tip" bundle="${lang}" var="comment_tip" />

    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/settings.css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>
    <script src="${pageContext.request.contextPath}/js/validation.js"></script>



</head>

<body>
<%@ include file="header.jsp" %>

<div class="wrapper">


    <%@ include file="search.jsp" %>

    <section class="container">

        <h2 class="page-heading heading--outcontainer">${singleMovie.title}
            <c:if test="${singleMovie.originalTitle ne null}"> (${singleMovie.originalTitle})</c:if>
            </h2>
        <div class="col-sm-8 col-md-9 col-sm-push-4 col-md-push-3">
            <div class="movie">

                <div class="movie__info">
                    <div class="col-sm-6 col-md-4 movie-mobile">
                        <div class="movie__images">
                            <span class="movie__rating">${singleMovie.averageRating}</span>
                            <img src="${singleMovie.posterURL}">
                        </div>


                        <div class="movie__rate">${your_vote}: <br/>
                            <c:choose>
                                <c:when test="${not empty loggedUser and loggedUser.role ne 'BANNED'}">
                                    <form action="controller" name="rateForm" id="rateForm" method="POST" class="comment-form">
                                        <input type="hidden" name="command" value="rate" />
                                        <input type="hidden" name="userId" value="${loggedUser.userId}" />
                                        <input type="hidden" name="movieId" value="${singleMovie.movieId}" />
                                        <span class="star-cb-group">

                                                <input type="radio" id="rating-5" name="rating" value="5"
                                                        <c:choose>
                                                            <c:when test="${loggedUserRating eq 5 }">
                                                                checked="checked"
                                                            </c:when>
                                                            <c:otherwise></c:otherwise>
                                                        </c:choose>
                                                /><label for="rating-5">5</label>
                                                <input type="radio" id="rating-4" name="rating" value="4"
                                                        <c:choose>
                                                            <c:when test="${ loggedUserRating eq 4}">
                                                                checked="checked"
                                                            </c:when>
                                                            <c:otherwise></c:otherwise>
                                                        </c:choose>
                                                /><label for="rating-4">4</label>
                                                <input type="radio" id="rating-3" name="rating" value="3"
                                                        <c:choose>
                                                            <c:when test="${loggedUserRating eq 3}">
                                                                checked="checked"
                                                            </c:when>
                                                            <c:otherwise></c:otherwise>
                                                        </c:choose>
                                                /><label for="rating-3">3</label>
                                                <input type="radio" id="rating-2" name="rating" value="2"
                                                        <c:choose>
                                                            <c:when test="${ loggedUserRating eq 2 }">
                                                                checked="checked"
                                                            </c:when>
                                                            <c:otherwise></c:otherwise>
                                                        </c:choose>
                                                /><label for="rating-2">2</label>
                                                <input type="radio" id="rating-1" name="rating" value="1"
                                                        <c:choose>
                                                            <c:when test="${ loggedUserRating eq 1}">
                                                                checked="checked"
                                                            </c:when>
                                                            <c:otherwise></c:otherwise>
                                                        </c:choose>
                                                /><label for="rating-1">1</label>
                                                <input type="radio" id="rating-0" name="rating" value="0" class="star-cb-clear" /><label for="rating-0">0</label>
                                                <c:choose>
                                                    <c:when test="${loggedUserRating ne 0}">
                                                        <a href="#" data-toggle="modal" data-target="#ratingModal" class="delete-rating"></a>
                                                    </c:when>
                                                    <c:otherwise />
                                                </c:choose>





                                    </span>
                                    </form>


                                </c:when>
                                <c:otherwise> ${permission} </c:otherwise>
                            </c:choose>

                        </div>
                    </div>

                    <div class="col-sm-6 col-md-8">

                        <p class="movie__option">
                            <strong>${categories}: </strong>
                            <c:forEach items="${singleMovie.genres}" var="genre" varStatus="genreLoop">
                                <a href="${pageContext.request.contextPath}/controller?command=view_with_genre&genre=${genre.label}">${genre.label}</a>${!genreLoop.last ? ',' : ''}
                            </c:forEach>
                        </p>
                        <p class="movie__option"><strong>${release_date}: </strong>${singleMovie.worldPremiere}</p>
                        <p class="movie__option"><strong>${directors}: </strong>
                            <c:forEach items="${singleMovie.people}" var="director" varStatus="directorLoop">
                                <c:choose>
                                    <c:when test="${director.role eq 'DIRECTOR'}">
                                        <a href="controller?command=view_person&personId=${director.personId}">${director.firstName} ${director.lastName}</a>${!directorLoop.last ? ',' : ''}
                                    </c:when>
                                </c:choose>
                            </c:forEach>
                        </p>
                        <p class="movie__option"><strong>${actors}: </strong>
                            <c:forEach items="${singleMovie.people}" var="actor" varStatus="actorLoop">
                                <c:choose>
                                    <c:when test="${actor.role eq 'ACTOR'}">
                                        <a href="controller?command=view_person&personId=${actor.personId}">${actor.firstName} ${actor.lastName}</a>${!actorLoop.last ? ',' : ''}
                                    </c:when>
                                </c:choose>
                            </c:forEach>
                        </p>
                        <p class="movie__option"><strong>${age_restriction}: </strong><span>${singleMovie.ageLimit}</span></p>
                        <p class="movie__option"><strong>${budget}: </strong><span>$ ${singleMovie.budget}</span></p>

                        <a href="#commentsSection" class="comment-link">${comments_name}:  ${comments.size()}</a>

                        <div class="movie__btns">
                            <c:choose>
                                <c:when test="${loggedUser.role eq 'ADMIN'}">
                                    <a href="${pageContext.request.contextPath}/controller?command=update_movie_page&movieId=${singleMovie.movieId}" class="btn btn-md btn--warning">${edit}</a>
                                </c:when>
                            </c:choose>
                            <c:if test="${not empty loggedUser}">
                                <c:choose>
                                    <c:when test="${not inWatchlist}">
                                        <a href="${pageContext.request.contextPath}/controller?command=add_to_favourites&movieId=${singleMovie.movieId}&userId=${loggedUser.userId}" class="watchlist">${add_to_watchlist}</a>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="#" data-toggle="modal" data-target="#removeFavouritesModal" class="watchlist" >${remove_from_watchlist}</a>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                        </div>

                    </div>
                </div>

                <div class="clearfix"></div>

                <h2 class="page-heading">${the_plot}</h2>

                <p class="movie__describe">${singleMovie.briefDescription}</p>

                <h2 class="page-heading">${media}</h2>

                <div class="movie__media">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <center>
                                <c:choose>
                                    <c:when test="${not empty singleMovie.trailerURL }">
                                        <iframe width="720" height="360" src="${singleMovie.trailerURL}" frameborder="0" allowfullscreen>
                                        </iframe>
                                    </c:when>
                                    <c:otherwise>
                                        ${no_trailer}
                                    </c:otherwise>
                                </c:choose>

                            </center>
                        </div>
                    </div>

                </div>

            </div>


            <div class="choose-container">
                <h2 id="commentsSection" class="page-heading">${comments_name} (${comments.size()})</h2>
                <div class="comment-wrapper">
                    <c:choose>
                        <c:when test="${loggedUser.role ne 'BANNED' and loggedUser ne null}">
                            <form id="comment-form" name="addCommentForm" id="addCommentForm" class="comment-form" method="POST" action="controller">
                                <input type="hidden" name="command" value="add_comment"/>
                                <input type="hidden" name="userId" value="${loggedUser.userId}" />
                                <input type="hidden" name="movieId" value="${singleMovie.movieId}" />

                                <textarea class="comment-form__text" placeholder="${add_your_comment}" id="commentContent"
                                          name="commentContent" onkeyup="validateAddingComment()" ></textarea>
                                <div class="tip"><span class="tip-message">${comment_tip}</span></div>
                                <button type="submit" id="addButton" class="btn btn-md btn--danger comment-form__btn" disabled>${add_comment}</button>
                            </form>
                        </c:when>
                        <c:otherwise>
                            ${not_allowed}
                        </c:otherwise>
                    </c:choose>

                    <div class="clearfix"></div>

                    <c:if test="${not empty errorMessage}">
                        <div class="alert alert-danger" id="success-alert">
                            <button type="button" class="close" data-dismiss="alert"></button>
                                ${errorMessage}
                        </div>
                    </c:if>

                    <div class="clearfix"></div>

                    <div class="comment-sets">

                        <c:forEach items="${comments}" var="entry">
                            <div class="comment" id="${entry.value.commentId}">
                                <div class="comment__images">
                                    <img alt="${entry.key.login}" src="${entry.key.imageURL}">
                                </div>

                                <c:choose>
                                    <c:when test="${loggerUser.role ne 'BANNED'}">
                                        <a href="${pageContext.request.contextPath}/controller?command=view_profile&userId=${entry.key.userId}" class="comment__author">${entry.key.login}</a>
                                    </c:when>
                                    <c:otherwise><span class="comment__author">${entry.key.login}</span></c:otherwise>
                                </c:choose>

                                <p class="comment__date"> ${entry.value.date}</p>
                                <p class="comment__message">${entry.value.content}</p>

                                <c:choose>
                                    <c:when test="${loggedUser.role eq 'ADMIN' or loggedUser.userId eq entry.key.userId}">
                                        <a href="controller?command=delete_comment&userId=${entry.key.userId}&commentId=${entry.value.commentId}&movieId=${singleMovie.movieId}"
                                           class="comment__reply">${delete}</a>
                                    </c:when>
                                </c:choose>

                            </div>
                        </c:forEach>



                    </div>
                </div>
            </div>
        </div>

        <aside class="col-sm-4 col-md-3 col-sm-pull-8 col-md-pull-9">
            <div class="sitebar sitebar--left">

                <div class="category category--discuss category--count marginb-sm mobile-category ls-cat">
                    <h3 class="category__title">${the_most} <br><span class="title-edition">${discussed_posts}</span></h3>
                    <ol>
                        <c:forEach items="${mostDiscussed}" var="item" begin="0" end="10">
                            <li><a href="${pageContext.request.contextPath}/controller?command=view_single_movie&movieId=${item.movieId}" class="category__item">${item.title}</a></li>
                        </c:forEach>
                    </ol>
                </div>

                <div class="category category--cooming category--count marginb-sm mobile-category rs-cat">
                    <h3 class="category__title">${top6}<br><span class="title-edition">${movies_lr}</span></h3>
                    <ol>
                        <c:forEach items="${topMovies}" var="item">
                            <li><a href="${pageContext.request.contextPath}/controller?command=view_single_movie&movieId=${item.movieId}" class="category__item">${item.title}</a></li>
                        </c:forEach>
                    </ol>
                </div>



            </div>
        </aside>

    </section>

    <div class="clearfix"></div>

</div>

<div class="col-sm-6 col-sm-offset-3 modal fade" id="ratingModal" tabindex="-1" role="dialog" aria-labelledby="ratingModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <center>${sure} ${delete_rating}</center>

            </div>
            <div class="modal-footer">
                <a href="controller?command=delete_rating&userId=${loggedUser.userId}&movieId=${singleMovie.movieId}"
                   class="btn btn-md btn--danger comment-form__btn btn-modal-left">${delete}</a>
                <a href="" class="btn btn-md btn--danger comment-form__btn" data-dismiss="modal">${cancel}</a>
            </div>
        </div>
    </div>
</div>



<div class="col-sm-6 col-sm-offset-3 modal fade" id="removeFavouritesModal" tabindex="-1" role="dialog" aria-labelledby="removeFavouritesModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <center>${sure} ${remove_movie} "${singleMovie.title}" ${from_favourites}</center>

            </div>
            <div class="modal-footer">
                <a href="controller?command=remove_from_favourites&movieId=${singleMovie.movieId}&userId=${loggedUser.userId}"
                   class="btn btn-md btn--danger comment-form__btn btn-modal-left">${delete}</a>
                <a href="" class="btn btn-md btn--danger comment-form__btn" data-dismiss="modal">${cancel}</a>
            </div>
        </div>
    </div>
</div>



<%@ include file="footer.jsp" %>


</body>
</html>
