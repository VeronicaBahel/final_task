<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.title" bundle="${lang}" var="title" />
    <fmt:message key="locale.original_title" bundle="${lang}" var="original_title" />
    <fmt:message key="locale.actors" bundle="${lang}" var="actors" />
    <fmt:message key="locale.directors" bundle="${lang}" var="directors" />
    <fmt:message key="locale.categories" bundle="${lang}" var="categories" />
    <fmt:message key="locale.select" bundle="${lang}" var="select" />
    <fmt:message key="locale.poster" bundle="${lang}" var="poster" />
    <fmt:message key="locale.trailer" bundle="${lang}" var="trailer" />
    <fmt:message key="locale.release_date" bundle="${lang}" var="release_date" />
    <fmt:message key="locale.age_restriction" bundle="${lang}" var="age_restriction" />
    <fmt:message key="locale.budget" bundle="${lang}" var="budget" />
    <fmt:message key="locale.save" bundle="${lang}" var="save" />
    <fmt:message key="locale.brief_description" bundle="${lang}" var="brief_description" />
    <fmt:message key="locale.budget_tip" bundle="${lang}" var="budget_tip" />
    <fmt:message key="locale.age_limit_tip" bundle="${lang}" var="age_limit_tip" />
    <fmt:message key="locale.brief_description_tip" bundle="${lang}" var="brief_description_tip" />

    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/settings.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>
    <script src="${pageContext.request.contextPath}/js/validation.js"></script>

</head>
<body>
<%@ include file="header.jsp" %>

<div class="wrapper">

    <%@ include file="search.jsp" %>

    <div class="clearfix"></div>

    <div class="contact-form-wrapper">

        <div class="container mt ">
            <div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">

                <form id='contact-form' class="form row" method='post' action="controller" accept-charset="utf-8" enctype="multipart/form-data">
                    <input type="hidden" name="command" value="${ambiguousCommand}" />
                    <input type="hidden" name="movieId" value="${singleMovie.movieId}"/>

                    <div class="form-group">
                        <label for="title" class="col-sm-2 pt ">${title}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form__name " id="title" name="title" onkeyup="validateAddingMovie()"
                                   value="${singleMovie.title}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="originalTitle" class="col-sm-2 pt ">${original_title}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form__name " id="originalTitle" name="originalTitle"
                                   value="${singleMovie.originalTitle}" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2 pt">
                            ${categories}
                        </div>
                        <div class="col-sm-10 text-left pt pb">
                            <a href="#" data-toggle="modal" data-target="#genresModal">${select}</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2 pt">
                            ${actors}
                        </div>
                        <div class="col-sm-10 text-left pt pb">
                            <a href="#" data-toggle="modal" data-target="#actorsModal">${select}</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2 pt">
                            ${directors}
                        </div>
                        <div class="col-sm-10 text-left pt pb">
                            <a href="#" data-toggle="modal" data-target="#directorsModal">${select}</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="worldPremiere" class="col-sm-2 pt ">${release_date}</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control form__name " name="worldPremiere" value="${singleMovie.worldPremiere}" onkeyup="validateAddingMovie()"
                                   id="worldPremiere" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="budget" class="col-sm-2 pt ">${budget}</label>
                        <div class="col-sm-10">
                            <div class="tip"><span class="tip-message">${budget_tip}</span></div>
                            <input type="number" class="form-control form__name" id="budget" name="budget" placeholder="($)" min="1000" onkeyup="validateAddingMovie()"
                                   value="${singleMovie.budget}"  required />

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ageLimit" class="col-sm-2 pt ">${age_restriction}</label>
                        <div class="col-sm-10">
                            <div class="tip"><span class="tip-message">${age_limit_tip}</span></div>
                            <input type="number" class="form-control form__name " id="ageLimit" name="ageLimit" onkeyup="validateAddingMovie()"
                                   value="${singleMovie.ageLimit}" required />

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="briefDescription" class="col-sm-2 pt ">${brief_description}</label>
                        <div class="col-sm-10">
                            <div class="tip"><span class="tip-message">${brief_description_tip}</span></div>
                            <textarea class="form-control form__name " id="briefDescription" onkeyup="validateAddingMovie()" name="briefDescription" required>${singleMovie.briefDescription}</textarea>

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="posterURL" class="col-sm-2 pt ">${poster}</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control form__name " id="posterURL" accept="image/*" name="posterURL">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="trailerURL" class="col-sm-2 pt ">${trailer}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form__name " id="trailerURL" name="trailerURL"
                                   value="${singleMovie.trailerURL}">
                        </div>
                    </div>

                    <input type="submit" class='btn btn-md btn--danger' id="saveButton" value="${save}" disabled  />


                    <div class="col-sm-6 col-sm-offset-3 modal fade" id="genresModal" tabindex="-1" role="dialog" aria-labelledby="genresModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <c:forEach items="${genres}" var="genre" varStatus="genreLoop">
                                        <label>
                                            <input type="checkbox" name="genres" id="genres" value="${genre.genreId}" class="checkbox"
                                        <c:forEach var="checkedGenre" items="${checkedGenres}">
                                        <c:if test="${checkedGenre eq genre}">
                                                      checked
                                        </c:if>
                                        </c:forEach>
                                        >${genre.label}</label> <br />
                                    </c:forEach>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-sm-offset-3 modal fade" id="actorsModal" tabindex="-1" role="dialog" aria-labelledby="actorsModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <c:forEach items="${people}" var="actor" varStatus="actorLoop">
                                        <label><input type="checkbox" name="actors" id="${actor.personId}" value="${actor.personId}" class="checkbox"

                                        <c:forEach items="${checkedPeople}"  var="checkedPerson" >
                                        <c:if test="${(checkedPerson eq actor) && (checkedPerson.role eq 'ACTOR')}">
                                                      checked
                                        </c:if>
                                        </c:forEach>

                                        >${actor.firstName} ${actor.lastName}</label> <br />
                                    </c:forEach>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-sm-offset-3 modal fade" id="directorsModal" tabindex="-1" role="dialog" aria-labelledby="directorsModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <c:forEach items="${people}" var="director" varStatus="directorLoop">
                                        <label><input type="checkbox" name="directors" id="${director.personId}" value="${director.personId}" class="checkbox"

                                        <c:forEach items="${checkedPeople}" var="checkedPerson" >
                                        <c:if test="${(checkedPerson eq director) && (checkedPerson.role eq 'DIRECTOR')}">
                                                      checked
                                        </c:if>
                                        </c:forEach>

                                        >${director.firstName} ${director.lastName}</label> <br />
                                    </c:forEach>
                                </div>

                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>


    <div class="clearfix"></div>


</div>

<%@ include file="footer.jsp" %>
</body>
</html>
