<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.remove_from_watchlist" bundle="${lang}" var="remove_from_watchlist" />
    <fmt:message key="locale.your_watchlist" bundle="${lang}" var="your_watchlist" />
    <fmt:message key="locale.categories" bundle="${lang}" var="categories" />
    <fmt:message key="locale.release_date" bundle="${lang}" var="release_date" />
    <fmt:message key="locale.directors" bundle="${lang}" var="directors" />
    <fmt:message key="locale.actors" bundle="${lang}" var="actors" />
    <fmt:message key="locale.age_restriction" bundle="${lang}" var="age_restriction" />
    <fmt:message key="locale.budget" bundle="${lang}" var="budget" />
    <fmt:message key="locale.previous" bundle="${lang}" var="previous" />
    <fmt:message key="locale.next" bundle="${lang}" var="next" />
    <fmt:message key="locale.nothing_in_watchlist" bundle="${lang}" var="nothing_in_watchlist" />
    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/settings.css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="wrapper">


    <%@ include file="search.jsp" %>

        <section class="container">
            <div class="col-sm-12">

                <h2 class="page-heading">${your_watchlist}</h2>


                <c:choose>
                    <c:when test="${watchlist.size() ne 0}">
                        <c:forEach items="${watchlist}" var="item">
                            <div class="movie movie--preview movie--full release">
                                <div class="col-sm-3 col-md-2 col-lg-2">
                                    <div class="movie__images">
                                        <img src="${item.posterURL}" >
                                    </div>
                                </div>

                                <div class="col-sm-9 col-md-10 col-lg-10 movie__about">
                                    <a href='controller?command=view_single_movie&movieId=${item.movieId}' class="movie__title link--huge">${item.title}</a>


                                    <p class="movie__option"><strong>${categories}: </strong>
                                        <c:forEach items="${item.genres}" var="genre" varStatus="genreLoop">
                                        <a href="controller?command=view_with_genre?genre=${genre.label}">${genre.label}</a>${!genreLoop.last ? ',' : ''}
                                        </c:forEach>
                                    <p class="movie__option"><strong>${release_date}: </strong> ${item.worldPremiere}</p>
                                    <p class="movie__option"><strong>${directors}: </strong>
                                        <c:forEach items="${item.people}" var="director" varStatus="directorLoop">
                                        <c:choose>
                                        <c:when test="${director.role eq 'DIRECTOR'}">
                                        <a href="controller?command=view_person&personId=${director.personId}">${director.firstName} ${director.lastName}</a>${!directorLoop.last ? ',' : ''}
                                        </c:when>
                                        </c:choose>
                                        </c:forEach>
                                    <p class="movie__option"><strong>${actors}: </strong>
                                        <c:forEach items="${item.people}" var="actor" varStatus="actorLoop">
                                        <c:choose>
                                        <c:when test="${actor.role eq 'ACTOR'}">
                                        <a href="controller?command=view_person&personId=${actor.personId}">${actor.firstName} ${actor.lastName}</a>${!actorLoop.last ? ',' : ''}
                                        </c:when>
                                        </c:choose>
                                        </c:forEach>
                                    <p class="movie__option"><strong>${age_restriction}: </strong> ${item.ageLimit}
                                    <p class="movie__option"><strong>${budget}: </strong> $ ${item.budget}

                                </div>

                                <div class="clearfix"></div>

                            </div>
                        </c:forEach>

                        <div class="coloum-wrapper">
                            <div class="pagination paginatioon--full">


                                <c:if test="${currentPageNumber != 1}">
                                    <a href="controller?command=view_movies&pageNumber=${currentPageNumber - 1}" class="pagination__prev">${previous}</a>
                                </c:if>

                                <center>
                                    <c:forEach begin="1" end="${numberOfPages}" var="i">
                                        <c:choose>
                                            <c:when test="${currentPageNumber eq i}">
                                                <td>${i}</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td><a href="controller?command=view_movies&pageNumber=${i}">${i}</a></td>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </center>

                                <c:if test="${currentPageNumber lt numberOfPages}">
                                    <td><a href="controller?command=view_movies&pageNumber=${currentPageNumber + 1}" class="pagination__next">${next}</a></td>
                                </c:if>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        ${nothing_in_watchlist}
                    </c:otherwise>
                </c:choose>


            </div>

        </section>

        <div class="clearfix"></div>

</div>
<%@ include file="footer.jsp" %>



</body>
</html>

