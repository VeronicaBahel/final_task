<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="localization" var="lang" />
<fmt:message key="locale.sign_in" bundle="${lang}" var="sign_in" />
<fmt:message key="locale.logout" bundle="${lang}" var="logout" />
<fmt:message key="locale.movies_and_tv" bundle="${lang}" var="movies_and_tv" />
<fmt:message key="locale.celebs" bundle="${lang}" var="celebs" />
<fmt:message key="locale.control_panel" bundle="${lang}" var="control_panel" />
<fmt:message key="locale.coming_soon" bundle="${lang}" var="coming_soon" />
<fmt:message key="locale.popular_movies" bundle="${lang}" var="popular_movies" />
<fmt:message key="locale.top100" bundle="${lang}" var="top100" />
<fmt:message key="locale.see_all" bundle="${lang}" var="see_all" />
<fmt:message key="locale.popular_celebs" bundle="${lang}" var="popular_celebs" />
<fmt:message key="locale.born_today" bundle="${lang}" var="born_today" />
<fmt:message key="locale.add_movie" bundle="${lang}" var="add_movie" />
<fmt:message key="locale.add_person" bundle="${lang}" var="add_person" />
<fmt:message key="locale.user_statistics" bundle="${lang}" var="user_statistics" />
<fmt:message key="locale.view_profile" bundle="${lang}" var="view_profile" />
<fmt:message key="locale.your_watchlist" bundle="${lang}" var="your_watchlist" />


<header class="header-wrapper header-wrapper--home">
    <div class="container">

        <a href="controller?command=default_command" class="logo">
            <img alt='logo' src="${pageContext.request.contextPath}/img/logo.png">
        </a>

        <nav id="navigation-box">

            <a href="#" id="navigation-toggle">
                        <span class="menu-icon">
                            <span class="icon-toggle" role="button" aria-label="Toggle Navigation">
                              <span class="lines"></span>
                            </span>
                        </span>
            </a>


            <ul id="navigation">
                <li>
                    <span class="sub-nav-toggle plus"></span>
                    <a href="#">${movies_and_tv}</a>
                    <ul>
                        <li class="menu__nav-item"><a href="${pageContext.request.contextPath}/controller?command=view_coming_soon">${coming_soon}</a></li>
                        <li class="menu__nav-item"><a href="${pageContext.request.contextPath}/controller?command=view_top_100">${top100}</a></li>
                        <li class="menu__nav-item"><a href="${pageContext.request.contextPath}/controller?command=view_movies">${see_all}</a></li>
                    </ul>
                </li>
                <li>
                    <span class="sub-nav-toggle plus"></span>
                    <a href="#">${celebs}</a>
                    <ul>
                        <li class="menu__nav-item"><a href="${pageContext.request.contextPath}/controller?command=view_born_today">${born_today}</a></li>
                        <li class="menu__nav-item"><a href="${pageContext.request.contextPath}/controller?command=view_people">${see_all}</a></li>
                    </ul>
                </li>
                <c:choose>
                    <c:when test="${loggedUser.role eq 'ADMIN'}">
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="#">${control_panel}</a>
                            <ul>
                                <li class="menu__nav-item"><a href="${pageContext.request.contextPath}/controller?command=add_movie_page">${add_movie}</a></li>
                                <li class="menu__nav-item"><a href="${pageContext.request.contextPath}/controller?command=add_person_page">${add_person}</a></li>
                                <li class="menu__nav-item"><a href="${pageContext.request.contextPath}/controller?command=view_user_table">${user_statistics}</a></li>
                            </ul>
                        </li>
                    </c:when>
                </c:choose>
            </ul>
        </nav>

        <div class="control-panel">
            <c:choose>
                <c:when test="${not empty loggedUser}">
                    <div class="auth auth--home">
                        <div class="auth__show">
                                <span class="auth__image">
                                  <img alt="" src="${loggedUser.imageURL}">
                                </span>
                        </div>
                        <a href="#" class="btn btn--sign btn--singin">
                                ${loggedUser.login}
                        </a>
                        <ul class="auth__function">
                            <li><a href="${pageContext.request.contextPath}/controller?command=view_profile&userId=${loggedUser.userId}" class="auth__function-item">${view_profile}</a></li>
                            <li><a href="${pageContext.request.contextPath}/controller?command=view_watchlist" class="auth__function-item">${your_watchlist}</a></li>
                            <li><a href="${pageContext.request.contextPath}/controller?command=logout" class="auth__function-item">${logout}</a></li>
                        </ul>
                    </div>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/controller?command=login_page&page=${pageContext.request.requestURI}" class="btn btn--sign">${sign_in}</a>
                </c:otherwise>
            </c:choose>


        </div>

    </div>
</header>
