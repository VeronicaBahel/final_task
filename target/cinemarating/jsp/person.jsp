<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.edit" bundle="${lang}" var="edit" />
    <fmt:message key="locale.actor" bundle="${lang}" var="actor" />
    <fmt:message key="locale.director" bundle="${lang}" var="director" />
    <fmt:message key="locale.not_an_actor" bundle="${lang}" var="not_an_actor" />
    <fmt:message key="locale.not_a_director" bundle="${lang}" var="not_a_director" />

    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/settings.css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>
</head>
<body>
<%@ include file="header.jsp" %>

<div class="wrapper">

    <div class="wrapper">

        <%@ include file="search.jsp" %>

        <section class="container">
            <h2 class=" heading--outcontainer center">
                <img src="${person.pictureURL}" class="width260-only" />
                <c:choose>
                    <c:when test="${loggedUser.role eq 'ADMIN'}">
                        <div><a href="controller?command=update_person_page&personId=${person.personId}" class="btn btn-md btn--warning">${edit}</a></div>
                    </c:when>
                </c:choose>

            </h2>
            <div class="contact">
                <p class="contact__title">
                    <span class="contact__describe">${person.firstName} ${person.lastName}</span>
                </p>

                <span class="contact__location">${person.birthPlace}</span>
                <span class="contact__age">${person.birthday}</span> <br/>

            </div>
            <c:choose>
                <c:when test="${personMovies.size() ne 0}">
                    <div class="tabs tabs--horisontal">

                        <div class="tab-content">
                            <div class="tab-pane active">
                                <div class="container">
                                    <div class="movie-time-wrap">

                                        <div class="col-sm-6">

                                            <c:forEach items="${personMovies}" var="item">

                                                <div class="movie movie--time">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-5">
                                                            <div class="movie__images">
                                                                <span class="movie__rating">${item.averageRating}</span>
                                                                <img src="${item.posterURL}">
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6 col-md-7">
                                                            <a href='controller?command=view_single_movie&movieId=${item.movieId}' class="movie__title">${item.title}</a>
                                                            <p class="movie__option">
                                                                <c:forEach items="${item.genres}" var="genre" varStatus="genreLoop">
                                                                    <a href="controller?command=view_with_genre?genre=${genre.label}">${genre.label}</a> ${!genreLoop.last ? '| ' : ''}
                                                                </c:forEach>
                                                            </p>
                                                        </div>

                                                    </div>
                                                </div>

                                            </c:forEach>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </c:when>
            </c:choose>




        </section>

        <div class="clearfix"></div>

    </div>

    <%@ include file="footer.jsp" %>

</body>
</html>

