<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.no_rates" bundle="${lang}" var="no_rates" />
    <fmt:message key="locale.rates_label" bundle="${lang}" var="rates_label" />

    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/settings.css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>
</head>
<body>
<%@ include file="header.jsp" %>

<div class="wrapper">

    <%@ include file="search.jsp" %>
    <div class="clearfix"></div>

    <section class="container">

        <h2 class="page-heading heading--outcontainer">${rates_label}</h2>

        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-8 col-md-9">
                    <c:choose>
                        <c:when test="${not empty rates}">
                            <c:forEach items="${rates}" var="item">
                                <div class="movie movie--test movie--test--light movie--test--left">
                                    <div class="movie__images">
                                        <a href="controller?command=view_single_movie&movieId=${item.key.movieId}" class="movie-beta__link">
                                            <div class="thumbnail">
                                                <img src="${item.key.posterURL}" class="portrait">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="movie__info">
                                        <a href="controller?command=view_single_movie&movieId=${item.key.movieId}" class="movie__title">${item.key.title}</a>

                                        <p class="movie__option">
                                            <c:forEach items="${item.key.genres}" var="genre" varStatus="genreLoop">
                                                <a href="controller?command=view_with_genre?genre=${genre.label}">${genre.label}</a> ${!genreLoop.last ? '| ' : ''}
                                            </c:forEach>
                                        </p>

                                        <div class="movie__rate">
                                            <div class="score"></div>
                                            <span class="movie__rating">${item.value.value}</span>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            ${no_rates}
                        </c:otherwise>
                    </c:choose>

                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </section>


</div>


<%@ include file="footer.jsp" %>
</body>
</html>
