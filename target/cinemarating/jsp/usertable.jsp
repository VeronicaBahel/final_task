<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="localization" var="lang" />
    <fmt:message key="locale.amovie" bundle="${lang}" var="amovie" />
    <fmt:message key="locale.ban" bundle="${lang}" var="ban" />
    <fmt:message key="locale.remove_ban" bundle="${lang}" var="remove_ban" />
    <fmt:message key="locale.user_control" bundle="${lang}" var="user_control" />
    <fmt:message key="locale.comments_count" bundle="${lang}" var="comments_count" />
    <fmt:message key="locale.ratings_count" bundle="${lang}" var="ratings_count" />
    <fmt:message key="locale.previous" bundle="${lang}" var="previous" />
    <fmt:message key="locale.next" bundle="${lang}" var="next" />

    <meta charset="utf-8">
    <title>${amovie}</title>
    <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/nav.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/settings.css" media="screen" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="wrapper">

    <%@ include file="search.jsp" %>

        <section class="container">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-heading">${user_control}</h2>

                        <div class="rates-wrapper rates--full">

                            <table>
                                <colgroup class="col-width-lg">
                                <colgroup class="col-width">
                                <colgroup class="col-width-sm">
                                <colgroup class="col-width">

                                <c:forEach items="${users}" var="item">
                                    <c:forEach items="${item}" var="entry" varStatus="loop">
                                        <tr class="rates">
                                            <td class="rates__obj"><a href="controller?command=view_profile&userId=${entry.key.userId}" class="rates__obj-name">${entry.key.login}</a></td>

                                            <td class="rates__vote">${comments_count}: ${entry.value[0]}</td>
                                            <td class="rates__result">${ratings_count}: ${entry.value[1]}</td>
                                            <td class="rates__stars">
                                                <center>${entry.key.role}
                                                    <c:choose>
                                                        <c:when test="${entry.key.role eq 'REGULAR'}"><a href="controller?command=ban_user&userId=${entry.key.userId}">(${ban})</a></c:when>
                                                        <c:when test="${entry.key.role eq 'BANNED'}"><a href="controller?command=remove_ban_from_user&userId=${entry.key.userId}">(${remove_ban})</a></c:when>
                                                    </c:choose>
                                                </center>
                                            </td>


                                        </tr>
                                    </c:forEach>
                                </c:forEach>


                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </section>

        <div class="clearfix"></div>



        <%@ include file="footer.jsp" %>

</body>
</html>

