<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="localization" var="lang" />
<fmt:message key="locale.search_a_movie" bundle="${lang}" var="search_a_movie" />
<fmt:message key="locale.search" bundle="${lang}" var="search" />

<div class="search-wrapper">
    <div class="container container--add">
        <form name="searchForm"  id='search-form' action="controller" method="POST" accept-charset="utf-8" class="search">
            <input type="hidden" name="command" value="search" />
            <input type="text" name="parameter" class="search__field" placeholder="${search}">
            <button type='submit' class="btn btn-md btn--danger search__button">${search_a_movie}</button>
        </form>
    </div>
</div>